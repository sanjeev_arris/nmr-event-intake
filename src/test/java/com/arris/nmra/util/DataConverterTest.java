package com.arris.nmra.util;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by smishra on 11/17/16.
 */
public class DataConverterTest {
    @Test
    public void dataTypeEnumTest() {
        Assert.assertEquals(DataConverter.DataType.OBJ, DataConverter.DataType.byCode(null));
        Assert.assertEquals(DataConverter.DataType.OBJ, DataConverter.DataType.byCode("  "));
        Assert.assertEquals(DataConverter.DataType.OBJ, DataConverter.DataType.byCode("uns"));

        Assert.assertEquals(DataConverter.DataType.BOOL, DataConverter.DataType.byCode("bool"));
        Assert.assertEquals(DataConverter.DataType.BOOL, DataConverter.DataType.byCode("Bool"));
        Assert.assertEquals(DataConverter.DataType.INT, DataConverter.DataType.byCode("int"));
        Assert.assertEquals(DataConverter.DataType.INT, DataConverter.DataType.byCode("Int"));
        Assert.assertEquals(DataConverter.DataType.LONG, DataConverter.DataType.byCode("long"));
        Assert.assertEquals(DataConverter.DataType.LONG, DataConverter.DataType.byCode("Long"));
        Assert.assertEquals(DataConverter.DataType.FLOAT, DataConverter.DataType.byCode("float"));
        Assert.assertEquals(DataConverter.DataType.FLOAT, DataConverter.DataType.byCode("Float"));
        Assert.assertEquals(DataConverter.DataType.DOUBLE, DataConverter.DataType.byCode("double"));
        Assert.assertEquals(DataConverter.DataType.DOUBLE, DataConverter.DataType.byCode("Double"));
        Assert.assertEquals(DataConverter.DataType.DATE, DataConverter.DataType.byCode("date"));
        Assert.assertEquals(DataConverter.DataType.DATE, DataConverter.DataType.byCode("Date"));
        Assert.assertEquals(DataConverter.DataType.TIME, DataConverter.DataType.byCode("time"));
        Assert.assertEquals(DataConverter.DataType.TIME, DataConverter.DataType.byCode("Time"));
    }

    @Test
    public void toInt() {
        Integer expected = 1234;
        Object value = 1234;
        String outFormat = null;
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));
        value = "1234.24";
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));
        value = new Double(1234.34);
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));

        value = new Date();
        expected = new Long(((Date) value).getTime()).intValue();
        Assert.assertEquals(expected, DataConverter.convert(((Date) value).getTime(), null, DataConverter.DataType.INT, null, outFormat));
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));

        value = new Time(((Date) value).getTime());
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));
        value = new Date().getTime();
        value = new BigInteger(value + "");
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));

        value = new BigDecimal(value.toString() + "." + value.toString());
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));

        value = new Boolean(true);
        Assert.assertEquals(1, DataConverter.convert(value, null, DataConverter.DataType.INT, null, outFormat));
    }

    @Test
    public void toLong() {
        Long expected = 1234l;
        Object value = 1234;
        String outFormat = null;
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));
        value = "1234.24";
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));
        value = new Double(1234.34);
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));

        value = new Date();
        expected = new Long(((Date) value).getTime()).longValue();
        Assert.assertEquals(expected, DataConverter.convert(((Date) value).getTime(), null, DataConverter.DataType.LONG, null, outFormat));
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));

        value = new Time(((Date) value).getTime());
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));
        value = new Date().getTime();
        value = new BigInteger(value + "");
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));

        value = new BigDecimal(value.toString() + "." + value.toString());
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));

        value = new Boolean(true);
        Assert.assertEquals(1l, DataConverter.convert(value, null, DataConverter.DataType.LONG, null, outFormat));
    }

    @Test
    public void toDouble() {
        Double expected = 1234.24;
        Object value = expected;
        String outFormat = null;
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));
        value = "1234.24";
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));
        value = new Double(1234.24);
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));

        value = new Date();
        expected = new Long(((Date) value).getTime()).doubleValue();
        Assert.assertEquals(expected, DataConverter.convert(((Date) value).getTime(), null, DataConverter.DataType.DOUBLE, null, outFormat));
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));

        value = new Time(((Date) value).getTime());
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));
        value = new Date().getTime();
        value = new BigInteger(value + "");
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, "#0.4", outFormat));

        value = new BigDecimal(value.toString() + "." + value.toString());
        Assert.assertEquals(0, (expected - (Double) DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat)), .2);

        value = new Boolean(true);
        Assert.assertEquals(1.0, DataConverter.convert(value, null, DataConverter.DataType.DOUBLE, null, outFormat));
    }

    @Test
    public void toStringValue() {
        String expected = "1234.24";
        Object value = expected;
        String outFormat = null;
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, outFormat));
        value = "1234.24";
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, outFormat));
        value = new Double(1234.24);
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, outFormat));

        value = new Date();
        expected = ((Date)value).toInstant().toString();
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, outFormat));

        value = new Time(((Date) value).getTime());
        expected = new SimpleDateFormat("dd:mm:ss").format((Time) value);
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, "dd:mm:ss"));
        expected = "1479402667206.8";
        value = new BigInteger("1479402667206");

        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, "#0.8"));

        value = new BigDecimal(value.toString() + "." + value.toString());
        expected = "1.479E12";
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, "0.###E0"));

        value = new Boolean(true);
        Assert.assertEquals("true", DataConverter.convert(value, null, DataConverter.DataType.STRING, null, outFormat));

        //utc test
        value = new Date().getTime();
        expected = new Date((long) value).toInstant().toString();
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, "utc"));

        value = new Date();
        expected = ((Date) value).toInstant().toString();
        Assert.assertEquals(expected, DataConverter.convert(value, null, DataConverter.DataType.STRING, null, "utc"));
    }

    @Test
    public void toDate() {
        Object date = new Date();
        Date expected = (Date) date;
        String outFormat = null;

        Assert.assertEquals(expected, DataConverter.convert(date, null, DataConverter.DataType.DATE, null, outFormat));
        Long dateInLong = expected.getTime();
        Assert.assertEquals(expected, DataConverter.convert(dateInLong, null, DataConverter.DataType.DATE, null, outFormat));

        String dateInUtc = new Date(dateInLong).toInstant().toString();
        Assert.assertEquals(expected, DataConverter.convert(dateInUtc, null, DataConverter.DataType.DATE, "utc", outFormat));
    }
}
