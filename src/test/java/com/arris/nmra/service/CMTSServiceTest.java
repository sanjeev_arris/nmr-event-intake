package com.arris.nmra.service;

import com.arris.nmra.model.CMTSUtilization;
import com.arris.nmra.util.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by smishra on 1/13/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class CMTSServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(CMTSServiceTest.class);
    @Autowired
    private JdbcTemplate template;
    @Autowired
    private CMTSService cmtsService;

    int tenantId;
    long topologyId;
    long createdAt;

    @Before
    public void setUp() throws IOException {
        tenantId = template.queryForObject("select id from tenant order by id asc limit 1", Integer.class);
        topologyId = template.queryForObject("select id from topology order by id asc limit 1", Long.class);
        DateTime dt = new DateTime();
        createdAt = dt.toInstant().getMillis();

        CMTSUtilization[] array = new ObjectMapper().readValue(Util.bytesFromResource("data/cmtsUtilization.json"), CMTSUtilization[].class);
        Arrays.stream(array).forEach(u -> {
            template.update("INSERT INTO cmts_util_summary(tenant_id, topology_id, market, hub, cmts, cm_count, " +
                "us_sg_count, us_avg_util, us_over_cap_count, us_over_cap_perc,  " +
                "ds_sg_count, ds_avg_util, ds_over_cap_count, ds_over_cap_perc," +
                "year, month, day, created_at) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", (ps) -> {
                ps.setInt(1, tenantId);
                ps.setLong(2, topologyId);
                ps.setString(3, u.getMarket());
                ps.setString(4, u.getHub());
                ps.setString(5, u.getCmts());
                ps.setInt(6, u.getCmCount());
                ps.setInt(7, u.getUpstreamSGCount());
                ps.setDouble(8, u.getUpstreamAvgUtil());
                ps.setInt(9, u.getUpstreamOverCapacityCount());
                ps.setDouble(10, u.getUpstreamOverCapacityPercent());
                ps.setInt(11, u.getDownstreamSGCount());
                ps.setDouble(12, u.getDownstreamAvgUtil());
                ps.setInt(13, u.getDownstreamOverCapacityCount());
                ps.setDouble(14, u.getDownstreamOverCapacityPercent());
                ps.setInt(15, dt.getYear());
                ps.setInt(16, dt.getMonthOfYear());
                ps.setInt(17, dt.getDayOfMonth());
                ps.setLong(18, createdAt);
            });
        });
    }

    @After
    public void tearDown() {
        template.update("DELETE FROM cmts_util_summary WHERE created_at = " + createdAt);
    }

    @Test
    public void givenCMTSSummaryTableWhenQueriedItShouldReturnCMTSUtilization() {
        int count =15;
        Collection<CMTSUtilization> utils = cmtsService.utilization(1, "1m", 1, count);
        Assert.assertTrue(utils.size() == count);
        Assert.assertFalse(utils.stream().filter(u -> u.getTopologyId()<=0).findFirst().isPresent());
        try {
            String str = new ObjectMapper().writeValueAsString(utils);
            logger.info(str);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}