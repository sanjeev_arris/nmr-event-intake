package com.arris.nmra.service;

import com.arris.nmra.model.SgStatsData;
import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by smishra on 11/14/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class SgRawToSG {
    private static final Logger logger = LoggerFactory.getLogger(SgRawToSG.class);
    @Value("${tenant}")
    private String tenant;
    private Integer tenantId;

    @Autowired
    private DBService dbService;

    @Before
    public void setUp() throws SQLException {
//        dbService.executeUpdate("if exists (select * from sys.tables where name ='sg_stats_daily') drop table sg_stats_daily");
//        dbService.executeUpdate("if exists (select * from sys.tables where name ='sg_stats_weekly') drop table sg_stats_weekly");
//        dbService.executeUpdate("if exists (select * from sys.tables where name ='sg_stats_monthly') drop table sg_stats_monthly");
//        dbService.executeUpdate("if exists (select * from sys.tables where name ='ch_stats') drop table ch_stats");
//        dbService.executeUpdate("if exists (select * from sys.tables where name ='sg_stats') drop table sg_stats");

        dbService.executeUpdate("truncate sg_stats_raw cascade");
        dbService.executeUpdate("truncate sg cascade");
        dbService.executeUpdate("" +
                "INSERT INTO sg (tmax, cmts, cable_mac, sg_id, year, month, wk, day, hour, minute, stats_ts, created_at) " +
                "SELECT 0, 'dummy', 'dummy', -1, 0, 0, 0, 0, 0, 0, -1, -1 " +
                "WHERE NOT EXISTS (SELECT id FROM sg)");

        tenantId = dbService.queryForCount("select id from tenant where name='"+tenant+"'");
    }

    @After
    public void tearDown() throws SQLException {
        dbService.executeUpdate("delete from sg");
        dbService.executeUpdate("delete from sg_stats_raw");
    }

    @Test
    public void givenSGStatsRawWhenExecutedOnlyUniqueSGShouldBeInserted() throws IOException, SQLException {
        SgStatsData[] statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/downstreamStats.json"), SgStatsData[].class);
        Map grouped = Arrays.stream(statsRaw).
                collect(Collectors.groupingBy(sg -> getKeyByAllFour(sg), Collectors.toList()));

        Assert.assertTrue(statsRaw.length>0);
        Collection<Integer> result = insertRawSG(statsRaw, true);

        logger.info("writeToDB result: " + result);
        Assert.assertFalse(result.isEmpty());

        int writeCount = dbService.queryForCount("select count(*) from sg_stats_raw");
        Assert.assertEquals(grouped.size(), writeCount);
    }

    @Test
    public void givenSGStatsRawWhenExecutedSGTableShouldBePopulated() throws IOException, SQLException {
        SgStatsData[] statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/dsSgStats_1.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        Collection<Integer> result = insertRawSG(statsRaw, true);

        logger.info("writeToDB result: " + result);
        Assert.assertEquals(statsRaw.length, result.size());
        long insertedInSGRaw = result.stream().filter(i -> i>0).count();
        Assert.assertEquals(1, insertedInSGRaw);
        long then = System.currentTimeMillis();

        String updateSGSql = new String(Util.bytesFromResource("sql/sg_stats_raw_2_sg.sql")).replace("__CREATED_AT__", then+"");

        dbService.executeUpdate(updateSGSql);
        int insertCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertEquals(insertedInSGRaw+1, insertCount);
    }

    @Test
    public void givenSGStatsRawWhenExecutedSGTableShouldBePopulatedWithUniqueRecordsOnly() throws IOException, SQLException {
        SgStatsData[] statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/dsSgStats_1.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        Collection<Integer> result = insertRawSG(statsRaw, true);
        long insertedInSGRaw = result.stream().filter(i -> i>0).count();
        Assert.assertEquals(1, insertedInSGRaw);

        logger.info("writeToDB result: " + result);
        Assert.assertEquals(statsRaw.length, result.size());

        Assert.assertEquals(1, insertedInSGRaw);

        String updateSGSql = new String(Util.bytesFromResource("sql/sg_stats_raw_2_sg.sql"));

        dbService.executeUpdate(updateSGSql.replace("__CREATED_AT__", System.currentTimeMillis()+""));
        int origSgCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertEquals(insertedInSGRaw+1, origSgCount);

        /*Drop data from raw and insert same data*/
        dbService.executeUpdate("delete from sg_stats_raw");
        statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/dsSgStats_1.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        result = insertRawSG(statsRaw, true);
        insertedInSGRaw = result.stream().filter(i -> i>0).count();
        Assert.assertEquals(1, insertedInSGRaw);

        dbService.executeUpdate(updateSGSql.replace("__CREATED_AT__", System.currentTimeMillis()+""));

        int newSgCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertEquals(origSgCount, newSgCount);

        /*Insert an upstream sg data (cmts, cabale_mac, sg same as one of the downstream data) */
        statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/usSgStats_1.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        result = insertRawSG(statsRaw, false);
        insertedInSGRaw = result.stream().filter(i -> i>0).count();
        Assert.assertEquals(1, insertedInSGRaw);

        dbService.executeUpdate(updateSGSql.replace("__CREATED_AT__", System.currentTimeMillis()+""));

        newSgCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertTrue(origSgCount>= newSgCount);
    }

    @Test
    public void givenSGStatsRawWhenExecutedSGTableShouldBePopulatedOnlyWithDelta() throws IOException, SQLException {
        SgStatsData[] statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/dsSgStats_1.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        Collection<Integer> result = insertRawSG(statsRaw, true);
        long insertedInSGRaw = result.stream().filter(i -> i>0).count();
        Assert.assertEquals(1, insertedInSGRaw);

        logger.info("writeToDB result: " + result);
        Assert.assertEquals(statsRaw.length, result.size());

        Assert.assertEquals(1, insertedInSGRaw);

        String updateSGSql = new String(Util.bytesFromResource("sql/sg_stats_raw_2_sg.sql"));

        dbService.executeUpdate(updateSGSql.replace("__CREATED_AT__", System.currentTimeMillis()+""));
        int totalSgCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertEquals(insertedInSGRaw+1, totalSgCount);

        /*Insert another unique downstream sg data*/
        statsRaw  = new ObjectMapper().readValue(Util.bytesFromResource("data/dsSgStats_2.json"), SgStatsData[].class);
        Assert.assertTrue(statsRaw.length>0);
        result = insertRawSG(statsRaw, true);
        insertedInSGRaw = result.stream().filter(i -> i>0).count();

        dbService.executeUpdate(updateSGSql.replace("__CREATED_AT__", System.currentTimeMillis()+""));

        int newSgCount = dbService.queryForCount("select count(*) from sg");
        Assert.assertEquals(totalSgCount+1, newSgCount);



    }

    private Collection<Integer> insertRawSG(SgStatsData[] statsRaw, boolean downstream) {
        long now = System.currentTimeMillis();
        return dbService.writeToDB(Arrays.stream(statsRaw).map(d -> {
            d.setTenantId(tenantId);
            d.setCreatedAt(now);
            d.setDownstream(downstream?1:0);
            return d;
        }).collect(Collectors.toList()), "rawStats");
    }


    protected String getKeyByAllFour(SgStatsData d) {
        return d.getDownstream()+ "_"+ d.getCmts() + "_" + d.getCableMac() + "_" + d.getSgId();
    }

    protected String getKeyByThree(SgStatsData d) {
        return d.getCmts() + "_" + d.getCableMac() + "_" + d.getSgId();
    }

    protected String getKeyByTwo(SgStatsData d) {
        return d.getCmts() + "_" + d.getCableMac();
    }
}
