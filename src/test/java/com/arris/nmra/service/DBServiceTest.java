package com.arris.nmra.service;

import com.arris.nmra.model.ModelUtil;
import com.arris.nmra.model.TopologyData;
import com.arris.nmra.util.Util;
import org.apache.commons.collections.map.HashedMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.*;

/**
 * Created by smishra on 11/14/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class DBServiceTest {
    private static final Logger logger = LoggerFactory.getLogger(DBServiceTest.class);

    @Autowired
    private DBService dbService;
    private int defaultTenant;
    @Before
    public void setUp() throws SQLException {
        //dbService.executeUpdate("delete from topology");
        defaultTenant = dbService.queryForCount("select id from tenant limit 1");
    }

    @After
    public void tearDown() throws SQLException {
        //dbService.executeUpdate("delete from topology");
    }

    @Test
    public void givenQueryWithRequiredParamsItShouldBeFound() {
        String stmt = new String(Util.bytesFromResource("sql/topologyAgg.sql"));
        Set<String> params = dbService.findRequiredParams(stmt);
        logger.info("required params: " + params);
        Assert.assertEquals(6, params.size());
        Assert.assertTrue(params.contains("__HUB"));
        Assert.assertTrue(params.contains("__TENANT"));
        Assert.assertTrue(params.contains("__TENANT_ID"));
        Assert.assertTrue(params.contains("__MARKET"));
        Assert.assertTrue(params.contains("__CMTS"));
        Assert.assertTrue(params.contains("__CREATED_AT"));
    }

    @Test
    public void givenMissingParamInMapAnExceptionShouldBeRaised() {
        String stmt = new String(Util.bytesFromResource("sql/topologyAgg.sql"));

        try {
            dbService.createPreparedStatement(stmt, new HashedMap());
            Assert.fail("MissingParamException should be thrown");
        } catch (MissingParamException e) {
            logger.info(e.getMessage());
        } catch (DataSourceException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void givenTopologyCollectionWhenPreparedStatementExecutedItShouldBeSaved() throws DataSourceException, SQLException, InterruptedException {
        int origCount = dbService.queryForCount("select count(*) from topology");
        logger.info("total topology count: " + origCount);
        Thread.sleep(1);
        List<TopologyData> data = getTopologyData();
        List<ModelUtil.ColumnData> psParams = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(data, psParams);
        psParams.stream().filter(p -> p.columnName.equals("tenant_id")).findFirst().ifPresent(p -> {
            if (p.columnName.equals("tenant_id") && (p.values.isEmpty() || p.values.get(0)==null)) {
                p.values = Collections.singletonList(defaultTenant);
            }
        });

        Collection<Integer> result = dbService.executePreparedStatement(stmt, psParams);
        logger.info("result of execution: " + result);
        int updateCount = dbService.queryForCount("select count(*) from topology");
        logger.info("Total topology count after update: " + updateCount);
        Assert.assertTrue(updateCount >= origCount);
    }

    private List<TopologyData> getTopologyData() {
        List<TopologyData> list = new ArrayList<>();
        {
            TopologyData topologyData = new TopologyData();
            topologyData.setCmts("cmts-1");
            topologyData.setHub("hub-1");
            topologyData.setMarket("market-1");
            topologyData.setTs(System.currentTimeMillis());
            list.add(topologyData);
        }

        {
            TopologyData topologyData = new TopologyData();
            topologyData.setCmts("cmts-2");
            topologyData.setHub("hub-1");
            topologyData.setMarket("market-1");
            topologyData.setTs(System.currentTimeMillis());
            list.add(topologyData);
        }
        return list;
    }
}
