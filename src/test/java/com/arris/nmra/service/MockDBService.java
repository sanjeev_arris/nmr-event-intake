package com.arris.nmra.service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by sanjeev on 1/3/17.
 */
public class MockDBService extends DBService {
    Collection<Collection<String>> stmtCollection = new ArrayList<>();
    @Override
    public void executeUpdatesInTransaction(Collection<String> stmts) throws DataSourceException {
        stmtCollection.add(stmts);
    }

    public Collection<Collection<String>> getStmtCollection() {
        return stmtCollection;
    }

    public void reset() {
        stmtCollection.clear();
    }
}
