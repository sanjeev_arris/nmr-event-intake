package com.arris.nmra.config;

import com.arris.nmra.service.DBService;
import com.arris.nmra.service.MockDBService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.config.CronTask;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by smishra on 12/22/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class DataAggregatorTest {
    private static final Logger logger = LoggerFactory.getLogger(DataAggregatorTest.class);

    private MockDBService mockDBService = new MockDBService();

    @Autowired
    private DBService dbService;
    @Autowired
    private DataAggregator dataAggregator;

    @Before
    public void setUp() {
        dataAggregator.dbService = mockDBService;
    }

    @After
    public void tearDown() {
        dataAggregator.dbService = dbService;
    }

    @Test
    public void cronTasksShouldBeConfiguredProperly() throws InterruptedException {
        Map<String, Collection<CronTask>> cronTaskMap = dataAggregator.cronTaskMap;

        Assert.assertEquals(2, cronTaskMap.size());
        Collection<CronTask> cronTasks = cronTaskMap.values().iterator().next();
        Assert.assertEquals(1, cronTasks.size());

        cronTaskMap.entrySet().forEach(e -> logger.info(String.format("Key %s -> Expression %s", e.getKey(), e.getValue().stream().map(t -> t.getExpression()).collect(Collectors.joining(",")))));
        Assert.assertTrue(mockDBService.getStmtCollection().isEmpty());
        cronTasks.iterator().next().getRunnable().run();
        Assert.assertFalse(mockDBService.getStmtCollection().isEmpty());
        Collection<String> stmts = mockDBService.getStmtCollection().iterator().next();
        Assert.assertEquals(2, stmts.size());
        stmts.forEach(s -> Assert.assertTrue(s.indexOf("__CREATED_AT__")<0));
    }
}
