package com.arris.nmra.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;

/**
 * Created by smishra on 12/22/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class ScheduledQueryTest {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledQueryTest.class);

    @Autowired
    private AggregationConfig aggregationConfig;

    @Test
    public void aggregationQueriesShouldBeDefined() {
        Assert.assertFalse(aggregationConfig.getQueries().isEmpty());
        aggregationConfig.getQueries().forEach(q -> Assert.assertFalse(q.getQueries().isEmpty()));
        aggregationConfig.getQueries().stream().flatMap(q -> q.getQueries().stream()).forEach(q -> Assert.assertNotNull(q.getQuery()));
        int queryCount = aggregationConfig.getQueries().stream().flatMap(q -> q.getQueries().stream()).collect(Collectors.toList()).size();
        Assert.assertEquals(5, queryCount);
        logger.info(aggregationConfig.getQueries().stream().flatMap(q -> q.getQueries().stream()).map(q -> q.getQuery()).collect(Collectors.toList()).toString());
    }
}
