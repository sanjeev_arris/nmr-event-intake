package com.arris.nmra;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class ImportFromQTest {
    private static final Logger log = LoggerFactory.getLogger(ImportFromQTest.class);

    @Autowired
    private RabbitTemplate template;
    String testOutDir = System.getProperty("user.dir") + "/testOutput/";

    @Before
    public void setUp() {
        File file = new File(testOutDir);
        file.mkdir();
    }

    @Test
    public void testTopology() throws IOException {
        String queue = "q.sa.topology.longterm";
        fetchQueueContent(queue, 10);
    }

    @Test
    public void testDownstreamStats() throws IOException {
        String queue = "q.sa.downstream.stats.longterm";
        fetchQueueContent(queue, 10);
    }

    @Test
    public void testDownstreamTiers() throws IOException {
        String queue = "q.sa.downstream.tiers.longterm";
        fetchQueueContent(queue, 10);
    }

    @Test
    public void testUpstreamStats() throws IOException {
        String queue = "q.sa.upstream.stats.longterm";
        fetchQueueContent(queue, 10);
    }

    @Test
    public void getTestUpstreamTiers() throws IOException {
        String queue = "q.sa.upstream.tiers.longterm";
        fetchQueueContent(queue, 10);
    }

    public void fetchQueueContent(String queue, int count) throws IOException {
        Message message;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int curCount = 0;
        while ((message = template.receive(queue)) != null) {
            try {
                baos.write(message.getBody());
                baos.write('\n');
            } catch (IOException e) {
                log.warn("Error writing to stream", e);
            }
            if (++curCount == count) break;
        }

        byte[] content = baos.toByteArray();
        if (content.length > 0) {
            String fileName = testOutDir + queue + "_" + System.currentTimeMillis() + ".json";
            File file = new File(fileName);
            Assert.assertFalse(file.exists());
            FileOutputStream faos = new FileOutputStream(fileName);
            faos.write(content);
            faos.flush();
            faos.close();

            FileInputStream fis = new FileInputStream(fileName);
            Assert.assertTrue(fis.available() == baos.size());
            fis.close();
        }
    }
}
