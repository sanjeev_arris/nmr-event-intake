package com.arris.nmra.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by smishra on 1/11/17.
 */
public class UtilizationNodeTest {
    @Test
    public void shouldBeAbleToWriteAndReadNodes() {
        Collection<UtilizationNode> nodes = new ArrayList<>();
        UtilizationNode n1 = createNode("New England", UtilizationNode.NodeType.market, 10);
    }

    @Test
    public void givenTwoUtilizationsWhenMergedThenItShouldResultMergedUtilization() {
        UtilizationNode.Utilization first = new UtilizationNode.Utilization();
        first.avgUtil = 1;
        first.overCapacityCount = 5;
        first.overCapacityPercent = 10;
        first.serviceGroups = 10;

        UtilizationNode.Utilization second = new UtilizationNode.Utilization();
        second.avgUtil = 5;
        second.overCapacityCount = 7;
        second.overCapacityPercent = 2;
        second.serviceGroups = 2;

        UtilizationNode.Utilization merged = UtilizationNode.merge(first, second);
        Assert.assertEquals(2, merged.count);
        Assert.assertEquals((first.overCapacityCount+second.overCapacityCount), merged.overCapacityCount, .0001);
        Assert.assertEquals((first.serviceGroups+second.serviceGroups), merged.serviceGroups, .0001);

        Assert.assertEquals((first.avgUtil+second.avgUtil)/2, merged.avgUtil, .0001);
        Assert.assertEquals((first.overCapacityPercent+second.overCapacityPercent)/2, merged.overCapacityPercent, .0001);
    }

    @Test
    public void givenUtilizationsWhenMergedThenItShouldResultMergedUtilization() {
        UtilizationNode.Utilization first = new UtilizationNode.Utilization();
        first.avgUtil = 1;
        first.overCapacityCount = 1;
        first.overCapacityPercent = 1;
        first.serviceGroups = 1;

        UtilizationNode.Utilization second = new UtilizationNode.Utilization();
        second.avgUtil = 2;
        second.overCapacityCount = 2;
        second.overCapacityPercent = 2;
        second.serviceGroups = 2;

        UtilizationNode.Utilization third = new UtilizationNode.Utilization();
        third.avgUtil = 3;
        third.overCapacityCount = 3;
        third.overCapacityPercent = 3;
        third.serviceGroups = 3;

        UtilizationNode.Utilization fourth = new UtilizationNode.Utilization();
        fourth.avgUtil = 4;
        fourth.overCapacityCount = 4;
        fourth.overCapacityPercent = 4;
        fourth.serviceGroups = 4;

        UtilizationNode.Utilization fifth = new UtilizationNode.Utilization();
        fifth.avgUtil = 5;
        fifth.overCapacityCount = 5;
        fifth.overCapacityPercent = 5;
        fifth.serviceGroups = 5;

        UtilizationNode.Utilization exp = new UtilizationNode.Utilization();
        exp.count =5;
        exp.avgUtil = (1+2+3+4+5)/5;
        exp.overCapacityCount = 1+2+3+4+5;
        exp.overCapacityPercent = (1+2+3+4+5)/5;
        exp.serviceGroups = 1+2+3+4+5;


        UtilizationNode.Utilization merged = UtilizationNode.merge(first, second);
        merged = UtilizationNode.merge(merged, third);
        merged = UtilizationNode.merge(merged, fourth);
        merged = UtilizationNode.merge(merged, fifth);

        Assert.assertEquals(exp.count, merged.count);
        Assert.assertEquals(exp.overCapacityCount, merged.overCapacityCount);
        Assert.assertEquals(exp.serviceGroups, merged.serviceGroups);

        Assert.assertEquals(exp.avgUtil, merged.avgUtil, .0001);
        Assert.assertEquals(exp.overCapacityPercent, merged.overCapacityPercent, .0001);
    }

    @Test
    public void givenChildUtilizationNodesWhenProjectedThenItShouldResultMergedUtilization() {
        UtilizationNode parent = new UtilizationNode();

        for (int i=1; i<=5; ++i) {
            parent.children.add(createUtilizationNode(i));
        }

        UtilizationNode.Utilization exp = new UtilizationNode.Utilization();
        exp.count =5;
        exp.avgUtil = (1+2+3+4+5)/5;
        exp.overCapacityCount = 1+2+3+4+5;
        exp.overCapacityPercent = (1+2+3+4+5)/5;
        exp.serviceGroups = 1+2+3+4+5;

        UtilizationNode.Utilization merged = parent.getUpstreamProjected();

        Assert.assertEquals(exp.count, merged.count);
        Assert.assertEquals(exp.overCapacityCount, merged.overCapacityCount);
        Assert.assertEquals(exp.serviceGroups, merged.serviceGroups);

        Assert.assertEquals(exp.avgUtil, merged.avgUtil, .0001);
        Assert.assertEquals(exp.overCapacityPercent, merged.overCapacityPercent, .0001);
    }

    @Test
    public void givenUtilizationTreeWhenProjectedThenItShouldResultMergedUtilization() {
        UtilizationNode root = new UtilizationNode();
        UtilizationNode firstChild = new UtilizationNode();
        UtilizationNode secondChild = new UtilizationNode();
        root.children.add(firstChild);
        root.children.add(secondChild);

        for (int i=1; i<=2; ++i) {
            firstChild.children.add(createUtilizationNode(i));
        }

        for (int i=3; i<=5; ++i) {
            secondChild.children.add(createUtilizationNode(i));
        }

        UtilizationNode.Utilization exp = new UtilizationNode.Utilization();
        exp.count =5;
        exp.avgUtil = (1+2+3+4+5)/5;
        exp.overCapacityCount = 1+2+3+4+5;
        exp.overCapacityPercent = (1+2+3+4+5)/5;
        exp.serviceGroups = 1+2+3+4+5;
        
        UtilizationNode.Utilization merged = root.getUpstreamProjected();

        Assert.assertEquals(exp.count, merged.count);
        Assert.assertEquals(exp.overCapacityCount, merged.overCapacityCount);
        Assert.assertEquals(exp.serviceGroups, merged.serviceGroups);

        Assert.assertEquals(exp.avgUtil, merged.avgUtil, .0001);
        Assert.assertEquals(exp.overCapacityPercent, merged.overCapacityPercent, .0001);

        assertValid(firstChild, 2, 1.5, (1+2), 1.5, (1+2));
        assertValid(secondChild, 3, (3+4+5)/3, (3+4+5), (3+4+5)/3, (3+4+5));
    }

    private void assertValid(UtilizationNode node, int count, double avgUtil, int capacityCount, double capacityPercentage, int sgCount) {
        UtilizationNode.Utilization exp = new UtilizationNode.Utilization();
        exp.count = count;
        exp.avgUtil = avgUtil;
        exp.overCapacityCount = capacityCount;
        exp.overCapacityPercent = capacityPercentage;
        exp.serviceGroups = sgCount;

        UtilizationNode.Utilization secondChildMerged = node.getUpstreamProjected();

        Assert.assertEquals(exp.count, secondChildMerged.count);
        Assert.assertEquals(exp.overCapacityCount, secondChildMerged.overCapacityCount);
        Assert.assertEquals(exp.serviceGroups, secondChildMerged.serviceGroups);

        Assert.assertEquals(exp.avgUtil, secondChildMerged.avgUtil, .0001);
        Assert.assertEquals(exp.overCapacityPercent, secondChildMerged.overCapacityPercent, .0001);
    }

    private UtilizationNode createUtilizationNode(int i) {
        UtilizationNode cnode = new UtilizationNode();

        UtilizationNode.Utilization child = new UtilizationNode.Utilization();
        child.avgUtil = i;
        child.overCapacityCount = i;
        child.overCapacityPercent = i;
        child.serviceGroups = i;
        cnode.setUpstreamProjected(child);
        return cnode;
    }

    private UtilizationNode createNode(String name, UtilizationNode.NodeType nodeType, int cms) {
        //UtilizationNode node = new UtilizationNode(name, nodeType, path, cms);
        return null;
    }
}
