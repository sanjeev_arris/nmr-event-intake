package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by smishra on 11/10/16.
 */
public class StatsDataTest {
    private static final Logger log = LoggerFactory.getLogger(StatsDataTest.class);
    @Test
    public void givenDownstreamJsonFileTierDataShouldBeRead() throws IOException {
        String fileName = "data/downstreamStats.json";
        ObjectMapper mapper = new ObjectMapper();
        StatsData[] array = mapper.readValue(Util.bytesFromResource(fileName), StatsData[].class);
        Assert.assertNotNull(array);
        Assert.assertTrue(array.length>0);

        byte[] bytes = mapper.writeValueAsBytes(array);
        log.debug(new String(bytes));
        StatsData[] converted = mapper.readValue(bytes, StatsData[].class);
        Assert.assertEquals(array.length, converted.length);
        for (int i=0; i<converted.length;++i) {
            Assert.assertEquals(array[i].getCableMac(), converted[i].getCableMac());
            Assert.assertTrue(array[i].getTs()!=null && array[i].getTs()>0);
            Assert.assertEquals(array[i].getTs(), converted[i].getTs());
        }
    }

    @Test
    public void givenUpstreamJsonFileTierDataShouldBeRead() throws IOException {
        String fileName = "data/upstreamStats.json";
        ObjectMapper mapper = new ObjectMapper();
        StatsData[] array = mapper.readValue(Util.bytesFromResource(fileName), StatsData[].class);
        Assert.assertNotNull(array);
        Assert.assertTrue(array.length>0);

        byte[] bytes = mapper.writeValueAsBytes(array);
        log.debug(new String(bytes));
        StatsData[] converted = mapper.readValue(bytes, StatsData[].class);
        Assert.assertEquals(array.length, converted.length);
        for (int i=0; i<converted.length;++i) {
            Assert.assertEquals(array[i].getCableMac(), converted[i].getCableMac());
            Assert.assertTrue(array[i].getTs()!=null && array[i].getTs()>0);
            Assert.assertEquals(array[i].getTs(), converted[i].getTs());
        }
    }
}
