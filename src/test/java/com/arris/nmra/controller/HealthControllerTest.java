package com.arris.nmra.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * Created by smishra on 12/9/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HealthControllerTest {
    private static final Logger logger = LoggerFactory.getLogger(HealthControllerTest.class);
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void systemInfoShouldBeReturned() {
        ResponseEntity<Map> resp = restTemplate.getForEntity("/status/system", Map.class);
        Assert.assertTrue(resp.getStatusCode().value()==200);
        logger.debug("system info: " + resp.getBody());
        Assert.assertFalse(resp.getBody().isEmpty());
    }
}
