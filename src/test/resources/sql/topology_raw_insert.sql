if not exists (select * from dbo.topology_raw where market = '$MARKET' AND hub = '$HUB' AND cmts = '$CMTS')
    INSERT INTO dbo.topology_raw ()
  create table dbo.topology_raw (
    id BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    tenant_id INT not null,
    ts DATETIMEOFFSET not null,
    market varchar(32) not null,
    hub varchar(32) not null,
    cmts VARCHAR(64) not null,
    year INT not null,
    month INT not null,
    day INT not null,
    hour INT not null,
    minute INT not null,
    created_at bigint not null,
    constraint fk_topology_raw_tenant foreign key (tenant_id) references dbo.tenant (id)
  );

