IF not exists (
    select * from topology
    where tenant_id = (select id from tenant where name = __TENANT) AND market= __MARKET AND hub = __HUB AND cmts = __CMTS)
      INSERT INTO topology (tenant_id, market, hub, cmts, statsTs, created_at) VALUES (__TENANT_ID, __MARKET, __HUB, __CMTS, __CREATED_AT)