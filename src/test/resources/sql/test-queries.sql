-- drop TABLE stats_raw;
-- drop TABLE tiers_raw;
-- drop table topology_raw;

select top 1 * from stats_raw;
-- TRUNCATE TABLE tiers

select max(id) - min(id) FROM tiers_raw;
select max(id) - min(id) from stats_raw;

-- drop TABLE ch_stats_raw;
-- TRUNCATE TABLE ch_stats;
-- TRUNCATE TABLE sg_stats;
-- drop TABLE sg_stats_raw;


select count(*) from topology;
select count(*) from tiers_raw;
select count(*) from tiers;
select count(*), created_at FROM sg_stats_raw GROUP BY created_at;
select top 10 * from sg_stats_raw;
select top 100 * from ch_stats_raw;



select count(*)
from ch_stats_raw ch join
  sg_stats_raw sg on
                    (ch.sg_id = sg.sg_id and ch.cable_mac = sg.cable_mac and ch.cmts = sg.cmts and
                     ch.year = sg.year and sg.month = ch.month and sg.day = ch.day and sg.hour=ch.hour and sg.minute =ch.minute and sg.downstream = ch.downstream)
WHERE ch.downstream =0;

select count(*)
from ch_stats_raw ch join
  sg_stats_raw sg on
                    (ch.sg_id = sg.sg_id and ch.cable_mac = sg.cable_mac and ch.cmts = sg.cmts and
                     sg.created_at = ch.created_at and sg.downstream = ch.downstream)
WHERE ch.downstream =0;


select count(*) from sg_stats_raw;

select cmts sg_cmts, cable_mac from sg_stats_raw GROUP BY cmts, cable_mac ORDER BY cmts, cable_mac;
select cmts, cable_mac from tiers_raw GROUP BY cmts, cable_mac ORDER BY cmts, cable_mac;

select cmts FROM sg_stats_raw sg
 EXCEPT
SELECT cmts FROM tiers_raw;

SELECT DISTINCT cmts FROM sg_stats_raw where cmts not in (SELECT cmts FROM tiers_raw);

SELECT DISTINCT sg.cmts, sg.cable_mac, t.id as t_id from sg_stats_raw sg left JOIN tiers_raw t on (sg.cmts=t.cmts and sg.cable_mac=t.cable_mac) WHERE t.id is null

SELECT DISTINCT cmts, cable_mac FROM sg_stats_raw where cable_mac not in (SELECT tiers_raw.cable_mac FROM tiers_raw);

(select cmts, cable_mac FROM sg_stats_raw sg
EXCEPT
  SELECT cmts, cable_mac FROM tiers_raw) ORDER BY cmts, cable_mac







select DISTINCT cmts from tiers_raw GROUP BY cmts ORDER BY cmts;

SELECT sum(*) FROM topology GROUP BY cmts;

select count(*) FROM sg_stats_raw sg JOIN tiers_raw t on (sg.cmts = t.cmts and sg.tenant_id = t.tenant_id AND sg.cable_mac = t.cable_mac)

select sg.cmts, sg.cable_mac, sg.sg_id, ch.channel, sg.year, sg.month, sg.day, sg.hour, sg.minute, sg.sg_kbps, ch.ch_kbps
from ch_stats_raw ch join
  sg_stats_raw sg on
                    (ch.sg_id = sg.sg_id and ch.cable_mac = sg.cable_mac and ch.cmts = sg.cmts and
                     ch.year = sg.year and sg.month = ch.month and sg.day = ch.day and sg.hour=ch.hour and sg.minute =ch.minute and sg.downstream = ch.downstream)
WHERE ch.downstream =1 and sg_kbps>0;

select sg.cmts, sg.cable_mac, sg.sg_id, ch.channel, sg.year, sg.month, sg.day, sg.hour, sg.minute, sg.sg_kbps, ch.ch_kbps
from ch_stats_raw ch join
  sg_stats_raw sg on
                    (ch.sg_id = sg.sg_id and ch.cable_mac = sg.cable_mac and ch.cmts = sg.cmts and
                     sg.created_at =ch.created_at and sg.downstream = ch.downstream)
WHERE ch.downstream =1 and sg_kbps>0;


select sg.cmts, sg.cable_mac, sg.sg_id, ch.channel, sg.downstream, sg.ts
from ch_stats_raw ch join
  sg_stats_raw sg on
                    (ch.sg_id = sg.sg_id and ch.cable_mac = sg.cable_mac and ch.cmts = sg.cmts and
                     ch.year = sg.year and sg.month = ch.month and sg.day = ch.day and sg.hour=ch.hour and sg.minute =ch.minute and sg.downstream = ch.downstream)


select count(*) from tiers_raw tr INNER join (
    select t.id, t.cmts from topology t
    where t.cmts = tr.cmts and t.year<= tr.year and t.month <= tr.month and t.day <= tr.day and t.hour <= tr.hour and t.minute <= tr.minute
    order by t.year DESC, t.month DESC , t.day DESC , t.hour DESC, t.minute DESC
  ) ss on tr.cmts = ss.cmts;

select t.cmts, t.year, t.month, t.day t_day, tr.day tr_day, t.minute, t.hour, tr.hour tr_hour, tr.minute tr_minute from tiers_raw tr join topology t on (
  t.cmts = tr.cmts and t.year <= tr.year and t.month <= tr.month and t.day <= tr.day
  )


select count(*) from tiers_raw tr join topology t on (
  t.cmts = tr.cmts and t.year <= tr.year and t.month <= tr.month and (t.day >= tr.day OR t.day <tr.day)
  )


select count(*) from tiers_raw tr JOIN topology t on (tr.cmts = t.cmts and t.year=tr.year and tr.month=t.month and tr.day=t.day and tr.hour=t.hour);

select count(*) from dbo.stats_raw where sg_pct_util=0 and ch_kbps=0;


select s.tenant_id, s.downstream, s.cmts, s.cable_mac, s.sg_id, s.sg_pct_util, s.sg_kbps, tmax, s.year, s.month, s.day, s.hour
from dbo.stats_raw as s inner join
  (select top 1 max(tmax) as tmax
   from dbo.tiers_raw
   where tenant_id =s.tenant_id and downstream = s.downstream and cmts = s.cmts and cable_mac = s.cable_mac and year = s.year and month = s.month and day=s.day ORDER BY hour desc) t on 1=1;


select cmts, cable_mac, channel, sg_pct_util, sg_kbps, ch_pct_util, ch_kbps from stats_raw where sg_pct_util>0 and downstream=1 ORDER BY cmts, cable_mac

select count(*) from stats_raw;

INSERT into dbo.sg_stats (tier_id, downstream, cable_mac, sg_id, sg_kbps, sg_pct_util, sg_max_cms, sg_max_online, year, month, day, hour, minute, tmax, stats_ts, created_at)
  (select tp.id tier_id, s.downstream, s.cable_mac,
     s.sg_id, s.sg_pct_util, s.sg_kbps, s.sg_max_cms, s.sg_max_online,
     s.year, s.month, s.day, s.hour, s.minute,
          (select top 1 t.tmax
           from dbo.tiers t
           where t.downstream = s.downstream and
                 t.cable_mac = s.cable_mac and t.year = s.year and t.month = s.month and
                 (t.day=s.day or t.day < s.day or t.day+1=s.day)
           ORDER BY tmax DESC, day DESC) as tmax,
     s.ts, SYSDATETIMEOFFSET() created_at);


SELECT sg.id, sg.sg_id, count(ch.id) num_ch, sg.stats_ts
from sg_stats sg JOIN ch_stats ch on (sg.id = ch.sg_id and sg.stats_ts=ch.stats_ts)
GROUP BY sg.id, sg.sg_id, sg.stats_ts;

select count(*) from sg_stats_raw;

select count(*) from
  (select tp.id topology_id, s.downstream, s.cable_mac,
     s.sg_id, s.sg_pct_util, s.sg_kbps, s.sg_max_cms, s.sg_max_online,
  s.year, s.month, s.day, s.hour, s.minute,
  (select top 1 t.id
    from dbo.tiers t
    where t.downstream = s.downstream and
         t.cable_mac = s.cable_mac and t.year = s.year and t.month = s.month and
         (t.day=s.day or t.day < s.day or t.day+1=s.day)
    ORDER BY tmax DESC, day DESC) as tmax,
     s.ts, SYSDATETIMEOFFSET() created_at
FROM sg_stats_raw s JOIN topology tp on s.tenant_id = tp.tenant_id and s.cmts = tp.cmts) as x;


select count(*) FROM sg_stats_raw;
select count(*) from
  (select s.downstream, s.cable_mac,
     s.sg_id, s.sg_pct_util, s.sg_kbps, s.sg_max_cms, s.sg_max_online,
     s.year, s.month, s.day, s.hour, s.minute,
          (select top 1 t.id
           from dbo.tiers t JOIN topology tp on (tp.cmts = s.cmts)
           where t.downstream = s.downstream and
                 t.cable_mac = s.cable_mac and t.year = s.year and t.month = s.month and
                 (t.day=s.day or t.day < s.day or t.day+1=s.day)
           ORDER BY tmax DESC, t.day DESC) as tier_id,
     s.ts, -1 created_at
   FROM sg_stats_raw s) as x;

select * from sg_stats_raw s ORDER BY s.cable_mac, s.month, s.day, s.hour;



SELECT DISTINCT topology_id, cable_mac FROM tiers;

select * FROM topology WHERE cmts = 'ciscoSCH1WithModular';
select * FROM topology WHERE cmts = 'Cisco3g60Secondary';

SELECT t.id, t.cable_mac, tmax, tp.cmts from tiers t JOIN topology tp on t.topology_id=tp.id
WHERE tp.cmts in ('Cisco3g60Secondary', 'ciscoSCH1WithModular') ORDER BY cable_mac;


SELECT * FROM sg_stats_raw WHERE sg_kbps>0 ORDER BY cable_mac, month, day, hour
SELECT * from sg_stats WHERE sg_kbps>0 ORDER BY cable_mac, month, day, hour

SELECT * FROM ch_stats_raw WHERE ch_kbps>0 ORDER BY channel, sg_id, month, day, hour
SELECT * from ch_stats WHERE ch_kbps>0 ORDER BY channel, month, day, hour

CREATE VIEW dbo.vw_sg_usage_downstream as
  select tp.cmts, tr.cable_mac, sg.sg_id, tr.tmax, sg.sg_kbps, sg.sg_pct_util, sg.sg_max_cms, sg.sg_max_online, sg.stats_ts
  FROM sg_stats sg JOIN tiers tr on (sg.tier_id=tr.id) JOIN topology tp on (tp.id =tr.topology_id)
WHERE sg.downstream =1;

CREATE VIEW dbo.vw_sg_usage_upstream as select tp.cmts, tr.cable_mac, sg.sg_id, tr.tmax, sg.sg_kbps, sg.sg_pct_util, sg.sg_max_cms, sg.sg_max_online, sg.stats_ts  FROM sg_stats sg JOIN tiers tr on (sg.tier_id=tr.id) JOIN topology tp on (tp.id =tr.topology_id)
WHERE sg.downstream =0;



select count(*)+27 FROM vw_sg_usage_downstream;
SELECT count(*) FROM sg_stats WHERE downstream=1;

select count(*)+80 FROM vw_sg_usage_upstream;
SELECT count(*) FROM sg_stats WHERE downstream=0;


select count(*) from vw_sg_usage_downstream WHERE tmax is null;
select count(*) FROM sg_stats WHERE downstream=0 and tier_id is null;

insert into dbo.tiers_tmp (topology_id, cable_mac, tmax, created_at) select topology_id, cable_mac, tmax, created_at from tiers;

DROP TABLE tiers_tmp;
select * into dbo.tiers_tmp from tiers_raw;

DROP TABLE tiers_tmp_1;
select topology_id, cable_mac, tmax into dbo.tiers_tmp_1 from tiers;

DELETE FROM tiers_tmp
OUTPUT DELETED.topology_id, DELETED.cable_mac, DELETED.tmax INTO dbo.tiers_tmp_1;

SELECT count(*) FROM tiers_tmp_1;
SELECT count(*) FROM tiers_tmp;


--create TABLE dbo.tiers_tmp (topology_id, cable_mac, tmax, created_at) select topology_id, cable_mac, tmax, created_at from tiers;


INSERT INTO sg_stats (downstream, cmts, cable_mac, sg_id, sg_pct_util, sg_kbps, sg_max_cms, sg_max_online, year, month, day, hour, minute, tier_id, stats_ts, created_at)
  select s.downstream, s.cmts, s.cable_mac, s.sg_id, s.sg_pct_util, s.sg_kbps, s.sg_max_cms, s.sg_max_online,
    s.year, s.month, s.day, s.hour, s.minute,
          (select top 1 t.id
           from dbo.tiers t JOIN topology tp on (s.cmts = tp.cmts)
           where s.cmts = tp.cmts and t.downstream = s.downstream and
                 t.cable_mac = s.cable_mac and t.year = s.year and t.month = s.month and
                 (t.day=s.day or t.day < s.day or t.day+1=s.day)
           ORDER BY t.day DESC, tmax DESC) as tier_id,
    s.ts, -1 created_at
  FROM sg_stats_raw s
  WHERE s.created_at >= (SELECT max(created_at) from sg_stats);