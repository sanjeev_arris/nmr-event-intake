INSERT INTO topology (tenant_id, market, hub, cmts, year, month, day, hour, minute, stats_ts, created_at)
  SELECT (SELECT id from tenant where name = 'demo'), 'dummy', 'dummy', 'dummy', 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (SELECT id from topology)
  ;
