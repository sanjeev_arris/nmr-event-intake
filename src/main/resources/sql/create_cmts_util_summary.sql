INSERT INTO cmts_util_summary
  SELECT t.tenant_id, t.id topology_id, t.market, t.hub, sg.cmts, sum(sgs.cm_count) cm_count,
                      count(sg.sg_id) ds_sg_count, avg(ds_curr) ds_avg_util,
    --avg(ds_w1) avg_ds_w1, avg(ds_w4) avg_ds_w4, avg(ds_w12) avg_ds_w12, avg(ds_w26) avg_ds_w26,
    -- sum(ds_curr_over) total_ds_curr_over,
    -- sum(ds_w1_over) total_ds_w1_over, sum(ds_w4_over) total_ds_w4_over, sum(ds_w12_over) total_ds_w12_over,
                      sum(ds_w26_over) ds_over_cap_count,
                      (sum(ds_w26_over)*1.0/count(sg.sg_id))*100 ds_over_cap_perc,
                      count(sg.sg_id) us_sg_count, avg(us_curr) us_avg_util,
    --avg(us_w1) avg_us_w1, avg(us_w4) avg_us_w4, avg(us_w12) avg_us_w12, avg(us_w26) avg_us_w26,
    -- sum(us_curr_over) total_us_curr_over, sum(us_w1_over) total_us_w1_over, sum(us_w4_over) total_us_w4_over,sum(us_w12_over) total_us_w12_over,
                      sum(us_w26_over) us_over_cap_count,
                      (sum(us_w26_over)*1.0/count(sg.sg_id))*100 us_over_cap_perc,
                      -1 year, -1 month, -1 day, -1 created_as
  FROM (
      (SELECT sg_id, curr ds_curr, w_1 ds_w1, w_4 ds_w4, w_12 ds_w12, w_26 ds_w26,
                     CASE WHEN (curr > 100) THEN 1 ELSE 0 END as ds_curr_over,
                     CASE WHEN (w_1 > 100) THEN 1 ELSE 0 END as ds_w1_over,
                     CASE WHEN (w_4 > 100) THEN 1 ELSE 0 END as ds_w4_over,
                     CASE WHEN (w_12 > 100) THEN 1 ELSE 0 END as ds_w12_over,
                     CASE WHEN (w_26 > 100) THEN 1 ELSE 0 END as ds_w26_over
       FROM req_cap
       WHERE downstream =1) ds
      JOIN
      (SELECT sg_id, curr us_curr, w_1 us_w1, w_4 us_w4, w_12 us_w12, w_26 us_w26,
                     CASE WHEN (curr > 100) THEN 1 ELSE 0 END as us_curr_over,
                     CASE WHEN (w_1 > 100) THEN 1 ELSE 0 END as us_w1_over,
                     CASE WHEN (w_4 > 100) THEN 1 ELSE 0 END as us_w4_over,
                     CASE WHEN (w_12 > 100) THEN 1 ELSE 0 END as us_w12_over,
                     CASE WHEN (w_26 > 100) THEN 1 ELSE 0 END as us_w26_over
       FROM req_cap r
       WHERE downstream =0) us ON (ds.sg_id = us.sg_id))
    JOIN (SELECT sg_id, max(sg_max_cms) cm_count from sg_stats GROUP BY sg_id) sgs on (sgs.sg_id = ds.sg_id)
    JOIN sg on (sg.id = sgs.sg_id)
    JOIN topology t on (sg.cmts = t.cmts)
  GROUP BY t.tenant_id, t.id, t.market, t.hub, sg.cmts
--ORDER BY t.market asc, t.hub asc, sg.cmts asc






--
--
-- INSERT INTO cmts_util_summary
--   SELECT t.tenant_id, t.id topology_id, t.market, t.hub, sg.cmts, sum(sgs.cm_count) cm_count,
--                       count(sg.sg_id) ds_sg_count, avg(ds_curr) ds_avg_util,
--                       sum(ds_w26_over) ds_over_cap_count,
--                       (sum(ds_w26_over)*1.0/count(sg.sg_id))*100 ds_over_cap_perc,
--                       count(sg.sg_id) us_sg_count, avg(us_curr) us_avg_util,
--                       sum(us_w26_over) us_over_cap_count,
--                       (sum(us_w26_over)*1.0/count(sg.sg_id))*100 us_over_cap_perc,
--                       -1 year, -1 month, -1 day, -1 created_as
--   FROM (
--       (SELECT sg_id, curr ds_curr, w_1 ds_w1, w_4 ds_w4, w_12 ds_w12, w_26 ds_w26,
--                      CASE WHEN (curr > 100) THEN 1 ELSE 0 END as ds_curr_over,
--                      CASE WHEN (w_1 > 100) THEN 1 ELSE 0 END as ds_w1_over,
--                      CASE WHEN (w_4 > 100) THEN 1 ELSE 0 END as ds_w4_over,
--                      CASE WHEN (w_12 > 100) THEN 1 ELSE 0 END as ds_w12_over,
--                      CASE WHEN (w_26 > 100) THEN 1 ELSE 0 END as ds_w26_over
--        FROM req_cap
--        WHERE downstream =1) ds
--       JOIN
--       (SELECT sg_id, curr us_curr, w_1 us_w1, w_4 us_w4, w_12 us_w12, w_26 us_w26,
--                      CASE WHEN (curr > 100) THEN 1 ELSE 0 END as us_curr_over,
--                      CASE WHEN (w_1 > 100) THEN 1 ELSE 0 END as us_w1_over,
--                      CASE WHEN (w_4 > 100) THEN 1 ELSE 0 END as us_w4_over,
--                      CASE WHEN (w_12 > 100) THEN 1 ELSE 0 END as us_w12_over,
--                      CASE WHEN (w_26 > 100) THEN 1 ELSE 0 END as us_w26_over
--        FROM req_cap r
--        WHERE downstream =0) us ON (ds.sg_id = us.sg_id))
--     JOIN (SELECT sg_id, max(sg_max_cms) cm_count from sg_stats GROUP BY sg_id) sgs on (sgs.sg_id = ds.sg_id)
--     JOIN sg on (sg.id = sgs.sg_id)
--     JOIN topology t on (sg.cmts = t.cmts)
--   GROUP BY t.tenant_id, t.id, t.market, t.hub, sg.cmts
