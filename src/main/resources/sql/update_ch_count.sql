WITH tmp AS
(SELECT sg_stats_id, count(*) ch_count
 FROM ch_stats
 WHERE stats_ts > (SELECT MIN(stats_ts) FROM sg_stats where ch_count =-1) GROUP BY sg_stats_id
)
UPDATE sg_stats
SET ch_count = tmp.ch_count
FROM sg_stats as sg INNER JOIN tmp ON sg.id = tmp.sg_stats_id
WHERE sg.ch_count =-1
