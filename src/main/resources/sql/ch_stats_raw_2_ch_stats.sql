INSERT INTO ch_stats (sg_stats_id, downstream, channel, ch_pct_util, ch_kbps, ch_max_cms, ch_max_online,
                      year, month, wk, day_of_wk, day, hour, minute, stats_ts, created_at)
  SELECT sgs.id, ch.downstream, ch.channel, ch.ch_pct_util, ch.ch_kbps, ch.ch_max_cms, ch.ch_max_online,
    ch.year, ch.month, ch.wk, ch.day_of_wk, ch.day, ch.hour, ch.minute, ch.ts, __CREATED_AT__
  FROM ch_stats_raw ch
  JOIN sg_stats sgs on (ch.ts = sgs.stats_ts)
    JOIN sg on (sgs.sg_id = sg.id and ch.downstream=sg.downstream AND ch.cmts = sg.cmts AND ch.cable_mac = sg.cable_mac AND ch.sg_id = sg.sg_id)
  WHERE ch.created_at >=  (SELECT max(created_at) FROM ch_stats)
