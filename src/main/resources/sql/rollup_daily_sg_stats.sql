INSERT INTO sg_stats_daily (sg_id, downstream,
                                peak_pct_util, avg_pct_util, stdev_pct_util,
                                peak_kbps, avg_kbps, stdev_kbps,
                                peak_cms, avg_cms, stdev_cms,
                                peak_cms_online, avg_cms_online, stdev_cms_online,
                                peak_ch_count, avg_ch_count, stdev_ch_count,
                                year, month, day, wk, day_of_wk, created_at
)
  SELECT sg_id, downstream,
    max(sg_pct_util) peak_pct_util, avg(sg_pct_util) avg_pct_util, stddev(sg_pct_util) stdev_pct_util,
    max(sg_kbps) peak_kbps, avg(sg_kbps) avg_kbps, stddev(sg_kbps) stdev_kbps,
    max(sg_max_cms) peak_cms, avg(sg_max_cms) avg_cms, stddev(sg_max_cms) stdev_cms,
    max(sg_max_online) peak_cms_online, avg(sg_max_online) avg_cms_online, stddev(sg_max_online) stdev_cms_online,
    max(ch_count) peak_ch_count, avg(ch_count) avg_ch_count, stddev(ch_count) stdev_ch_count,
    year, month, day, wk, day_of_wk, __CREATED_AT__
  FROM sg_stats
  WHERE created_at >= (SELECT max(created_at) from sg_stats_daily)
  GROUP BY sg_id, downstream, year, month, wk, day

