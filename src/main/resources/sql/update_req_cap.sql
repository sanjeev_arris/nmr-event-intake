INSERT INTO req_cap (sg_id, model_id, downstream, curr, w_1, w_4, w_12, w_26, year, month, day, created_at)
  SELECT m.sg_id, m.id as model_id, m.downstream,
        m.intercept+ m.slope*(m.num_samples+0) as curr,
        m.intercept+ m.slope*(m.num_samples+1) as w_1,
        m.intercept+ m.slope*(m.num_samples+4) as w_4,
        m.intercept+ m.slope*(m.num_samples+12) as w_12,
        m.intercept+ m.slope*(m.num_samples+26) as w_26,
        m.year, m.month, m.day_of_wk, m.day, __CREATED_AT__ as created_at
  FROM node_split_model as m
  WHERE m.created_at > (select max(created_at) FROM req_cap)