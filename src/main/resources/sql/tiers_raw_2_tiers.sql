INSERT INTO tiers (topology_id, cable_mac, tmax, downstream, year, month, day_of_wk, day, hour, minute, stats_ts, created_at)
  select t.id as topology_id, tr.cable_mac, tr.tmax, tr.downstream, tr.year, tr.month, tr.day_of_wk, tr.day, tr.hour, tr.minute, tr.ts as stats_ts, __CREATED_AT__
  from tiers_raw tr join topology t on (t.tenant_id = tr.tenant_id and t.cmts = tr.cmts)
  WHERE tr.created_at >= (SELECT max(created_at) from tiers)
