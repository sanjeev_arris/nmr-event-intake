INSERT INTO tenant (name, created_at)
  SELECT 'demo', -1
  WHERE NOT EXISTS (
      SELECT name, created_at FROM tenant WHERE name='demo'
  );

INSERT INTO topology (tenant_id, market, hub, cmts, year, month, day, hour, minute, stats_ts, created_at)
  SELECT (SELECT id from tenant where name='demo'), 'dummy', 'dummy', 'dummy', 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM topology
  );


INSERT INTO tiers (topology_id, cable_mac, tmax, downstream, year, month, day, hour, minute, stats_ts, created_at)
  SELECT (SELECT id from topology where market='dummy'), 'dummy', 0, -1, 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM tiers
  );


INSERT INTO sg (tmax, cmts, cable_mac, sg_id, year, month, wk, day, hour, minute, stats_ts, created_at)
  SELECT 0, 'dummy', 'dummy', 'dummy', 0, 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM sg where sg_id='dummy'
  );


INSERT INTO sg_stats_weekly (
  sg_id, downstream,
  peak_pct_util, avg_pct_util, stdev_pct_util,
  peak_kbps, avg_kbps, stdev_kbps,
  peak_cms, avg_cms, stdev_cms,
  peak_cms_online, avg_cms_online, stdev_cms_online,
  peak_ch_count, avg_ch_count, stdev_ch_count,
  year, month, wk, created_at)
  SELECT
    (select id from sg where sg_id='dummy'), -1,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0, -1
  WHERE NOT EXISTS (
      SELECT id FROM sg_stats_weekly
  );


INSERT INTO sg_stats_monthly (
  sg_id, downstream,
  peak_pct_util, avg_pct_util, stdev_pct_util,
  peak_kbps, avg_kbps, stdev_kbps,
  peak_cms, avg_cms, stdev_cms,
  peak_cms_online, avg_cms_online, stdev_cms_online,
  peak_ch_count, avg_ch_count, stdev_ch_count,
  year, month, created_at)
  SELECT
    (select id from sg where sg_id='dummy'), -1,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, -1
  WHERE NOT EXISTS (
      SELECT id FROM sg_stats_monthly
  );


INSERT INTO sg_stats_daily (
  sg_id, downstream,
  peak_pct_util, avg_pct_util, stdev_pct_util,
  peak_kbps, avg_kbps, stdev_kbps,
  peak_cms, avg_cms, stdev_cms,
  peak_cms_online, avg_cms_online, stdev_cms_online,
  peak_ch_count, avg_ch_count, stdev_ch_count,
  year, month, wk, day, created_at)
  SELECT
    (SELECT id from sg where sg_id='dummy'), -1,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0, 0, -1
  WHERE NOT EXISTS (
      SELECT id FROM sg_stats_daily
  );


INSERT INTO ch_stats (sg_stats_id, downstream, channel, ch_pct_util, ch_kbps, ch_max_cms, ch_max_online, year, month, wk, day, hour, minute, stats_ts, created_at)
  SELECT null, -1, 'dummy', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM ch_stats
  );


INSERT INTO tier_stats (tier_id, nsub, tavg, downstream, year, month, day, hour, minute, stats_ts, created_at)
  SELECT (SELECT id from tiers where cable_mac='dummy'), 0, 0, -1, -1, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM tier_stats
  );


INSERT INTO sg_stats (sg_id, downstream, sg_pct_util, sg_kbps, sg_max_cms, sg_max_online, year, month, wk, day, hour, minute, stats_ts, created_at)
  SELECT (select id from sg where sg_id='dummy'), -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1
  WHERE NOT EXISTS (
      SELECT id FROM sg_stats
  );


INSERT INTO req_cap (sg_id, model_id, downstream, curr, w_1, w_4, w_8, w_12, w_26, year, month,day, created_at)
  SELECT (SELECT id from sg where sg.cable_mac = 'dummy'), -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1
  WHERE NOT EXISTS (
      SELECT id FROM req_cap
  );
