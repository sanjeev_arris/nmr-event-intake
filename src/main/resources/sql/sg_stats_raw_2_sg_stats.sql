INSERT INTO sg_stats (downstream, sg_id, sg_pct_util, sg_kbps, sg_max_cms, sg_max_online,
                      year, month, wk, day_of_wk, day, hour, minute, stats_ts, created_at)
  select raw.downstream, sg.id, raw.sg_pct_util, raw.sg_kbps, raw.sg_max_cms, raw.sg_max_online,
    raw.year, raw.month, raw.wk, raw.day_of_wk, raw.day, raw.hour, raw.minute, raw.ts, __CREATED_AT__ created_at
  FROM sg_stats_raw raw JOIN sg on (raw.cmts = sg.cmts and raw.cable_mac=sg.cable_mac and raw.sg_id = sg.sg_id)
  WHERE raw.created_at >= (SELECT max(created_at) from sg_stats)


