INSERT INTO tier_stats (tier_id, tavg, nsub, downstream, year, month, day_of_wk, day, hour, minute, stats_ts, created_at)
  select tiers.id as tier_id, tr.tavg, tr.nsub, tr.downstream, tr.year, tr.month, tr.day_of_wk, tr.day, tr.hour, tr.minute, tr.ts as stats_ts, __CREATED_AT__
  from tier_stats_raw tr join
    topology t on (t.cmts = tr.cmts) JOIN
    tiers on (t.id = tiers.topology_id and tr.tmax = tiers.tmax)
  WHERE tr.created_at >= (SELECT max(created_at) from tier_stats)

