-- DROP TABLE ch_stats_raw;
-- DROP TABLE ch_stats;
-- DROP TABLE sg_stats_raw;
-- DROP TABLE sg_stats;
-- DROP TABLE tier_stats_raw;
-- DROP TABLE tier_stats;
-- DROP TABLE tiers_raw;
-- DROP TABLE tiers;
-- DROP TABLE topology;
-- DROP TABLE tenant;

create table IF NOT EXISTS tenant (
  id SERIAL  PRIMARY KEY,
  name varchar(32) not null,
  created_at bigint not null
);

create table IF NOT EXISTS sg_stats_raw (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT not NULL,
  ts BIGINT not null,
  cmts varchar(64) not null,
  cable_mac varchar(64) not null,
  sg_id varchar(32) not null,
  sg_pct_util decimal not null,
  sg_kbps decimal not null,
  sg_max_cms int not null,
  sg_max_online int not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  created_at bigint not null,
  constraint fk_sg_stats_raw_tenant foreign key (tenant_id) references tenant (id)
);

create index IF NOT EXISTS idx_tenant_id_sg_stats_raw on sg_stats_raw(tenant_id);

create index IF NOT EXISTS idx_cmts_sg_stats_raw on sg_stats_raw(cmts);

create index IF NOT EXISTS idx_cable_mac_sg_stats_raw on sg_stats_raw(cable_mac);

create index IF NOT EXISTS idx_sg_id_sg_stats_raw on sg_stats_raw(sg_id);

create index IF NOT EXISTS idx_year_sg_stats_raw on sg_stats_raw(year);

create index IF NOT EXISTS idx_month_sg_stats_raw on sg_stats_raw(month);

create index IF NOT EXISTS idx_wk_sg_stats_raw on sg_stats_raw(wk);

create index IF NOT EXISTS idx_day_wk_sg_stats_raw on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_sg_stats_raw on sg_stats_raw(day);

create index IF NOT EXISTS idx_hour_sg_stats_raw on sg_stats_raw(hour);

create index IF NOT EXISTS idx_minute_sg_stats_raw on sg_stats_raw(minute);

create index IF NOT EXISTS idx_created_at_sg_stats_raw on sg_stats_raw(created_at);

create index IF NOT EXISTS idx_ts_sg_stats_raw on sg_stats_raw(ts);

create index IF NOT EXISTS idx_downstream_sg_stats_raw on sg_stats_raw(downstream);

create table IF NOT EXISTS ch_stats_raw (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT not NULL,
  ts BIGINT not null,
  cmts varchar(64) not null,
  cable_mac varchar(64) not null,
  sg_id varchar(32) not null,
  channel varchar(64) not null,
  ch_pct_util decimal not null,
  ch_kbps decimal not null,
  ch_max_cms int not null,
  ch_max_online int not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  created_at bigint not null,
  constraint fk_ch_stats_raw_tenant foreign key (tenant_id) references tenant (id)
);

create index IF NOT EXISTS idx_tenant_id_ch_stats_raw on ch_stats_raw(tenant_id);

create index IF NOT EXISTS idx_cmts_ch_stats_raw on ch_stats_raw(cmts);

create index IF NOT EXISTS idx_cable_mac_ch_stats_raw on ch_stats_raw(cable_mac);

create index IF NOT EXISTS idx_sg_id_ch_stats_raw on ch_stats_raw(sg_id);

create index IF NOT EXISTS idx_channel_ch_stats_raw on ch_stats_raw(channel);

create index IF NOT EXISTS idx_ts_ch_stats_raw on ch_stats_raw(ts);

create index IF NOT EXISTS idx_created_at_ch_stats_raw on ch_stats_raw(created_at);

create index IF NOT EXISTS idx_downstream_ch_stats_raw on ch_stats_raw(downstream);

create table IF NOT EXISTS tiers_raw (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT not NULL,
  ts BIGINT not null,
  cmts varchar(64) not null,
  cable_mac varchar(64) not null,
  tmax int not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  created_at bigint not null,
  constraint fk_tiers_raw_tenant foreign key (tenant_id) references tenant (id)
);

create index IF NOT EXISTS idx_tenant_id_tiers_raw on tiers_raw(tenant_id);

create index IF NOT EXISTS idx_cmts_tiers_raw on tiers_raw(cmts);

create index IF NOT EXISTS idx_cable_mac_tiers_raw on tiers_raw(cable_mac);

create index IF NOT EXISTS idx_year_tiers_raw on tiers_raw(year);

create index IF NOT EXISTS idx_month_tiers_raw on tiers_raw(month);

create index IF NOT EXISTS idx_day_wk_tiers_raw on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_tiers_raw on tiers_raw(day);

create index IF NOT EXISTS idx_hour_tiers_raw on tiers_raw(hour);

create index IF NOT EXISTS idx_downstream_tiers_raw on tiers_raw(downstream);

create index IF NOT EXISTS idx_tmax_tiers_raw on tiers_raw(tmax);


create table IF NOT EXISTS tier_stats_raw (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT not NULL,
  ts BIGINT not null,
  cmts varchar(64) not null,
  cable_mac varchar(64) not null,
  nsub int not null,
  tavg decimal not null,
  tmax int not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  created_at bigint not null,
  constraint fk_tier_stats_raw_tenant foreign key (tenant_id) references tenant (id)
);

create index IF NOT EXISTS idx_tenant_id_tier_stats_raw on tier_stats_raw(tenant_id);

create index IF NOT EXISTS idx_cmts_tier_stats_raw on tier_stats_raw(cmts);

create index IF NOT EXISTS idx_cable_mac_tier_stats_raw on tier_stats_raw(cable_mac);

create index IF NOT EXISTS idx_tmax_tier_stats_raw on tier_stats_raw(tmax);

create index IF NOT EXISTS idx_year_tier_stats_raw on tier_stats_raw(year);

create index IF NOT EXISTS idx_month_tier_stats_raw on tier_stats_raw(month);

create index IF NOT EXISTS idx_day_wk_tier_stats_raw on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_tier_stats_raw on tier_stats_raw(day);

create index IF NOT EXISTS idx_hour_tier_stats_raw on tier_stats_raw(hour);

create index IF NOT EXISTS idx_downstream_tier_stats_raw on tier_stats_raw(downstream);

create table IF NOT EXISTS topology (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT not null,
  stats_ts BIGINT not null,
  market varchar(32) not null,
  hub varchar(32) not null,
  cmts VARCHAR(64) not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  created_at bigint not null,
  constraint fk_topology_tenant foreign key (tenant_id) references tenant (id)
);

create index IF NOT EXISTS idx_tenant_id_topology on topology(tenant_id);

create index IF NOT EXISTS idx_cmts_topology on topology(cmts);

create index IF NOT EXISTS idx_year_topology on topology(year);

create index IF NOT EXISTS idx_month_topology on topology(month);

create index IF NOT EXISTS idx_day_wk_topology on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_topology on topology(day);

create index IF NOT EXISTS idx_hour_topology on topology(hour);


create table IF NOT EXISTS tiers (
  id BIGSERIAL  PRIMARY KEY,
  topology_id BIGINT NOT NULL,
  cable_mac varchar(64) not null,
  tmax int not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  stats_ts BIGINT not null,
  created_at BIGINT not null,
  constraint fk_tiers_topology foreign key (topology_id) references topology (id)
);

create index IF NOT EXISTS idx_topology_id_tiers on tiers(topology_id);

create index IF NOT EXISTS idx_cable_mac_tiers on tiers(cable_mac);

create index IF NOT EXISTS idx_tmax_tiers on tiers(tmax);

create index IF NOT EXISTS idx_year_tiers on tiers(year);

create index IF NOT EXISTS idx_month_tiers on tiers(month);

create index IF NOT EXISTS idx_day_wk_tiers on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_tiers on tiers(day);

create index IF NOT EXISTS idx_hour_tiers on tiers(hour);

create index IF NOT EXISTS idx_downstream_tiers on tiers(downstream);

create index IF NOT EXISTS idx_created_at_tiers on tiers(created_at);


create table IF NOT EXISTS tier_stats (
  id BIGSERIAL  PRIMARY KEY,
  tier_id BIGINT not NULL,
  nsub int not null,
  tavg decimal not null,
  downstream int not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  stats_ts BIGINT not null,
  created_at bigint not null,
  constraint fk_tier_stats_tier_id foreign key (tier_id) references tiers (id)
);


create index IF NOT EXISTS idx_tier_id_tier_stats on tier_stats(tier_id);

create index IF NOT EXISTS idx_year_tier_stats on tier_stats(year);

create index IF NOT EXISTS idx_month_tier_stats on tier_stats(month);

create index IF NOT EXISTS idx_day_wk_tier_stats on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_tier_stats on tier_stats(day);

create index IF NOT EXISTS idx_hour_tier_stats on tier_stats(hour);

create index IF NOT EXISTS idx_downstream_tier_stats on tier_stats(downstream);

create index IF NOT EXISTS idx_created_at_tier_stats on tier_stats(created_at);


create table IF NOT EXISTS sg (
  id BIGSERIAL  PRIMARY KEY,
  tmax INT,
  cmts varchar(64) not null,
  cable_mac varchar(64) not null,
  sg_id varchar(32) not null,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  stats_ts BIGINT not null,
  created_at BIGINT not null
);


create index IF NOT EXISTS idx_sg_id_sg on sg(sg_id);

create index IF NOT EXISTS idx_year_sg on sg(year);

create index IF NOT EXISTS idx_month_sg on sg(month);

create index IF NOT EXISTS idx_wk_sg on sg(month);

create index IF NOT EXISTS idx_day_wk_sg on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_sg on sg(day);

create index IF NOT EXISTS idx_hour_sg on sg(hour);

create index IF NOT EXISTS idx_minute_sg on sg(minute);

create index IF NOT EXISTS idx_created_at_sg on sg(created_at);


create table IF NOT EXISTS sg_stats (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT not null,
  downstream int not null,
  sg_pct_util decimal not null,
  sg_kbps decimal not null,
  sg_max_cms int not null,
  sg_max_online int not null,
  ch_count int not null DEFAULT -1,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  stats_ts BIGINT not null,
  created_at BIGINT not null,
  constraint fk_sg_stats_sg_id foreign key (sg_id) references sg (id)
);

create index IF NOT EXISTS idx_sg_id_sg_stats on sg_stats(sg_id);

create index IF NOT EXISTS idx_sg_id_sg_stats on sg_stats(sg_id);

create index IF NOT EXISTS idx_year_sg_stats on sg_stats(year);

create index IF NOT EXISTS idx_month_sg_stats on sg_stats(month);

create index IF NOT EXISTS idx_wk_sg_stats on sg_stats(wk);

create index IF NOT EXISTS idx_day_wk_sg_stats on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_sg_stats on sg_stats(day);

create index IF NOT EXISTS idx_hour_sg_stats on sg_stats(hour);

create index IF NOT EXISTS idx_minute_sg_stats on sg_stats(minute);

create index IF NOT EXISTS idx_created_at_sg_stats on sg_stats(created_at);

create index IF NOT EXISTS idx_downstream_sg_stats on sg_stats(downstream);


create table IF NOT EXISTS ch_stats (
  id BIGSERIAL  PRIMARY KEY,
  sg_stats_id BIGINT,
  downstream int not null,
  channel varchar(64) not null,
  ch_pct_util decimal not null,
  ch_kbps decimal not null,
  ch_max_cms int not null,
  ch_max_online int not null,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  hour INT not null,
  minute INT not null,
  stats_ts BIGINT not null,
  created_at BIGINT not null,
  constraint fk_ch_stats_sg_stats_id foreign key (sg_stats_id) references sg_stats (id)
);


create index IF NOT EXISTS idx_sg_stats_id_ch_stats on ch_stats(sg_stats_id);

create index IF NOT EXISTS idx_year_ch_stats on ch_stats(year);

create index IF NOT EXISTS idx_month_ch_stats on ch_stats(month);

create index IF NOT EXISTS idx_day_wk_ch_stats on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_ch_stats on ch_stats(day);

create index IF NOT EXISTS idx_hour_ch_stats on ch_stats(hour);

create index IF NOT EXISTS idx_minute_ch_stats on ch_stats(minute);

create index IF NOT EXISTS idx_created_at_ch_stats on ch_stats(created_at);

create index IF NOT EXISTS idx_downstream_ch_stats on ch_stats(downstream);

create index IF NOT EXISTS idx_channel_ch_stats on ch_stats(channel);


CREATE OR REPLACE VIEW vw_sg_usage_downstream as
  SELECT sg.id as sg_id, sg.sg_id as sg_name, sg.cmts, sg.cable_mac, sg.tmax, sgs.sg_kbps, sgs.sg_pct_util, sgs.sg_max_cms, sgs.sg_max_online, sgs.ch_count, sgs.stats_ts
  FROM sg_stats sgs
    JOIN sg ON (sgs.sg_id=sg.id)
  WHERE sgs.downstream =1;

CREATE OR REPLACE VIEW vw_sg_usage_upstream as
  SELECT sg.id as sg_id, sg.sg_id as sg_name, sg.cmts, sg.cable_mac, sg.tmax, sgs.sg_kbps, sgs.sg_pct_util, sgs.sg_max_cms, sgs.sg_max_online, sgs.ch_count, sgs.stats_ts
  FROM sg_stats sgs
    JOIN sg ON (sgs.sg_id=sg.id)
  WHERE sgs.downstream =0;


create table IF NOT EXISTS sg_stats_daily (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT not null,
  downstream int not null,
  peak_pct_util decimal not null,
  avg_pct_util decimal not null,
  stdev_pct_util decimal not null,
  peak_kbps decimal not null,
  avg_kbps decimal not null,
  stdev_kbps decimal not null,
  peak_cms int not null,
  avg_cms int not null,
  stdev_cms int not null,
  peak_cms_online int not null,
  avg_cms_online int not null,
  stdev_cms_online int not null,
  peak_ch_count int not null DEFAULT -1,
  avg_ch_count int not null DEFAULT -1,
  stdev_ch_count int not null DEFAULT -1,
  year INT not null,
  month INT not null,
  wk INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  created_at BIGINT not null,
  constraint fk_sg_stats_daily_sg_id foreign key (sg_id) references sg (id)
);


create index IF NOT EXISTS idx_sg_id_sg_stats_daily on sg_stats_daily(sg_id);

create index IF NOT EXISTS idx_sg_id_sg_stats_daily on sg_stats_daily(sg_id);

create index IF NOT EXISTS idx_year_sg_stats_daily on sg_stats_daily(year);

create index IF NOT EXISTS idx_month_sg_stats_daily on sg_stats_daily(month);

create index IF NOT EXISTS idx_wk_sg_stats_daily on sg_stats_daily(wk);

create index IF NOT EXISTS idx_day_wk_sg_stats_daily on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_sg_stats_daily on sg_stats_daily(day);

create index IF NOT EXISTS idx_created_at_sg_stats_daily on sg_stats_daily(created_at);

create index IF NOT EXISTS idx_downstream_sg_stats_daily on sg_stats_daily(downstream);


create table IF NOT EXISTS sg_stats_weekly (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT not null,
  downstream int not null,
  peak_pct_util decimal not null,
  avg_pct_util decimal not null,
  stdev_pct_util decimal not null,
  peak_kbps decimal not null,
  avg_kbps decimal not null,
  stdev_kbps decimal not null,
  peak_cms int not null,
  avg_cms int not null,
  stdev_cms decimal not null,
  peak_cms_online int not null,
  avg_cms_online int not null,
  stdev_cms_online decimal not null,
  peak_ch_count int not null DEFAULT -1,
  avg_ch_count int not null DEFAULT -1,
  stdev_ch_count decimal not null DEFAULT -1,
  year INT not null,
  month INT not null,
  wk INT not null,
  created_at BIGINT not null,
  constraint fk_sg_stats_weekly_sg_id foreign key (sg_id) references sg (id)
);

create index IF NOT EXISTS idx_sg_id_sg_stats_weekly on sg_stats_weekly(sg_id);

create index IF NOT EXISTS idx_sg_id_sg_stats_weekly on sg_stats_weekly(sg_id);

create index IF NOT EXISTS idx_year_sg_stats_weekly on sg_stats_weekly(year);

create index IF NOT EXISTS idx_month_sg_stats_weekly on sg_stats_weekly(month);

create index IF NOT EXISTS idx_wk_sg_stats_weekly on sg_stats_weekly(wk);

create index IF NOT EXISTS idx_created_at_sg_stats_weekly on sg_stats_weekly(created_at);

create index IF NOT EXISTS idx_downstream_sg_stats_weekly on sg_stats_weekly(downstream);


create table IF NOT EXISTS sg_stats_monthly (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT not null,
  downstream int not null,
  peak_pct_util decimal not null,
  avg_pct_util decimal not null,
  stdev_pct_util decimal not null,
  peak_kbps decimal not null,
  avg_kbps decimal not null,
  stdev_kbps decimal not null,
  peak_cms int not null,
  avg_cms int not null,
  stdev_cms decimal not null,
  peak_cms_online int not null,
  avg_cms_online int not null,
  stdev_cms_online int not null,
  peak_ch_count decimal not null DEFAULT -1,
  avg_ch_count int not null DEFAULT -1,
  stdev_ch_count decimal not null DEFAULT -1,
  year INT not null,
  month INT not null,
  created_at BIGINT not null,
  constraint fk_sg_stats_monthly_sg_id foreign key (sg_id) references sg (id)
);


create index IF NOT EXISTS idx_sg_id_sg_stats_monthly on sg_stats_monthly(sg_id);

create index IF NOT EXISTS idx_sg_id_sg_stats_monthly on sg_stats_monthly(sg_id);

create index IF NOT EXISTS idx_year_sg_stats_monthly on sg_stats_monthly(year);

create index IF NOT EXISTS idx_month_sg_stats_monthly on sg_stats_monthly(month);

create index IF NOT EXISTS idx_created_at_sg_stats_monthly on sg_stats_monthly(created_at);

create index IF NOT EXISTS idx_downstream_sg_stats_monthly on sg_stats_monthly(downstream);


create table IF NOT EXISTS node_split_model (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT NOT NULL,
  downstream int not null,
  intercept decimal(10,4) not null,
  slope decimal(10,4) not null,
  xbar decimal(10,4) not null,
  stdev_x decimal(10,4) not null,
  num_samples int not null,
  rse decimal(10,4) not null,
  curr decimal(10,4),
  w_1 decimal(10,4),
  w_4 decimal(10,4),
  w_8 decimal(10,4),
  w_12 decimal(10,4),
  w_26 decimal(10,4),
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  created_at BIGINT not null,
  constraint fk_node_split_model_sg foreign key (sg_id) references sg (id)
);

create index IF NOT EXISTS idx_sg_id_node_split_model on node_split_model(sg_id);

create index IF NOT EXISTS idx_year_node_split_model on node_split_model(year);

create index IF NOT EXISTS idx_month_node_split_model on node_split_model(month);

create index IF NOT EXISTS idx_day_wk_node_split_model on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_node_split_model on node_split_model(day);

create index IF NOT EXISTS idx_created_at_node_split_model on node_split_model(created_at);

create index IF NOT EXISTS idx_downstream_node_split_model on node_split_model(downstream);

create table IF NOT EXISTS req_cap (
  id BIGSERIAL  PRIMARY KEY,
  sg_id BIGINT NOT NULL,
  model_id BIGINT NOT NULL,
  downstream int not null,
  curr decimal(10,4) not null,
  w_1 decimal(10,4) not null,
  w_4 decimal(10,4) not null,
  w_8 decimal(10,4) not null,
  w_12 decimal(10,4) not null,
  w_26 decimal(10,4) not null,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  created_at BIGINT not null,
  constraint fk_req_cap_sg foreign key (sg_id) references sg (id)
);


create index IF NOT EXISTS idx_created_at_req_cap on req_cap(created_at);

create table IF NOT EXISTS cmts_util_summary (
  id BIGSERIAL  PRIMARY KEY,
  tenant_id INT NOT NULL,
  topology_id BIGINT NOT NULL,
  market VARCHAR(32) NOT NULL,
  hub VARCHAR(32) NOT NULL,
  cmts VARCHAR(64) NOT NULL,
  cm_count INT NOT NULL,
  us_sg_count INT NOT NULL,
  us_avg_util DECIMAL(10,4) NOT NULL,
  us_over_cap_count INT NOT NULL,
  us_over_cap_perc DECIMAL(10,4) NOT NULL,
  ds_sg_count INT NOT NULL,
  ds_avg_util DECIMAL(10,4) NOT NULL,
  ds_over_cap_count INT NOT NULL,
  ds_over_cap_perc DECIMAL(10,4) NOT NULL,
  year INT not null,
  month INT not null,
  day_of_wk int not null default -1,
  day INT not null,
  created_at BIGINT not null,
  constraint fk_tenant_id_cmts_util_summary foreign key (tenant_id) references tenant (id),
  constraint fk_topology_id_cmts_util_summary foreign key (topology_id) references topology (id)
);

create index IF NOT EXISTS idx_tenant_id_cmts_util_summary on cmts_util_summary(tenant_id);

create index IF NOT EXISTS idx_topology_id_cmts_util_summary on cmts_util_summary(tenant_id);

create index IF NOT EXISTS idx_year_cmts_util_summary on cmts_util_summary(year);

create index IF NOT EXISTS idx_month_cmts_util_summary on cmts_util_summary(month);

create index IF NOT EXISTS idx_day_wk_cmts_util_summary on sg_stats_raw(day_of_wk);

create index IF NOT EXISTS idx_day_cmts_util_summary on cmts_util_summary(day);

create index IF NOT EXISTS idx_created_at_cmts_util_summary on cmts_util_summary(created_at);

