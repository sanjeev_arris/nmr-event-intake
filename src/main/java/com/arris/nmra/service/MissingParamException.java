package com.arris.nmra.service;

/**
 * Created by smishra on 11/28/16.
 */
public class MissingParamException extends Exception {
    public MissingParamException(){}
    public MissingParamException(String msg){
        super(msg);
    }
    public MissingParamException(String msg, Throwable t) {
        super(msg,t);
    }
}
