package com.arris.nmra.service;

import com.arris.nmra.model.CMTSUtilization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.util.Collection;
import java.util.List;

/**
 * Created by smishra on 1/13/17.
 */
@Component
public class CMTSService {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    static final private String SUMMARY_QUERY =
        "SELECT * FROM cmts_util_summary ORDER BY id limit ? OFFSET ?";

    public Collection<CMTSUtilization> utilization(int version, String interval, int offset, int count) {
        List<CMTSUtilization> rows =
            jdbcTemplate.query((conn) -> {
                PreparedStatement ps = conn.prepareStatement(SUMMARY_QUERY);
                ps.setInt(1, count);
                ps.setInt(2, offset);
                return ps;
            }, (rs, i) -> {
                CMTSUtilization util = new CMTSUtilization();
                util.setTopologyId(rs.getInt("topology_id"));
                util.setMarket(rs.getString("market"));
                util.setHub(rs.getString("hub"));
                util.setCmts(rs.getString("cmts"));
                util.setCmCount(rs.getInt("cm_count"));

                util.setUpstreamSGCount(rs.getInt("us_sg_count"));
                util.setUpstreamAvgUtil(rs.getDouble("us_avg_util"));
                util.setUpstreamOverCapacityCount(rs.getInt("us_over_cap_count"));
                util.setUpstreamOverCapacityPercent(rs.getDouble("us_over_cap_perc"));

                util.setDownstreamSGCount(rs.getInt("ds_sg_count"));
                util.setDownstreamAvgUtil(rs.getDouble("ds_avg_util"));
                util.setDownstreamOverCapacityCount(rs.getInt("ds_over_cap_count"));
                util.setDownstreamOverCapacityPercent(rs.getDouble("ds_over_cap_perc"));

                return util;
            });

        return rows;
    }
}
