package com.arris.nmra.service;

import com.arris.nmra.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by smishra on 11/23/16.
 */
@Component
public class AMQPMessageListener {
    private static final Logger logger = LoggerFactory.getLogger(AMQPMessageListener.class);

    private static final String TENANT_ID_QUERY = "select id from tenant where name = '$NAME'";

    private ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private DBService dbService;
    @Value("${tenant}")
    private String tenant;
    @Value("${processChannel}")
    private boolean processChannel;

    private Integer tenantId;

    @RabbitListener(queues = "#{downstreamStatsQueue.name}")
    public void onDownstreamStats(byte[] data) {
        logger.info("received downstream stats: " + data.length);
        try {
            long now = System.currentTimeMillis();
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, SgStatsData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(1);
                    return d;
                }).collect(Collectors.toList()),
                "downstreamStats");
            if (processChannel)
                dbService.writeToDB(
                    Arrays.stream(mapper.readValue(data, ChStatsData[].class)).map(d -> {
                        d.setCreatedAt(now);
                        d.setTenantId(tenantId);
                        d.setDownstream(1);
                        return d;
                    }).collect(Collectors.toList()),
                    "downstreamStats");
        } catch (Throwable e) {
            logger.warn("Error converting json data to downstream StatsData: " + e.getMessage());
        }
    }

    @RabbitListener(queues = "#{upstreamStatsQueue.name}")
    public void onUpstreamStats(byte[] data) {
        logger.info("received upstream stats: " + data.length);
        try {
            long now = System.currentTimeMillis();
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, SgStatsData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(0);
                    return d;
                }).collect(Collectors.toList()),
                "upstreamStats");
            if (processChannel)
                dbService.writeToDB(
                    Arrays.stream(mapper.readValue(data, ChStatsData[].class)).map(d -> {
                        d.setCreatedAt(now);
                        d.setTenantId(tenantId);
                        d.setDownstream(0);
                        return d;
                    }).collect(Collectors.toList()),
                    "upstreamStats");
        } catch (Throwable e) {
            logger.warn("Error converting json data to upstream StatsData: " + e.getMessage());
        }
    }

    @RabbitListener(queues = "#{downstreamTiersQueue.name}")
    public void onDownstreamTiers(byte[] data) {
        logger.info("received downstream tiers: " + data.length);
        try {
            long now = System.currentTimeMillis();
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, TierData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(1);
                    return d;
                }).collect(Collectors.toList()),
                "downstreamTiers");
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, TierStatsData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(1);
                    return d;
                }).collect(Collectors.toList()),
                "downstreamTiers");
        } catch (Throwable e) {
            logger.warn("Error converting json data to downstream TierData: " + e.getMessage());
        }
    }

    @RabbitListener(queues = "#{upstreamTiersQueue.name}")
    public void onUpstreamTiers(byte[] data) {
        logger.info("received upstream tiers: " + data.length);
        try {
            long now = System.currentTimeMillis();
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, TierData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(0);
                    return d;
                }).collect(Collectors.toList()),
                "upstreamTiers");
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, TierStatsData[].class)).map(d -> {
                    d.setCreatedAt(now);
                    d.setTenantId(tenantId);
                    d.setDownstream(0);
                    return d;
                }).collect(Collectors.toList()),
                "upstreamTiers");
        } catch (Throwable e) {
            logger.warn("Error converting json data to upstream TierData: " + e.getMessage());
        }
    }

    @RabbitListener(queues = "#{topologyQueue.name}")
    public void onTopology(byte[] data) {
        logger.info("received topology: " + data.length);
        try {
            dbService.writeToDB(
                Arrays.stream(mapper.readValue(data, TopologyData[].class)).map(d -> {
                    d.setTenantId(tenantId);
                    return d;
                }).collect(Collectors.toList()),
                "topology");
        } catch (Throwable e) {
            logger.warn("Error converting json data to TopologyData: " + e.getMessage());
        }
    }

    @PostConstruct
    private void init() throws SQLException {
        this.tenantId = dbService.queryForCount(TENANT_ID_QUERY.replace("$NAME", tenant));
    }
}
