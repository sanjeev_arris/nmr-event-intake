package com.arris.nmra.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchService;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

/**
 * Created by smishra on 2/24/17.
 */
@Component
public class DirectoryWatcherService {
    @Value("${incomingFolder}")
    private String incomingFolder;
    @Value("${processingFolder}")
    private String procesessingFolder;
    @Value("${processedFolder}")
    private String procesessedFolder;

    private File incoming;
    private File processing;
    private File processed;
    private WatchService watchService;

    @PostConstruct
    public void init() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        this.incoming = new File(incomingFolder);
        if (!this.incoming.exists()) {
            this.incoming.mkdirs();
        }
        this.processing = new File(procesessingFolder);
        if (!this.processing.exists()) {
            this.processing.mkdirs();
        }
        this.processed = new File(procesessedFolder);
        if (!this.processed.exists()) {
            this.processed.mkdirs();
        }

        Path path = incoming.toPath();
        path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
        startWatching();
    }

    private void startWatching() {
        Observable.fromCallable(() -> poll()).ofType(File.class).
            subscribeOn(Schedulers.computation()).
            observeOn(Schedulers.computation());


    }

    private <T> T poll() {
        return null;
    }
}
