package com.arris.nmra.service;

/**
 * Created by smishra on 11/28/16.
 */
public class DataSourceException extends Exception {
    public DataSourceException(){}
    public DataSourceException(String msg){
        super(msg);
    }
    public DataSourceException(String msg, Throwable t) {
        super(msg,t);
    }
}
