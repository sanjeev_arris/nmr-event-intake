package com.arris.nmra;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.Slf4jReporter.LoggingLevel;
import com.codahale.metrics.jvm.FileDescriptorRatioGauge;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.session.SessionAutoConfiguration;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@SpringBootApplication(exclude = SessionAutoConfiguration.class)
public class App {
    private static final String COMPLETION_LISTENER = "completionListener";

    public static void main(String[] args) throws InterruptedException {
        SpringApplication app = new SpringApplication(App.class);
        ApplicationContext ctxt = app.run(args);
        Thread.currentThread().join();
    }

    @Bean
    public YamlPropertySourceLoader makeYamlLoader() {
        return new YamlPropertySourceLoader();
    }

    @Bean
    public ScheduledExecutorService createThreadPool(@Value("${threadpool.core:5}") int corePoolSize) {
        CustomizableThreadFactory tf = new CustomizableThreadFactory("task-");
        tf.setDaemon(true);
        return new ScheduledThreadPoolExecutor(corePoolSize, tf);
    }

    @Autowired
    private MetricRegistry registry;

    @Value("${metrics.enable.gc:false}")
    boolean enableGcMetrics;

    @Value("${metrics.enable.file-descr:false}")
    boolean enableFdMetrics;

    @Value("${metrics.enable.memory:false}")
    boolean enableMemoryMetrics;

    @Value("${metrics.enable.threads:false}")
    boolean enableThreadMetrics;

    @Bean
    public MetricRegistry makeRegistry() {
        return new MetricRegistry();
    }

    @PostConstruct
    public void initMetrics() {
        Map<String, com.codahale.metrics.Metric> metrics = new HashMap<String, com.codahale.metrics.Metric>();
        if (enableGcMetrics) metrics.put("gc", new GarbageCollectorMetricSet());
        if (enableFdMetrics) metrics.put("fd", new FileDescriptorRatioGauge());
        if (enableMemoryMetrics) metrics.put("memory", new MemoryUsageGaugeSet());
        if (enableThreadMetrics) metrics.put("threads", new ThreadStatesGaugeSet());

        registry.registerAll(() -> metrics);
    }

    @Bean(destroyMethod = "stop")
    public Slf4jReporter createMetricsReporter(
        @Value("${metrics.report-seconds:60}") int reportSec) {
        Slf4jReporter reporter = Slf4jReporter.forRegistry(registry)
            .outputTo(LoggerFactory.getLogger("metrics"))
            .withLoggingLevel(LoggingLevel.INFO)
            .build();
        reporter.start(reportSec, TimeUnit.SECONDS);
        return reporter;
    }
}
