package com.arris.nmra.controller;

import com.arris.nmra.model.Status;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by smishra on 12/7/16.
 */
@RestController
@RequestMapping("/status")
public class HealthController {
    @RequestMapping("")
    public Status status() {
        return Status.OK;
    }

    @RequestMapping("/system")
    public Map systemInfo() {
        Map detail = new HashMap();
        Runtime r = Runtime.getRuntime();
        detail.put("timestamp", System.currentTimeMillis());
        detail.put("freeMemory",r.freeMemory());
        detail.put("totalMemory", r.totalMemory());
        detail.put("processors", r.availableProcessors());
        detail.put("maxMemory", r.maxMemory());
        return detail;
    }
}

