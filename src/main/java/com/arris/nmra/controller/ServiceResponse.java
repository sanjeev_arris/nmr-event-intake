package com.arris.nmra.controller;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by smishra on 1/13/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceResponse<T> {
    private int offset;
    private int size;
    private T content;
    private boolean success = true;
    private int respCode;
    private String respMessage;

    static public ServiceResponse OK = new ServiceResponse();

    ServiceResponse(){}

    ServiceResponse(T t, int offset, int size) {
        content = t;
        this.offset = offset;
        this.size =size;
        this.respCode=200;
    }

    ServiceResponse(boolean failed, int respCode, String respMessage) {
        this.setRespCode(respCode);
        this.setRespMessage(respMessage);
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getRespCode() {
        return respCode;
    }

    public void setRespCode(int respCode) {
        this.respCode = respCode;
        if (respCode>=400) success = false;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
