package com.arris.nmra.controller;

import com.arris.nmra.model.CMTSUtilization;
import com.arris.nmra.model.Status;
import com.arris.nmra.model.UtilizationNode;
import com.arris.nmra.service.CMTSService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smishra on 12/7/16.
 */
@RestController
@RequestMapping("/cmts")
public class CMTSController {
    @Autowired
    private CMTSService cmtsService;

    @RequestMapping("/util")
    public ServiceResponse<Collection<UtilizationNode>> summary(
        @RequestParam(value="version", defaultValue = "1") int version,
        @RequestParam(value="interval", defaultValue = "1m") String interval,
        @RequestParam(value="offset", defaultValue = "0") int offset,
        @RequestParam(value="count", defaultValue = "10") int count) {
        Collection<UtilizationNode> coll =
            UtilizationNode.toUtilizationNodes(cmtsService.utilization(version, interval, offset, count));

        return new ServiceResponse<>(coll, offset, coll.size());
    }
}

