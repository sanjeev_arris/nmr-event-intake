package com.arris.nmra.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * Created by smishra on 11/16/16.
 */
abstract public class DataConverter {
    static private final Logger logger = LoggerFactory.getLogger(DataConverter.class);

    static public enum DataType {
        BOOL("bool"), INT("int"), LONG("long"), FLOAT("float"), DOUBLE("double"), DATE("date"), TIME("time"), STRING("string"), OBJ("obj");

        private String code;

        private DataType(String code) {
            this.code = code;
        }

        static public DataType byCode(String code) {
            if (StringUtils.isEmpty(code)) return OBJ;
            String cc = code.trim().toLowerCase();
            Optional<DataType> optional = Arrays.asList(DataType.values()).stream().filter(t -> t.code.equalsIgnoreCase(cc)).findFirst();
            return optional.isPresent() ? optional.get() : OBJ;
        }
    }

    static public Object convert(Object value, DataType fromType, DataType toType, String inFormat, String outFormat) {
        if (value == null) return value;

        try {
            switch (toType) {
                case BOOL:
                    if (value instanceof Number) return ((Number) value).intValue() > 0;
                    char yn = String.valueOf(value).trim().toLowerCase().charAt(0);
                    return yn == 'y' || yn == '1' || yn == 't';
                case INT:
                    if (value instanceof Integer) return value;
                    Long longV = toLong(value, fromType, inFormat);
                    if (longV != null) return longV.intValue();
                    return -1;
                case LONG:
                    if (value instanceof Long) return value;
                    return toLong(value, fromType, inFormat);
                case FLOAT:
                    if (value instanceof Float) return value;
                    return toDouble(value).floatValue();
                case DOUBLE:
                    if (value instanceof Double) return value;
                    return toDouble(value);
                case STRING:
                    return toString(value, fromType, inFormat, outFormat);
                case TIME:
                    return new Time(toDate(value, fromType, inFormat).getTime());
                case DATE:
                    return toDate(value, fromType, inFormat);
                default:
                    return value;
            }
        } catch (Exception e) {
            logger.info(String.format("conversion failed: value: %s fromType: %s toType: %s inFormat: %s outFormat: %s", value, fromType, toType, inFormat, outFormat));
            return null;
        }
    }

    private static String formatDate(Date value, String format) {
        format = format.trim();
        Date dt = value;
        if (format.toLowerCase().equalsIgnoreCase("utc")) {
            return dt.toInstant().toString();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(dt);
    }

    private static String formatDouble(Double value, String format) {
        if (!StringUtils.isEmpty(format)) {
            try {
                if (format.equalsIgnoreCase("utc")) {
                    return new Date(value.longValue()).toInstant().toString();
                }
                NumberFormat formatter = new DecimalFormat(format.trim());
                return formatter.format(value);
            } catch (Exception e) {
                logger.debug(format + " failed to formatDouble: " + value);
            }
        }
        return String.valueOf(value);
    }

    public static Double toDouble(Object value) {
        if (value instanceof Number) return ((Number) value).doubleValue();
        if (value instanceof Date) return Double.valueOf(((Date) value).getTime()).doubleValue();
        if (value instanceof Boolean) return (Boolean) value ? 1.0 : 0;
        return Double.parseDouble(String.valueOf(value));
    }

    public static Long toLong(Object value, DataType fromType, String inFormat) {
        if (value instanceof Number) return ((Number) value).longValue();
        if (value instanceof Date) return Double.valueOf(((Date) value).getTime()).longValue();
        if (value instanceof Boolean) return (Boolean) value ? 1l : 0;
        String str = String.valueOf(value);

        if (str.indexOf(".") > 0) {
            return ((Double) Double.parseDouble(str)).longValue();
        }

        if (!StringUtils.isEmpty(inFormat)) {
            try {
                for (String f : inFormat.split(":")) {
                    if (f.trim().equalsIgnoreCase("utc")) {
                        return Util.toMillisFromUTC(str);
                    }
                    return new SimpleDateFormat(inFormat).parse(str).getTime();
                }

            } catch (ParseException e) {
                logger.debug("failed to apply formatDouble: " + inFormat + " on string: " + str + " to convert to long");
            }
        }

        try {
            return Long.parseLong(String.valueOf(value));
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static Date toDate(Object value, DataType fromType, String inFormat) throws ParseException {
        if (value instanceof Date) return (Date) value;
        if (value instanceof Long) return new Date((Long) value);
        if (value instanceof Number) return new Date(((Number) value).longValue());
        if (!StringUtils.isEmpty(inFormat)) {
            if (inFormat.trim().equalsIgnoreCase("utc")) {
                return new Date(Instant.parse(value.toString()).toEpochMilli());
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat(inFormat);
            return dateFormat.parse(value.toString());
        } else {
            Long v = toLong(value, fromType, inFormat);
            if (v != null) return new Date(v);
            return new Date(value.toString());
        }
    }

    public static String toString(Object value, DataType fromType, String inFormat, String outFormat) throws ParseException {
        if (value instanceof Date) {
            if (outFormat != null && outFormat.trim().equalsIgnoreCase("utc"))
                return ((Date) value).toInstant().toString();
            if (outFormat != null) return new SimpleDateFormat(outFormat).format((Date) value);
            return ((Date) value).toInstant().toString();
        }

        if (value instanceof Number) {
            if (fromType != null && fromType.equals(DataType.DATE)) {
                return toString(new Date(((Number) value).longValue()), null, inFormat, outFormat);
            } else if (outFormat != null) {
                return formatDouble(toDouble(value), outFormat);
            } else {
                return value.toString();
            }
        }

        if (!StringUtils.isEmpty(inFormat)) {
            Date date = null;
            String[] inFormatParts = inFormat.split("|");
            for (String f : inFormatParts) {
                try {
                    if (f.trim().equalsIgnoreCase("utc")) {
                        date = new Date(Instant.parse(value.toString()).toEpochMilli());
                    } else {
                        date = new SimpleDateFormat(f).parse(value.toString());
                    }
                    return formatDate(date, outFormat);
                } catch (Exception e) {
                    logger.debug("toString failed to convert string to string: " + value + ", " + e.getMessage());
                    if (date != null) return date.toInstant().toString();
                }
            }
        }
        if (StringUtils.isNotEmpty(outFormat)) {
            Date dt = toDate(value, fromType, inFormat);
            if (dt != null) return formatDate(dt, outFormat);
        }

        return value.toString();
    }

}
