package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by smishra on 11/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Model(value = "tiers_raw", preInsertCheck = "where not exists ")
public class TierData {
    @ModelAttr("tenant_id")
    Integer tenantId;

    @ModelAttr(preCheck = true)
    String cmts;
    @ModelAttr(value = "cable_mac", preCheck = true)
    String cableMac;
    @ModelAttr(preCheck = true)
    int tmax;
    @ModelAttr(preCheck = true)
    int downstream;

    Integer year;
    Integer month;

    @ModelAttr("day_of_wk")
    Integer dayOfWk;

    Integer day;
    Integer hour;
    Integer minute;

    Long ts;

    @ModelAttr("created_at")
    long createdAt = System.currentTimeMillis();

    public void setTs(String ts) {
        this.setTs(Util.toMillis(ts, System.currentTimeMillis()));
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public String getCableMac() {
        return cableMac;
    }

    public void setCableMac(String cableMac) {
        this.cableMac = cableMac;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
        DateTime dt = new DateTime(this.ts);
        this.year = dt.getYear();
        this.month = dt.getMonthOfYear();
        this.dayOfWk = dt.getDayOfWeek();
        this.day = dt.getDayOfMonth();
        this.hour = dt.getHourOfDay();
        this.minute = dt.getMinuteOfHour();
    }

    public int getDownstream() {
        return downstream;
    }

    public void setDownstream(int downstream) {
        this.downstream = downstream;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public int getTmax() {
        return tmax;
    }

    public void setTmax(int tmax) {
        this.tmax = tmax;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getTenantId() {
        return tenantId;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDayOfWk() {
        return dayOfWk;
    }

    public void setDayOfWk(Integer dayOfWk) {
        this.dayOfWk = dayOfWk;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }
}
