package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by smishra on 11/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Model(value = "stats_raw")
public class StatsData {
    @ModelAttr("tenant_id")
    Integer tenantId;

    String cmts;

    @ModelAttr("cable_mac")
    String cableMac;

    @JsonProperty("sg_id")
    @ModelAttr("sg_id")
    String sgId;
    String channel;

    @JsonProperty("sg_pct_util")
    @ModelAttr(value = "sg_pct_util")
    double sgPctUtil;

    @JsonProperty("sg_kbps")
    @ModelAttr("sg_kbps")
    double sgKbps;

    @JsonProperty("sg_max_cms")
    @ModelAttr("sg_max_cms")
    int sgMaxCms;

    @JsonProperty("sg_max_online")
    @ModelAttr("sg_max_online")
    int sgMaxOnline;

    @JsonProperty("ch_pct_util")
    @ModelAttr("ch_pct_util")
    double chPctUtil;

    @JsonProperty("ch_kbps")
    @ModelAttr("ch_kbps")
    int chKbps;

    @JsonProperty("ch_max_cms")
    @ModelAttr("ch_max_cms")
    int chMaxCms;

    @JsonProperty("ch_max_online")
    @ModelAttr("ch_max_online")
    int chMaxOnline;

    Integer year;
    Integer month;

    @ModelAttr("day_of_wk")
    Integer dayOfWk;

    Integer day;
    Integer hour;
    Integer minute;

    @ModelAttr(ignore = true)
    Long ts;

    @ModelAttr(value = "ts")
    Date statsTs;

    int downstream;

    @JsonProperty("created_at")
    @ModelAttr("created_at")
    Long createdAt = System.currentTimeMillis();

    public void setTs(String ts) {
        this.setTs(Util.toMillis(ts, System.currentTimeMillis()));
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public String getCableMac() {
        return cableMac;
    }

    public void setCableMac(String cableMac) {
        this.cableMac = cableMac;
    }

    public String getSgId() {
        return sgId;
    }

    public void setSgId(String sgId) {
        this.sgId = sgId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public double getSgPctUtil() {
        return sgPctUtil;
    }

    public void setSgPctUtil(double sgPctUtil) {
        this.sgPctUtil = sgPctUtil;
    }

    public double getSgKbps() {
        return sgKbps;
    }

    public void setSgKbps(double sgKbps) {
        this.sgKbps = sgKbps;
    }

    public int getSgMaxCms() {
        return sgMaxCms;
    }

    public void setSgMaxCms(int sgMaxCms) {
        this.sgMaxCms = sgMaxCms;
    }

    public int getSgMaxOnline() {
        return sgMaxOnline;
    }

    public void setSgMaxOnline(int sgMaxOnline) {
        this.sgMaxOnline = sgMaxOnline;
    }

    public double getChPctUtil() {
        return chPctUtil;
    }

    public void setChPctUtil(double chPctUtil) {
        this.chPctUtil = chPctUtil;
    }

    public int getChKbps() {
        return chKbps;
    }

    public void setChKbps(int chKbps) {
        this.chKbps = chKbps;
    }

    public int getChMaxCms() {
        return chMaxCms;
    }

    public void setChMaxCms(int chMaxCms) {
        this.chMaxCms = chMaxCms;
    }

    public int getChMaxOnline() {
        return chMaxOnline;
    }

    public void setChMaxOnline(int chMaxOnline) {
        this.chMaxOnline = chMaxOnline;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
        DateTime dt = new DateTime(this.ts);
        this.statsTs = dt.toDate();
        this.year = dt.getYear();
        this.month = dt.getMonthOfYear();
        this.dayOfWk = dt.getDayOfWeek();
        this.day = dt.getDayOfMonth();
        this.hour = dt.getHourOfDay();
        this.minute = dt.getMinuteOfHour();
    }

    public int getDownstream() {
        return downstream;
    }

    public void setDownstream(int downstream) {
        this.downstream = downstream;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }
    public Integer getTenantId() {
        return tenantId;
    }


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDayOfWk() {
        return dayOfWk;
    }

    public void setDayOfWk(Integer dayOfWk) {
        this.dayOfWk = dayOfWk;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    public Date getStatsTs() {
        return statsTs;
    }

    public void setStatsTs(Date statsTs) {
        this.statsTs = statsTs;
    }

    @Override
    public String toString() {
        return "StatsData{" +
            "tenantId=" + tenantId +
            ", cmts='" + cmts + '\'' +
            ", cableMac='" + cableMac + '\'' +
            ", sgId='" + sgId + '\'' +
            ", channel='" + channel + '\'' +
            ", sgPctUtil=" + sgPctUtil +
            ", sgKbps=" + sgKbps +
            ", sgMaxCms=" + sgMaxCms +
            ", sgMaxOnline=" + sgMaxOnline +
            ", chPctUtil=" + chPctUtil +
            ", chKbps=" + chKbps +
            ", chMaxCms=" + chMaxCms +
            ", chMaxOnline=" + chMaxOnline +
            ", year=" + year +
            ", month=" + month +
            ", day=" + day +
            ", hour=" + hour +
            ", minute=" + minute +
            ", ts=" + ts +
            ", statsTs=" + statsTs +
            ", downstream=" + downstream +
            ", createdAt=" + createdAt +
            '}';
    }
}
