package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Created by smishra on 11/28/16.
 */
public class ModelUtil {
    private static final Logger log = LoggerFactory.getLogger(ModelUtil.class);

    public static <T> String createPreparedStatementAndPopulateColumnDataForPS(Collection<T> modelCollection,
                                                                               Collection<ColumnData> columnDataForPreparedStmt) {
        if (modelCollection == null || modelCollection.isEmpty()) return "";
        Object model = modelCollection.stream().findFirst().get();
        String tableName = getTableName(model);
        String preInsertStmt = getPreInsertCheck(model);
        Collection<ColumnData> preInsertCheckColumnData =null;
        boolean preInsertCheck = !StringUtils.isEmpty(preInsertStmt);

        Collection<ColumnData> dataForInserts = getColumnDataForInserts(modelCollection);
        String stmt =  createPreparedStatements(dataForInserts, tableName, preInsertCheck);
        dataForInserts.forEach(v -> columnDataForPreparedStmt.add(v));

        if (preInsertCheck) {
            preInsertCheckColumnData = getPreCheckColumnData(modelCollection);
            if (preInsertCheckColumnData!=null) {
                preInsertCheckColumnData.forEach(e -> columnDataForPreparedStmt.add(e));
            }

            preInsertStmt = preInsertStmt + createSelectCriteria(tableName, preInsertCheckColumnData);
        }

        if (preInsertStmt!=null) {
            stmt = stmt + " " + preInsertStmt;
        }
        return stmt;
    }

    private static String createSelectCriteria(String tableName, Collection<ColumnData> columnData) {
        if (columnData==null || columnData.isEmpty()) return null;
        StringBuilder sb = new StringBuilder("(select * from ").append(tableName).append(" where ");
        for (ColumnData cd: columnData) {
            sb.append(cd.columnName).append("=? and ");
        }

        return sb.substring(0, sb.length() - " and ".length())+")";
    }

    public static String getTableName(Object model) {
        String tableName = null;
        Annotation[] types = model.getClass().getAnnotations();
        if (types != null) {
            Optional<Annotation> optional = Arrays.stream(model.getClass().getAnnotations()).filter(t -> t.annotationType().equals(Model.class)).findFirst();
            if (optional.isPresent()) {
                tableName = ((Model) optional.get()).value();
            }
        }
        if (tableName == null) {
            tableName = model.getClass().getName();
            tableName = tableName.substring(tableName.lastIndexOf('.') + 1).toLowerCase();
        }

        return tableName;
    }

    public static String getPreInsertCheck(Object model) {
        String preInsertCheck = null;
        Annotation[] types = model.getClass().getAnnotations();
        if (types != null) {
            Optional<Annotation> optional = Arrays.stream(model.getClass().getAnnotations()).filter(t -> t.annotationType().equals(Model.class)).findFirst();
            if (optional.isPresent()) {
                return ((Model) optional.get()).preInsertCheck();
            }
        }

        return preInsertCheck;
    }

    public static <T> List<ColumnData> getColumnDataForInserts(Collection<T> modelCollection) {
        final List<ColumnData> values = new ArrayList<>();
        if (modelCollection == null || modelCollection.isEmpty()) return values;
        T model = modelCollection.stream().findFirst().get();

        Arrays.stream(model.getClass().getDeclaredFields()).filter(f -> {
            ModelAttr a = f.getAnnotation(ModelAttr.class);
            if (a != null && a.ignore()) return false;
            return true;
        }).forEach(f -> {
            final ColumnData param = new ColumnData();

            ModelAttr a = f.getAnnotation(ModelAttr.class);

            if (a != null && !StringUtils.isEmpty(a.value())) {
                param.columnName = a.value();
            } else {
                param.columnName = f.getName();
            }

            param.type = f.getType();
            values.add(param);

            modelCollection.forEach(m -> {
                try {
                    log.debug(String.format("field %s: model: %s", f.getName(), m));
                    Object value = f.get(m);
                    log.debug(String.format("field %s: value: %s model: %s", f.getName(), value, m));
                    if (a != null && a.size() != -1 && String.class.isAssignableFrom(f.getType())) {
                        if (value != null) {
                            value = Util.createStringFrom(value.toString(), a.size());
                        }
                    }

                    param.values.add(value);
                } catch (IllegalAccessException e) {
                    log.info(e.getMessage());
                }
            });
        });
        return values;
    }

    public static <T> List<ColumnData> getPreCheckColumnData(Collection<T> list) {
        final List<ColumnData> values = new ArrayList<>();
        if (list == null || list.isEmpty()) return values;
        T model = list.stream().findFirst().get();

        Arrays.stream(model.getClass().getDeclaredFields()).filter(f -> {
            ModelAttr a = f.getAnnotation(ModelAttr.class);
            if (a != null && !a.ignore() && a.preCheck()) return true;
            return false;
        }).forEach(f -> {
            final ColumnData param = new ColumnData();

            ModelAttr a = f.getAnnotation(ModelAttr.class);

            param.columnName = !StringUtils.isEmpty(a.value()) ? param.columnName = a.value() : f.getName();
            param.type = f.getType();

            values.add(param);

            list.forEach(m -> {
                try {
                    log.debug(String.format("field %s: model: %s", f.getName(), m));
                    Object value = f.get(m);
                    log.debug(String.format("field %s: value: %s model: %s", f.getName(), value, m));
                    if (a != null && a.size() != -1 && String.class.isAssignableFrom(f.getType())) {
                        if (value != null) {
                            value = Util.createStringFrom(value.toString(), a.size());
                        }
                    }

                    param.values.add(value);
                } catch (IllegalAccessException e) {
                    log.info(e.getMessage());
                }
            });
        });
        return values;
    }

    static private String createPreparedStatements(Collection<ColumnData> columnDatas, String tableName, boolean preInsertCheck) {
        if (preInsertCheck) {
            return formatStatements("insert into " + tableName + " (%s) select %s", columnDatas);
        }
        else {
            return formatStatements("insert into " + tableName + " (%s) values (%s)", columnDatas);
        }
    }

    static private String formatStatements(String stmt, Collection<ColumnData> columnDatas) {
        final StringBuilder columns = new StringBuilder();
        final StringBuilder placements = new StringBuilder();
        columnDatas.forEach(ps -> {
            columns.append(ps.columnName).append(",");
            placements.append("?").append(",");
        });
        if (columns.length() > 0) {
            columns.setLength(columns.length() - 1);
            placements.setLength(placements.length() - 1);
        }

        return String.format(stmt, columns.toString(), placements.toString());
    }



    /**This object maintains all the entries for a given column. For example,
     * if there are 100 rows for column 'first_name' there will be one object for first_name and
     * that will contain 100 values in its values list - one entry per row
     */
    static public class ColumnData {
        public String columnName;
        public String paramName;
        public List<Object> values = new ArrayList<>();
        public Class type;

        @Override
        public String toString() {
            return "ColumnData{" +
                "columnName='" + columnName + '\'' +
                ", paramName='" + paramName + '\'' +
                ", values=" + values +
                ", type=" + type +
                '}';
        }
    }
}
