package com.arris.nmra.config;

import com.arris.nmra.service.DBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.PeriodicTrigger;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by smishra on 12/22/16.
 */
@Configuration
@EnableScheduling
@Import({AggregationConfig.class})
public class DataAggregator implements SchedulingConfigurer {
    private static final Logger logger = LoggerFactory.getLogger(DataAggregator.class);

    @Autowired
    private AggregationConfig aggregationConfig;

    @Autowired
    DBService dbService;
    Map<String, Collection<CronTask>> cronTaskMap = new HashMap<>();

    @PostConstruct
    private void init() {
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        aggregationConfig.getQueries().forEach(q -> {
            switch (q.getScheduleType()) {
                case CRON:
                    logger.info(String.format("dataAggregator: configureTasks: configuring cronTasks: %s -> %s", q.getSchedule(), q.getQueries()));
                    CronTask ct = new CronTask(toRunnable(q.getQueries(), true), q.getSchedule());
                    scheduledTaskRegistrar.addCronTask(ct);
                    cronTaskMap.putIfAbsent(q.getSchedule(), new ArrayList<>());
                    cronTaskMap.get(q.getSchedule()).add(ct);
                    break;
                case NonTxCRON:
                    logger.info(String.format("dataAggregator: configureTasks: configuring cronTasks: %s -> %s", q.getSchedule(), q.getQueries()));
                    CronTask ct2 = new CronTask(toRunnable(q.getQueries(), false), q.getSchedule());
                    scheduledTaskRegistrar.addCronTask(ct2);
                    cronTaskMap.putIfAbsent(q.getSchedule(), new ArrayList<>());
                    cronTaskMap.get(q.getSchedule()).add(ct2);
                    break;
                case DELAY:
                case INTERVAL:
                case ONCE:
                    Runnable task = toRunnable(q.getQueries(), true);
                    logger.info("scheduling task to run just once: ");
                    scheduledTaskRegistrar.addTriggerTask(task, new RunOnceTrigger(Long.parseLong(q.getSchedule())));
                    break;
                default: logger.warn("Unsupported schedule type: " + q.getScheduleType());
            }
        });
    }

    Runnable toRunnable(List<ScheduledQuery.Query> queries, boolean transactional) {
        return () -> {
            final long now = System.currentTimeMillis();

            try {
                Collection stmt = queries.stream().map(q -> q.getQuery().replace("__CREATED_AT__", now + "")).collect(Collectors.toList());
                if (transactional) {
                    dbService.executeUpdatesInTransaction(stmt);
                }
                else {
                    dbService.executeUpdatesWithoutTransaction(stmt);
                }
            } catch (Throwable e) {
                logger.warn("Exception while running scheduledQuery", e);
            }
        };
    }

    private static class RunOnceTrigger extends PeriodicTrigger {

        public RunOnceTrigger(long period) {
            super(period);
            setInitialDelay(period);
        }

        @Override
        public Date nextExecutionTime(TriggerContext triggerContext) {
            if(triggerContext.lastCompletionTime() == null) { 	// hasn't executed yet
                return super.nextExecutionTime(triggerContext);
            }
            return null;
        }
    }
}
