package com.arris.nmra.config;

import com.arris.nmra.util.Util;

import java.util.List;

public class ScheduledQuery {
	public static enum ScheduleType { CRON, NonTxCRON, INTERVAL, DELAY, ONCE }
	
	public static class Query {
		private String namedQuery;
		private String query;

		public String getNamedQuery() {
			return namedQuery;
		}

		public void setNamedQuery(String namedQuery) {
			this.namedQuery = namedQuery;
			setQuery(new String(Util.bytesFromResource(namedQuery)));
		}

		public String getQuery() {
			return query;
		}
		public void setQuery(String query) {
			this.query = query;
		}

		@Override
		public String toString() {
			return "Query{" +
					"namedQuery='" + namedQuery + '\'' +
					", query='" + query + '\'' +
					'}';
		}
	}

	private ScheduleType scheduleType;
	private String schedule;
	private List<Query> queries;

	public ScheduleType getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public List<Query> getQueries() {
		return queries;
	}
	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}
}
