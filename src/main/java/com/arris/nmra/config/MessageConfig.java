package com.arris.nmra.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smishra on 11/23/16.
 */
@Configuration
public class MessageConfig {
    @Autowired
    private ApplicationContext context;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Bean
    public Queue downstreamStatsQueue(@Value("${messaging.downstreamStats.queue}") String queue,
                                      @Value("${messaging.downstreamStats.ttl}") Long ttl) {
        return createQueue(queue, ttl);
    }

    @Bean
    public Queue downstreamTiersQueue(@Value("${messaging.downstreamTiers.queue}") String queue,
                                      @Value("${messaging.downstreamTiers.ttl}") Long ttl) {
        return createQueue(queue, ttl);
    }

    @Bean
    public Queue upstreamStatsQueue(@Value("${messaging.upstreamStats.queue}") String queue,
                                    @Value("${messaging.upstreamStats.ttl}") Long ttl) {
        return createQueue(queue, ttl);
    }

    @Bean
    public Queue upstreamTiersQueue(@Value("${messaging.upstreamTiers.queue}") String queue,
                                    @Value("${messaging.upstreamTiers.ttl}") Long ttl) {
        return createQueue(queue, ttl);
    }

    @Bean
    public Queue topologyQueue(@Value("${messaging.topology.queue}") String queue,
                               @Value("${messaging.topology.ttl}") Long ttl) {
        return createQueue(queue, ttl);
    }

    @Bean
    public Binding downstreamStatsBinding (
        @Value("${messaging.downstreamStats.queue}") String queue,
        @Value("${messaging.downstreamStats.routing}") String key,
        @Value("${messaging.downstreamStats.ttl}") Long ttl
    ) {
        return BindingBuilder.
            bind(downstreamStatsQueue(queue, ttl)).
            to(new TopicExchange(exchange)).with(key);
    }

    @Bean
    public Binding downstreamTiersBinding (
        @Value("${messaging.downstreamTiers.queue}") String queue,
        @Value("${messaging.downstreamTiers.routing}") String key,
        @Value("${messaging.downstreamTiers.ttl}") Long ttl
    ) {
        return BindingBuilder.bind(downstreamTiersQueue(queue, ttl)).
            to(new TopicExchange(exchange)).with(key);
    }

    @Bean
    public Binding upstreamStatsBinding (
        @Value("${messaging.upstreamStats.queue}") String queue,
        @Value("${messaging.upstreamStats.routing}") String key,
        @Value("${messaging.upstreamStats.ttl}") Long ttl
    ) {
        return BindingBuilder.bind(upstreamStatsQueue(queue, ttl)).
            to(new TopicExchange(exchange)).with(key);
    }

    @Bean
    public Binding upstreamTiersBinding (
        @Value("${messaging.upstreamTiers.queue}") String queue,
        @Value("${messaging.upstreamTiers.routing}") String key,
        @Value("${messaging.upstreamTiers.ttl}") Long ttl
    ) {
        return BindingBuilder.bind(upstreamTiersQueue(queue, ttl)).
            to(new TopicExchange(exchange)).with(key);
    }

    @Bean
    public Binding topologyBinding (
        @Value("${messaging.topology.queue}") String queue,
        @Value("${messaging.topology.routing}") String key,
        @Value("${messaging.topology.ttl}") Long ttl
    ) {
        return BindingBuilder.bind(topologyQueue(queue, ttl)).
            to(new TopicExchange(exchange)).with(key);
    }

    private Queue createQueue(String name, Long ttl) {
        if (ttl==null || ttl<=0)
            return new Queue(name);
        else {
            Map args = new HashMap();
            args.put("x-message-ttl", ttl);
            return new Queue(name, true, false, false, args);
        }
    }
}
