package com.arris.nmra.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Map;

/**
 * Created by smishra on 12/7/16.
 */
//@EnableRedisHttpSession
//@Configuration
public class SessionConfig {
//    @Bean
    public JedisConnectionFactory connectionFactory(
        @Value("${spring.redis.host}") String host,
        @Value("${spring.redis.port}") int port,
        @Value("${spring.redis.password}") String password,
        @Value("${spring.redis.pool.max-active}") int maxActive,
        @Value("${spring.redis.pool.max-idle}") int maxIdle,
        @Value("${spring.redis.pool.max-wait}") int maxWait
        ) {
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(host);
        factory.setPort(port);
        factory.setPassword(password);
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(maxActive);
        config.setMaxIdle(maxIdle);
        config.setMaxWaitMillis(maxWait);
        factory.setPoolConfig(config);

        return factory;
    }

    //@Bean
    public HttpSessionStrategy httpSessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }
}
