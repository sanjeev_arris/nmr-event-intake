package com.arris.nmra.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties("aggregations")
public class AggregationConfig {
	private List<ScheduledQuery> queries;

	public List<ScheduledQuery> getQueries() {
		return queries;
	}

	public void setQueries(List<ScheduledQuery> queries) {
		this.queries = queries;
	}
}
