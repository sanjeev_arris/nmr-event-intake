package com.arris.nmra

import org.slf4j.LoggerFactory
import java.lang.{Double => JDouble}


import scala.io.Source

/**
  * Hello world!
  *
  */
object Simulator extends App {
  val logger = LoggerFactory.getLogger(getClass)
  logger.info("hello world!")

  //    val cmtsUtil5Num = Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("data/distribution.txt")).
  //      getLines().filter(!_.startsWith("#")).
  //      map(CMTSUtilWith5Num(_)).toIndexedSeq
  //    logger.info(cmtsUtil5Num.toString())

  val cmtsUtil = Source.fromInputStream(this.getClass.getClassLoader.getResourceAsStream("data/sg_stats_ds.txt")).
    getLines().filter(!_.startsWith("#")).
    map(CMTSUtil(_)).toIndexedSeq

  val in = this.getClass.getClassLoader.getResourceAsStream("data/sg_stats_ds.txt")

//  var bytez = Array[Byte](1024)
//  var read = -1
//  while ( (read=in.read(bytez))!=-1)
//    logger.info(cmtsUtil.toString())
//  //NormalDistribution
}

case class CMTSUtilWith5Num(line: String) {
  val (name: String, percUtilMin: Double, percUtilQu1st: Double, percUtilMed: Double, percUtilMean: Double, percUtilQu3rd: Double, percUtilMax: Double, percUtilSD: Double,
  kbpsMin: Double, kbpsQu1st: Double, kbpsMed: Double, kbpsMean: Double, kbpsQu3rd: Double, kbpsMax: Double, kbpsSD: Double) = line.split(",") match {
    case arr: Array[String] => (arr(0),
      JDouble.parseDouble(arr(1)), JDouble.parseDouble(arr(2)), JDouble.parseDouble(arr(3)), JDouble.parseDouble(arr(4)), JDouble.parseDouble(arr(5)),
      JDouble.parseDouble(arr(6)), JDouble.parseDouble(arr(7)), JDouble.parseDouble(arr(8)), JDouble.parseDouble(arr(9)), JDouble.parseDouble(arr(10)),
      JDouble.parseDouble(arr(11)), JDouble.parseDouble(arr(12)), JDouble.parseDouble(arr(13)), JDouble.parseDouble(arr(14))
      )
    case _ => ("", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
  }
}

case class CMTSUtil(line: String) {
  var ts: Long = _
  var (dummy: String, name: String, macName: String, percUtil: Double, kbps: Double, uniqueCM: Int, onlineCM: Int, cmCount: Int) = line.split(",") match {
    case arr: Array[String] => (arr(0), arr(1), arr(2),
      JDouble.parseDouble(arr(3)), JDouble.parseDouble(arr(4)), Integer.parseInt(arr(5)), Integer.parseInt(arr(6)), Integer.parseInt(arr(7)))
    case _ => ("", "", "", 0, 0, 0, 0, 0)
  }
}

