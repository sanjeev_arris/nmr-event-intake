package com.arris.nmra.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScheduledTask {
	public static enum ScheduleType { CRON, INTERVAL, DELAY, ONCE }

	public static class Task {
		private String operation;
		private Map params = new HashMap();

		public String getOperation() {
			return operation;
		}
		public void setOperation(String operation) {
			this.operation = operation;
		}

		@Override
		public String toString() {
			return "Task{" +
				", operation='" + operation + '\'' +
				'}';
		}

		public Map<String, String> getParams() {
			return params;
		}

		public void setParams(Map<String, String> params) {
			this.params = params;
		}
	}

	private ScheduleType scheduleType;
	private String schedule;
	private List<Task> tasks;

	public ScheduleType getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
}
