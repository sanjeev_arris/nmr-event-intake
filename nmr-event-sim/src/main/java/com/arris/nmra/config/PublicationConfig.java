package com.arris.nmra.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@ConfigurationProperties("publications")
public class PublicationConfig {
    private List<ScheduledTask> tasks;

    public List<ScheduledTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<ScheduledTask> tasks) {
        this.tasks = tasks;
    }
}
