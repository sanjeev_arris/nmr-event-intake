package com.arris.nmra.config;

import com.arris.nmra.service.DataListener;
import com.arris.nmra.service.Simulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by smishra on 12/22/16.
 */
@Configuration
@EnableScheduling
@Import({PublicationConfig.class})
public class DataPublisher implements SchedulingConfigurer {
    private static final Logger logger = LoggerFactory.getLogger(DataPublisher.class);

    @Autowired
    private Simulator simulator;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private PublicationConfig publicationConfig;

    Map<String, Collection<CronTask>> cronTaskMap = new HashMap<>();

    @PostConstruct
    private void init() {
        template.setMessageConverter(new Jackson2JsonMessageConverter());
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        publicationConfig.getTasks().forEach(q -> {
            switch (q.getScheduleType()) {
                case CRON:
                    logger.info(String.format("dataAggregator: configureTasks: configuring cronTasks: %s -> %s", q.getSchedule(), q.getTasks()));
                    CronTask ct = new CronTask(toRunnable(q.getTasks()), q.getSchedule());
                    scheduledTaskRegistrar.addCronTask(ct);
                    cronTaskMap.putIfAbsent(q.getSchedule(), new ArrayList<>());
                    cronTaskMap.get(q.getSchedule()).add(ct);
                    break;
                case DELAY:
                case INTERVAL:
                case ONCE:
                    scheduleOnce(q.getSchedule(), q.getTasks());
                default:
                    logger.warn("Unsupported schedule type: " + q.getScheduleType());
            }
        });
    }

    private void scheduleOnce(String schedule, List<ScheduledTask.Task> tasks) {
        try {
            tasks.forEach(t -> {
                Object data = simulator.simulate(t.getOperation(), t.getParams(), (collection) -> {
                    if (collection!=null && !collection.isEmpty()) {
                        template.convertAndSend(exchange, t.getParams().get("routing").toString(), collection);
                    }
                });
                if (data instanceof Boolean && !((Boolean) data)) {
                    logger.info("Skipping ...." + t.getOperation());
                    return;
                }
                template.convertAndSend(exchange, t.getParams().get("routing").toString(), data);
            });

        }
        catch (Throwable t) {
            logger.warn("error in scheduleOnce", t);
        }
    }

    Runnable toRunnable(List<ScheduledTask.Task> tasks) {
        return () -> {
            try {
                tasks.forEach(t -> {
                    Object data = simulator.simulate(t.getOperation(), t.getParams(), null);
                    if (data instanceof Boolean && !((Boolean) data)) {
                        logger.info("Skipping ...." + t.getOperation());
                        return;
                    }
                    template.convertAndSend(exchange, t.getParams().get("routing").toString(), data);
                });
            } catch (Throwable e) {
                logger.warn("Exception while running scheduledTask", e);
            }
        };
    }

    void setRabbitTemplate(RabbitTemplate template) {
        this.template = template;
    }
}
