package com.arris.nmra.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.util.Date;

/**
 * Created by smishra on 11/10/16.
 */
public class Util {
    private static final Logger log = LoggerFactory.getLogger(Util.class);
    private static SimpleDateFormat WITH_HOUR = new SimpleDateFormat("yyyy-MM-dd HH");
    private static SimpleDateFormat WITH_HOUR_MINUTE = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    static public Long toMillis(String ts, Long defaultValue) {
        if (StringUtils.isEmpty(ts)) return defaultValue;
        Long millis = defaultValue;
        try {
            millis = Long.parseLong(ts);
        } catch (NumberFormatException nfe) {
            try {
                millis = toMillisFromUTC(ts);
            } catch (DateTimeParseException e) {
                try {
                    millis = WITH_HOUR.parse(ts).getTime();
                } catch (ParseException e1) {
                    try {
                        millis = WITH_HOUR_MINUTE.parse(ts).getTime();
                    } catch (ParseException e2) {
                        log.warn("Error parsing ts: " + ts + " with format: " + WITH_HOUR_MINUTE.toString());
                    }
                }
            }
        }
        return millis;
    }

    static public Long toMillisFromUTC(String utc) {
        return Instant.parse(utc).toEpochMilli();
    }

    static public String toUTC(Long ts) {
        Date date = new Date(ts);
        return date.toInstant().toString();
    }

    public static InputStream streamFromResource(String fileName) {
        InputStream is = null;
        try {
            if (fileName.startsWith("classpath:")) {
                fileName = fileName.substring("classpath:".length());
                is = Util.class.getClassLoader().getResourceAsStream(fileName);
            } else {
                is = Util.class.getClassLoader().getResourceAsStream(fileName);
                if (is == null) {
                    log.debug(String.format("resource %s not found in classpath, will try to use filesystem", fileName));
                    is = new FileInputStream(fileName);
                    if (is == null) {
                        log.debug(String.format("resource %s not found in filesystem", fileName));
                    }
                }
            }
        } catch (Throwable t) {
            log.warn("Error loading resource from: " + fileName, t);
        }

        return is;
    }

    public static byte[] bytesFromResource(String fileName) {
        InputStream is = streamFromResource(fileName);
        byte[] bytes = new byte[0];
        if (is != null) {
            BufferedInputStream bis = new BufferedInputStream(is, 1024);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int readCount = -1;
            bytes = new byte[1024];
            try {
                while ((readCount = bis.read(bytes)) != -1) {
                    baos.write(bytes, 0, readCount);
                }

                bytes = baos.toByteArray();
            } catch (IOException e) {
                log.warn("Error reading inputstream for file: " + fileName, e);
            }
        }

        return bytes;
    }

    public static boolean extractBool(String value) {
        if (StringUtils.isEmpty(value)) return false;
        char first = value.trim().toLowerCase().charAt(0);
        return first == 't' || first == '1' || first == 'y' ? true : false;
    }

    public static String unquote(String str) {
        if (str.charAt(0)=='"') str = str.substring(1);
        if (str.charAt(str.length()-1)=='"') str = str.substring(0, str.length()-1);
        return str;
    }

    public static String quote(String str) {
        if (str==null)return null;
        if (!str.startsWith("\"")) str = "\"" + str;
        if (!str.endsWith("\"")) str = str + "\"";
        return str;
    }

    static public String createStringFrom(String source, int length) {
        return createStringFrom(source, length, " ");
    }

    static public String createStringFrom(String source, int length, String padding) {
        return createStringFrom(source, length, padding, true);
    }

    static public String createStringFrom(String source, int length, String padding, boolean padRight) {
        if (source == null) source = "";
        String sub = StringUtils.substring(source, 0, length);
        if (padRight) {
            return StringUtils.rightPad(sub, length, padding);
        } else {
            return StringUtils.leftPad(sub, length, padding);
        }
    }

    static public String createQuotedStringFrom(String source, int length, boolean pad, String padding) {
        if (source == null) source = "";
        if (pad)
            return "\"" + createStringFrom(source, length, padding) + "\"";
        else {
            if (source.length() > length) source = source.substring(0, length);
            return "\"" + source + "\"";
        }
    }
}
