package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by smishra on 11/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Model(value = "sg_stats_raw", preInsertCheck = "if not exists ")
public class SgStatsData {
    @ModelAttr(value = "tenant_id", preCheck = true)
    Integer tenantId;

    @ModelAttr(preCheck = true)
    String cmts;

    @ModelAttr(value = "cable_mac", preCheck = true)
    String cableMac;

    @JsonProperty("sg_id")
    @ModelAttr(value = "sg_id", preCheck = true)
    String sgId;

    @JsonProperty("sg_pct_util")
    @ModelAttr(value = "sg_pct_util")
    double sgPctUtil;

    @JsonProperty("sg_kbps")
    @ModelAttr("sg_kbps")
    double sgKbps;

    @JsonProperty("sg_max_cms")
    @ModelAttr("sg_max_cms")
    int sgMaxCms;

    @JsonProperty("sg_max_online")
    @ModelAttr("sg_max_online")
    int sgMaxOnline;

    Integer year;
    Integer month;
    Integer wk;
    Integer day;
    Integer hour;
    Integer minute;

    @ModelAttr(preCheck = true)
    Long ts;

    @ModelAttr(preCheck = true)
    int downstream;

    @JsonProperty("created_at")
    @ModelAttr(value = "created_at")
    Long createdAt = System.currentTimeMillis();

    public SgStatsData() {
    }

    public SgStatsData(String cmts, String cableMac, String sgId, double sgPctUtil, double sgKbps, long ts) {
        this(cmts, cableMac, sgId, sgPctUtil, sgKbps, false, ts);
    }

    public SgStatsData(String cmts, String cableMac, String sgId, Double sgPctUtil, Double sgKbps, boolean downstream, long now) {
        this.setCmts(cmts);
        this.setCableMac(cableMac);
        this.setSgId(sgId);
        this.setSgPctUtil(sgPctUtil);
        this.setSgKbps(sgKbps);
        this.setDownstream(downstream ? 1 : 0);
        this.setTs(now);
    }

    public void setTs(String ts) {
        this.setTs(Util.toMillis(ts, System.currentTimeMillis()));
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public String getCableMac() {
        return cableMac;
    }

    public void setCableMac(String cableMac) {
        this.cableMac = cableMac;
    }

    public String getSgId() {
        return sgId;
    }

    public void setSgId(String sgId) {
        this.sgId = sgId;
    }

    public double getSgPctUtil() {
        return sgPctUtil;
    }

    public void setSgPctUtil(double sgPctUtil) {
        this.sgPctUtil = sgPctUtil;
    }

    public double getSgKbps() {
        return sgKbps;
    }

    public void setSgKbps(double sgKbps) {
        this.sgKbps = sgKbps;
    }

    public int getSgMaxCms() {
        return sgMaxCms;
    }

    public void setSgMaxCms(int sgMaxCms) {
        this.sgMaxCms = sgMaxCms;
    }

    public int getSgMaxOnline() {
        return sgMaxOnline;
    }

    public void setSgMaxOnline(int sgMaxOnline) {
        this.sgMaxOnline = sgMaxOnline;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
        DateTime dt = new DateTime(this.ts);
        this.year = dt.getYear();
        this.month = dt.getMonthOfYear();
        this.wk = dt.getWeekOfWeekyear();
        if (dt.getWeekyear()!=this.year) this.wk = 0;
        this.day = dt.getDayOfMonth();
        this.hour = dt.getHourOfDay();
        this.minute = dt.getMinuteOfHour();
    }

    public int getDownstream() {
        return downstream;
    }

    public void setDownstream(int downstream) {
        this.downstream = downstream;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }

    public Integer getTenantId() {
        return tenantId;
    }


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getWk() {
        return wk;
    }

    public void setWk(Integer wk) {
        this.wk = wk;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return "StatsData{" +
                "tenantId=" + tenantId +
                ", cmts='" + cmts + '\'' +
                ", cableMac='" + cableMac + '\'' +
                ", sgId='" + sgId + '\'' +
                ", sgPctUtil=" + sgPctUtil +
                ", sgKbps=" + sgKbps +
                ", sgMaxCms=" + sgMaxCms +
                ", sgMaxOnline=" + sgMaxOnline +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", hour=" + hour +
                ", minute=" + minute +
                ", ts=" + ts +
                ", downstream=" + downstream +
                ", createdAt=" + createdAt +
                '}';
    }
}
