package com.arris.nmra.model;

/**
 * Created by smishra on 1/11/17.
 */
public class CMTSUtilization {
    private int topologyId;
    private String market;
    private String hub;
    private String cmts;
    private int cmCount;

    private int upstreamSGCount;
    private double upstreamAvgUtil;
    private int upstreamOverCapacityCount;
    private double upstreamOverCapacityPercent;

    private int downstreamSGCount;
    private double downstreamAvgUtil;
    private int downstreamOverCapacityCount;
    private double downstreamOverCapacityPercent;

    public CMTSUtilization(){}

    public CMTSUtilization(int topologyId, String market, String hub, String cmts) {
        this.topologyId = topologyId;
        this.market = market;
        this.hub = hub;
        this.cmts = cmts;
    }

    public int getTopologyId() {
        return topologyId;
    }

    public void setTopologyId(int topologyId) {
        this.topologyId = topologyId;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public int getUpstreamSGCount() {
        return upstreamSGCount;
    }

    public void setUpstreamSGCount(int upstreamSGCount) {
        this.upstreamSGCount = upstreamSGCount;
    }

    public double getUpstreamAvgUtil() {
        return upstreamAvgUtil;
    }

    public void setUpstreamAvgUtil(double upstreamAvgUtil) {
        this.upstreamAvgUtil = upstreamAvgUtil;
    }

    public int getUpstreamOverCapacityCount() {
        return upstreamOverCapacityCount;
    }

    public void setUpstreamOverCapacityCount(int upstreamOverCapacityCount) {
        this.upstreamOverCapacityCount = upstreamOverCapacityCount;
    }

    public double getUpstreamOverCapacityPercent() {
        return upstreamOverCapacityPercent;
    }

    public void setUpstreamOverCapacityPercent(double upstreamOverCapacityPercent) {
        this.upstreamOverCapacityPercent = upstreamOverCapacityPercent;
    }

    public int getDownstreamSGCount() {
        return downstreamSGCount;
    }

    public void setDownstreamSGCount(int downstreamSGCount) {
        this.downstreamSGCount = downstreamSGCount;
    }

    public double getDownstreamAvgUtil() {
        return downstreamAvgUtil;
    }

    public void setDownstreamAvgUtil(double downstreamAvgUtil) {
        this.downstreamAvgUtil = downstreamAvgUtil;
    }

    public int getDownstreamOverCapacityCount() {
        return downstreamOverCapacityCount;
    }

    public void setDownstreamOverCapacityCount(int downstreamOverCapacityCount) {
        this.downstreamOverCapacityCount = downstreamOverCapacityCount;
    }

    public double getDownstreamOverCapacityPercent() {
        return downstreamOverCapacityPercent;
    }

    public void setDownstreamOverCapacityPercent(double downstreamOverCapacityPercent) {
        this.downstreamOverCapacityPercent = downstreamOverCapacityPercent;
    }

    public int getCmCount() {
        return cmCount;
    }

    public void setCmCount(int cmCount) {
        this.cmCount = cmCount;
    }
}
