package com.arris.nmra.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;

/**
 * Created by smishra on 1/11/17.
 */
public class UtilizationNode {
    private Integer topologyId;
    private String name;
    private String path = "";
    private NodeType type;
    private int cms;
    private Utilization upstreamProjected =new Utilization();
    private Utilization downstreamProjected =new Utilization();
    private boolean hasChildren = false;
    @JsonIgnore
    Collection<UtilizationNode> children = new ArrayList<>();

    public Integer getTopologyId() {
        return topologyId;
    }

    public void setTopologyId(Integer topologyId) {
        this.topologyId = topologyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public NodeType getType() {
        return type;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public int getCms() {
        return children.isEmpty() ?
                cms :
                this.children.stream().map(c -> c.getCms()).mapToInt(Integer::new).sum();
    }

    public void setCms(int cms) {
        this.cms = cms;
    }

    public Utilization getUpstreamProjected() {
        return children.isEmpty() ?
            upstreamProjected :
            this.children.stream().map(c -> c.getUpstreamProjected()).reduce(null, (x, y) -> merge(x, y));
    }

    public static Utilization merge(Utilization left, Utilization right) {
        if (left==null) return right;
        Utilization merged = new Utilization();
        merged.count = left.count + right.count;
        merged.serviceGroups = left.serviceGroups + right.serviceGroups;
        merged.overCapacityCount = left.overCapacityCount + right.overCapacityCount;
        merged.overCapacityPercent = (left.overCapacityPercent*left.count + right.overCapacityPercent*right.count) /merged.count;
        merged.avgUtil = (left.avgUtil*left.count + right.avgUtil*right.count)/merged.count;

        return merged;
    }

    public void setUpstreamProjected(Utilization upstreamProjected) {
        this.upstreamProjected = upstreamProjected;
    }

    public Utilization getDownstreamProjected() {
        return children.isEmpty() ?
            downstreamProjected :
            this.children.stream().map(c -> c.getDownstreamProjected()).reduce(null, (x, y) -> merge(x, y));
    }

    public void setDownstreamProjected(Utilization downstreamProjected) {
        this.downstreamProjected = downstreamProjected;
    }

    public boolean hasChildren() {
        return !children.isEmpty();
    }

    static public enum NodeType {
        market, hub, cmts;
    }

    static public class Utilization {
        @JsonIgnore
        public int count=1;
        public double avgUtil;
        public int serviceGroups;
        public double overCapacityPercent;
        public int overCapacityCount;
    }

    static public Collection<UtilizationNode> toUtilizationNodes(Collection<CMTSUtilization> cmtsUtils) {
        Map<String, Map<String, Collection<CMTSUtilization>>> cmtsByMarketAndHub = new HashMap<>();

        cmtsUtils.forEach(u -> {
            cmtsByMarketAndHub.putIfAbsent(u.getMarket(), new HashMap<>());
            cmtsByMarketAndHub.get(u.getMarket()).putIfAbsent(u.getHub(), new ArrayList<>());
            cmtsByMarketAndHub.get(u.getMarket()).get(u.getHub()).add(u);
        });

        Collection<UtilizationNode> utilizationNodes = new ArrayList<>();

        cmtsByMarketAndHub.entrySet().forEach(e -> appendUtilizationNodes(e, utilizationNodes));

        return utilizationNodes;
    }

    private static void appendUtilizationNodes(Map.Entry<String, Map<String, Collection<CMTSUtilization>>> e, Collection<UtilizationNode> utilizationNodes) {
        String market = e.getKey();
        final UtilizationNode marketNode = new UtilizationNode();
        marketNode.setName(market);
        marketNode.setType(NodeType.market);

        utilizationNodes.add(marketNode);

        e.getValue().entrySet().forEach(h -> {
            final UtilizationNode hubNode = new UtilizationNode();
            marketNode.children.add(hubNode);

            hubNode.setName(h.getKey());
            hubNode.path = marketNode.getName();
            hubNode.setType(NodeType.hub);
            utilizationNodes.add(hubNode);

            h.getValue().forEach(c -> {
                UtilizationNode cmts = new UtilizationNode();
                hubNode.children.add(cmts);

                cmts.setName(c.getCmts());
                cmts.setType(NodeType.cmts);
                cmts.path = hubNode.path + "/" + hubNode.name;
                utilizationNodes.add(cmts);

                cmts.setCms(c.getCmCount());
                cmts.setTopologyId(c.getTopologyId());
                cmts.setUpstreamProjected(getUtilization(c, false));
                cmts.setDownstreamProjected(getUtilization(c, true));
            });
        });
    }

    private static Utilization getUtilization(CMTSUtilization cmUtil, boolean downstream) {
        Utilization utilization = new Utilization();
        if (downstream) {
            utilization.serviceGroups = cmUtil.getDownstreamSGCount();
            utilization.avgUtil = cmUtil.getDownstreamAvgUtil();
            utilization.overCapacityCount = cmUtil.getDownstreamOverCapacityCount();
            utilization.overCapacityPercent = cmUtil.getDownstreamOverCapacityPercent();
        } else {
            utilization.serviceGroups = cmUtil.getUpstreamSGCount();
            utilization.avgUtil = cmUtil.getUpstreamAvgUtil();
            utilization.overCapacityCount = cmUtil.getUpstreamOverCapacityCount();
            utilization.overCapacityPercent = cmUtil.getUpstreamOverCapacityPercent();
        }
        return utilization;
    }
}
