package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by smishra on 11/10/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Model(value = "ch_stats_raw")
public class ChStatsData {
    @ModelAttr(value = "tenant_id", preCheck = true)
    Integer tenantId;

    @ModelAttr(preCheck = true)
    String cmts;

    @ModelAttr(value = "cable_mac", preCheck = true)
    String cableMac;

    @JsonProperty("sg_id")
    @ModelAttr(value = "sg_id", preCheck = true)
    String sgId;

    @ModelAttr(preCheck = true)
    String channel;

    @JsonProperty("ch_pct_util")
    @ModelAttr("ch_pct_util")
    double chPctUtil;

    @JsonProperty("ch_kbps")
    @ModelAttr("ch_kbps")
    int chKbps;

    @JsonProperty("ch_max_cms")
    @ModelAttr("ch_max_cms")
    int chMaxCms;

    @JsonProperty("ch_max_online")
    @ModelAttr("ch_max_online")
    int chMaxOnline;

    Integer year;
    Integer month;
    Integer wk;
    Integer day;
    Integer hour;
    Integer minute;

    @ModelAttr(preCheck = true)
    Long ts;

    @ModelAttr(preCheck = true)
    int downstream;

    @JsonProperty("created_at")
    @ModelAttr(value = "created_at")
    Long createdAt = System.currentTimeMillis();

    public void setTs(String ts) {
        this.setTs(Util.toMillis(ts, System.currentTimeMillis()));
    }

    public String getCmts() {
        return cmts;
    }

    public void setCmts(String cmts) {
        this.cmts = cmts;
    }

    public String getCableMac() {
        return cableMac;
    }

    public void setCableMac(String cableMac) {
        this.cableMac = cableMac;
    }

    public String getSgId() {
        return sgId;
    }

    public void setSgId(String sgId) {
        this.sgId = sgId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public double getChPctUtil() {
        return chPctUtil;
    }

    public void setChPctUtil(double chPctUtil) {
        this.chPctUtil = chPctUtil;
    }

    public int getChKbps() {
        return chKbps;
    }

    public void setChKbps(int chKbps) {
        this.chKbps = chKbps;
    }

    public int getChMaxCms() {
        return chMaxCms;
    }

    public void setChMaxCms(int chMaxCms) {
        this.chMaxCms = chMaxCms;
    }

    public int getChMaxOnline() {
        return chMaxOnline;
    }

    public void setChMaxOnline(int chMaxOnline) {
        this.chMaxOnline = chMaxOnline;
    }

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
        DateTime dt = new DateTime(this.ts);
        this.year = dt.getYear();
        this.month = dt.getMonthOfYear();
        this.wk = dt.getWeekOfWeekyear();
        if (dt.getWeekyear()!=this.year) this.wk = 0;
        this.day = dt.getDayOfMonth();
        this.hour = dt.getHourOfDay();
        this.minute = dt.getMinuteOfHour();
    }

    public int getDownstream() {
        return downstream;
    }

    public void setDownstream(int downstream) {
        this.downstream = downstream;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setTenantId(Integer tenantId) {
        this.tenantId = tenantId;
    }
    public Integer getTenantId() {
        return tenantId;
    }


    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getWk() {
        return wk;
    }

    public void setWk(Integer wk) {
        this.wk = wk;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinute() {
        return minute;
    }

    public void setMinute(Integer minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return "StatsData{" +
            "tenantId=" + tenantId +
            ", cmts='" + cmts + '\'' +
            ", cableMac='" + cableMac + '\'' +
            ", sgId='" + sgId + '\'' +
            ", channel='" + channel + '\'' +
            ", chPctUtil=" + chPctUtil +
            ", chKbps=" + chKbps +
            ", chMaxCms=" + chMaxCms +
            ", chMaxOnline=" + chMaxOnline +
            ", year=" + year +
            ", month=" + month +
            ", day=" + day +
            ", hour=" + hour +
            ", minute=" + minute +
            ", ts=" + ts +
            ", downstream=" + downstream +
            ", createdAt=" + createdAt +
            '}';
    }
}
