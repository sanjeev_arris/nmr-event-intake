package com.arris.nmra.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by smishra on 7/12/16.
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelAttr {
    String value() default "";
    String param() default "";
    boolean ignore() default false;
    boolean quoted() default false;
    boolean padded() default false;
    String padding() default " ";
    boolean padRight() default true;
    String format() default "";
    int size() default -1;
    int minLength() default 0;
    int maxLength() default 255;
    boolean preCheck() default false;
}
