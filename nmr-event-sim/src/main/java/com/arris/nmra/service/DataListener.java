package com.arris.nmra.service;

/**
 * Created by smishra on 1/26/17.
 */
public interface DataListener<T> {
    void handle(T t);
}
