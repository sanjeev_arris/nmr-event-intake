package com.arris.nmra.service;

import com.arris.nmra.model.ModelUtil;
import com.arris.nmra.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by smishra on 11/28/16.
 */
@Component
public class DBService {
    private static final Logger log = LoggerFactory.getLogger(DBService.class);
    private static final int MAX_BATCH_SIZE = 20;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private DataSource dataSource;

    private String sqlDelimitar = ";";

    public void executeUpdate(String stmt) throws SQLException {
        jdbcTemplate.execute(stmt);
    }

    public int queryForCount(String query) throws SQLException {
        return jdbcTemplate.queryForObject(query, Integer.class);
    }

    public  <T> Collection<Integer> writeToDB(Collection<T> data, String dataName) {
        List<ModelUtil.ColumnData> columnDatas = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(data, columnDatas);
        if (stmt.trim().isEmpty()) {
            log.warn("Can't create preparedStatment for " + dataName);
            return Collections.EMPTY_LIST;
        }
        try {
            return executePreparedStatement(stmt, columnDatas);
        } catch (Throwable e) {
            log.warn(String.format("Error executing prepared statement %s for %s, cause: %s", stmt, dataName, e.getMessage()), e);
        }
        return Collections.EMPTY_LIST;
    }

    public void executeUpdatesInTransaction(Collection<String> stmts) throws DataSourceException {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            statement = connection.createStatement();
            for (String s : stmts) {
                long start = System.currentTimeMillis();
                log.debug("executeUpdatesInTransaction: " + s);
                statement.execute(s);

                log.debug(String.format("updated: %s in %s msec stmt: %s",statement.getUpdateCount(), (System.currentTimeMillis()-start), s));
            }
            connection.commit();
        } catch (SQLException e) {
            String msg = String.format("DBService: executeUpdateInTransaction: exception for stmts: %s, details: %s", stmts,
                    e.getNextException() != null ? e.getNextException().getMessage() : e.getMessage());
            log.warn(msg, e);
            throw new DataSourceException(msg, e);
        } finally {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                }

                if (statement != null) try {
                    statement.close();
                } catch (SQLException e1) {
                }

                try {
                    connection.close();
                } catch (SQLException e) {

                }

            }
        }
    }

    public Collection<Integer> executePreparedStatement(String stmt, List<ModelUtil.ColumnData> columnDataList) throws DataSourceException {
        if (columnDataList == null || columnDataList.isEmpty()) return Collections.EMPTY_LIST;
        Connection connection = null;
        PreparedStatement ps = null;
        stmt = stmt + sqlDelimitar;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(stmt);

            /*  find the total number of rows for which data needs to be inserted -
                all columns will have same number of rows so just use the first entry to
                get row count
            */
            Collection<Object> rows = columnDataList.get(0).values;
            Collection<Integer> psExecResult = new ArrayList<>(rows.size());

            int batchIndex = 0;
            for (int rowIndex = 0; rowIndex < rows.size(); ++rowIndex, ++batchIndex) {
                if (batchIndex == MAX_BATCH_SIZE) {
                    int[] result = ps.executeBatch();
                    for (int r : result) psExecResult.add(r);
                    batchIndex = 0;
                }
                for (int colIndex = 0; colIndex < columnDataList.size(); ++colIndex) {
                    ModelUtil.ColumnData columnData = columnDataList.get(colIndex);
                    Object obj = columnData.values.get(rowIndex);

                    if (columnData.type.equals(String.class)) {
                        if (obj != null) ps.setString(colIndex + 1, String.valueOf(obj).trim());
                        else ps.setNull(colIndex + 1, Types.VARCHAR);
                    } else if (Integer.class.isAssignableFrom(columnData.type)) {
                        if (obj != null) ps.setInt(colIndex + 1, (Integer) obj);
                        else ps.setNull(colIndex + 1, Types.NUMERIC);
                    } else if (Double.class.isAssignableFrom(columnData.type)) {
                        if (obj != null) ps.setDouble(colIndex + 1, (Double) obj);
                        else ps.setNull(colIndex + 1, Types.NUMERIC);
                    } else if (Date.class.isAssignableFrom(columnData.type)) {
                        if (obj != null) ps.setDate(colIndex + 1, new java.sql.Date(((Date) obj).getTime()));
                        else ps.setNull(colIndex + 1, Types.DATE);
                    } else
                        ps.setObject(colIndex + 1, obj);
                }
                ps.addBatch();
            }
            if (batchIndex > 0) {
                int[] result = ps.executeBatch();
                Collection<Integer> coll = new ArrayList<>();
                for (int r : result) coll.add(r);

                log.info(String.format("batchIndex %d res of executeBatch: ", batchIndex, coll));

                for (int r : result) psExecResult.add(r);
            }
            connection.commit();
            return psExecResult;
        } catch (SQLException e) {
            String msg = String.format("DBService: executePreparedStatement: exception for stmts: %s, details: %s", stmt,
                    e.getNextException() != null ? e.getNextException().getMessage() : e.getMessage());
            log.warn(msg, e);
            throw new DataSourceException(msg, e);
        } finally {
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                }
                try {
                    if (ps != null) try {
                        ps.close();
                    } catch (SQLException e1) {
                    }
                    connection.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    public void createPreparedStatement(String query, Map<String, Object> queryParams) throws MissingParamException, DataSourceException {
        for (Map.Entry<String, Object> e : queryParams.entrySet()) {
            String key = e.getKey().trim();
            if (!key.startsWith("__")) key = "__" + key;
            Object value = e.getValue();
            if (value instanceof String) value = Util.quote(value.toString());
            query = query.replace(key, value.toString());
        }
        Set<String> missingParams = findRequiredParams(query);
        if (!missingParams.isEmpty()) {
            throw new MissingParamException("No value found for params: " + String.valueOf(missingParams));
        }
    }

    public Set<String> findRequiredParams(String query) {
        Set<String> params = new HashSet<>();
        int __index = query.indexOf("__");
        while (__index > -1) {
            String missingParam = query.substring(__index);
            int spIndex = missingParam.indexOf(" ");

            if (spIndex > -1)
                missingParam = missingParam.substring(0, spIndex);
            else
                missingParam = missingParam.substring(0);
            missingParam = missingParam.trim();

            if (missingParam.endsWith(",")) missingParam = missingParam.substring(0, missingParam.length() - 1);
            if (missingParam.endsWith(")")) missingParam = missingParam.substring(0, missingParam.length() - 1);

            params.add(missingParam);
            query = query.substring(__index + missingParam.length());
            __index = query.indexOf("__");
        }

        return params;
    }


    @PostConstruct
    private void init() {
        dataSource = jdbcTemplate.getDataSource();
    }
}

