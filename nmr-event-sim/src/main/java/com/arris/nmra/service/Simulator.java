package com.arris.nmra.service;

import com.arris.nmra.model.SgStatsData;
import com.arris.nmra.model.TopologyData;
import com.arris.nmra.util.Util;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by smishra on 1/23/17.
 */
@Component
public class Simulator {
    private static final Logger logger = LoggerFactory.getLogger(Simulator.class);
    double onePerc = 10207.75;
    double mean = 40.25;
    double sd = 21.56168;
    double fudge = 0.0;
    NormalDistribution dist = new NormalDistribution(mean, sd);

    public TopologyData toTopology(String csv) {
        String[] parts = csv.split(",");
        long now = System.currentTimeMillis();
        return new TopologyData(parts[0], parts[1], parts[2], now);
    }

    public Collection<TopologyData> genTopology() {
        String data = new String(Util.bytesFromResource("data/topology.csv"));
        List<TopologyData> topologyList = Arrays.stream(data.split("\n")).map(line -> toTopology(line)).collect(Collectors.toList());
        try {
            logger.info("topology size: " + topologyList.size());
        } catch (Exception e) {
            logger.warn("Cant convert topology to json", e);
        }
        return topologyList;
    }

    public void setMeanAndSD(double mean, double sd) {
        this.mean = mean;
        this.sd = sd;
        dist = new NormalDistribution(mean, sd);
    }

    public Pair<Double, Double> getMeanAndSD() {
        return Pair.of(mean, sd);
    }

    public Collection<SgStatsData> genSgStats(long timestamp, boolean downstream) {
        Simulator simulator = new Simulator();
        Collection<TopologyData> topology = simulator.genTopology();
        logger.info("total topologies: " + topology.size());
        Collection<String> cableMacs = genCableMac();
        return genSgStats(topology, cableMacs, timestamp, downstream);
    }

    public Collection<SgStatsData> genSgStats(Collection<TopologyData> topology, Collection<String> cableMacs, long timestamp, boolean downstream) {
        long now = timestamp;

        Collection<SgStatsData> sgData = topology.stream().map(t -> {
            return cableMacs.stream().map(cm -> {
                Pair<Double, Double> data = getPercUtil();
                SgStatsData sgStatsData = new SgStatsData(t.getCmts(), cm, 1 + "", data.getFirst(), data.getSecond(), downstream, now);
                sgStatsData.setSgMaxCms(190);
                sgStatsData.setSgMaxOnline(189);
                return sgStatsData;
            }).collect(Collectors.toList());
        }).flatMap(m -> m.stream()).collect(Collectors.toList());
        return sgData;
    }


    public Collection<SgStatsData> genSgStats(boolean downstream) {
        return genSgStats(System.currentTimeMillis(), downstream);
    }

    public Collection<String> genCableMac() {
        Collection<String> cableMacs = new ArrayList<>();
        for (int i = 5; i < 8; ++i) {
            for (int j = 1; j < 2; ++j) {
                for (int k = 0; k < 9; ++k) {
                    cableMacs.add(String.format("Cable%d/%d/%d", i, j, k));
                }
            }
        }
        return cableMacs;
    }

    public void genOneYear(Map<String, String> params, DataListener<Collection> listener) throws IOException {
        fudge =0.0;
        double mean = 9.733031;
        double sd = 5.564968;
        boolean dowstream = false;
        int x = 0;
        double gradient = .301;
        double intercept = mean;

        if (params.get("mean") != null) {
            mean = Double.parseDouble(params.get("mean"));
        }
        if (params.get("sd") != null) {
            sd = Double.parseDouble(params.get("sd"));
        }
        if (params.get("gradient") != null) {
            gradient = Double.parseDouble(params.get("gradient"));
        }
        if (params.get("intercept") != null) {
            intercept = Double.parseDouble(params.get("intercept"));
        }
        else {
            intercept = mean;
        }
        if (params.get("downstream") != null && Boolean.parseBoolean(params.get("downstream"))) {
            dowstream = true;
        }
        logger.info(String.format("genOneYear: downstream: %s, mean: %s, sd: %s, gradient: %s", dowstream, mean, sd, gradient));
        this.setMeanAndSD(mean, sd);

        Collection<TopologyData> topology = genTopology();
        //topology = Collections.singleton(topology.iterator().next());

        if (listener != null) listener.handle(topology);

        Collection<String> cableMacs = genCableMac();

        Map<String, OutputStream> osMap = createOutputStream(topology, params);

        for (int month = 1; month <= 12; month++) {
            for (int day = 1; day <= 31; day++) {
                try {
                    DateTime dt = new DateTime(2016, month, day, 0, 0);
                    fudge = nextY(++x, gradient, intercept);
                    logger.info(String.format("month: %s, day: %s, x:%s, m:%s, c:%s, fudge:%s", month, day, x, gradient,intercept, fudge));

                    genSgStats(topology, cableMacs, dt.getMillis(), dowstream).stream().
                            collect(Collectors.groupingBy(d -> d.getCmts())).entrySet().forEach(e -> {
                        try {
                            write(e.getValue(), osMap.get(e.getKey()), listener);
                        } catch (IOException e1) {
                            logger.warn("Error writing data out", e1);
                        }
                    });

                } catch (Exception e) {
                    logger.warn("Failed to write data: ", e.getMessage());
                }
            }
        }
        closeFOS(osMap);
    }

    public double nextY(int x, double gradient, double intercept) {
        return gradient * x + intercept;
    }

    private Map<String, OutputStream> createOutputStream(Collection<TopologyData> topology, Map<String, String> params) {
        Map<String, OutputStream> osMap = new HashMap<>();
        if (params.get("file") != null && Boolean.parseBoolean(params.get("file"))) {
            topology.stream().map(t -> t.getCmts()).forEach(cmts -> {
                FileOutputStream fos = null;
                try {
                    String suffix = "ds";
                    if (params.get("downstream") != null && !Boolean.parseBoolean(params.get("downstream")))
                        suffix = "us";

                    fos = new FileOutputStream(cmts + "_2016_" + suffix + ".csv");
                    writeHeader(fos);
                    osMap.put(cmts, fos);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        return osMap;
    }

    private void writeHeader(FileOutputStream fout) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("cmts,mac,sg,pctUtil,kbps,year,month,wk,day,hour,min,ts").append("\n");
        fout.write(sb.toString().getBytes());
        fout.flush();
    }

    private void write(Collection<SgStatsData> data, OutputStream out, DataListener<Collection> listener) throws IOException {
        if (out != null) {
            StringBuilder sb = new StringBuilder();
            for (SgStatsData sg : data) {
                sb.append(sg.getCmts()).append(",").
                        append(sg.getCableMac()).append(",").
                        append(sg.getSgId()).append(",").
                        append(sg.getSgPctUtil()).append(",").
                        append(sg.getSgKbps()).append(",").
                        append(sg.getYear()).append(",").
                        append(sg.getMonth()).append(",").
                        append(sg.getWk()).append(",").
                        append(sg.getDay()).append(",").
                        append(sg.getHour()).append(",").
                        append(sg.getMinute()).append(",").
                        append(java.time.Instant.ofEpochMilli(sg.getTs()).toString()).
                        append("\n");
                out.write(sb.toString().getBytes());
            }
        }
        if (listener != null) {
            listener.handle(data);
        }
    }

    private void closeFOS(Map<String, OutputStream> osMap) {
        osMap.values().forEach(os -> {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private Pair<Double, Double> getPercUtil() {
        double percUtil = dist.sample() + fudge;
        while (percUtil < 0) percUtil = dist.sample() + fudge;
        return Pair.of(percUtil, percUtil * onePerc);
    }

    public String[] getSupportedOperations() {
        return new String[]{"genTopology", "genSgStats"};
    }

    public Object simulate(String operation, Map<String, String> params, DataListener<Collection> listener) {
        switch (operation) {
            case "genTopology":
                return genTopology();
            case "genSgStats":
                return genSgStats(Boolean.parseBoolean(params.get("downstream")));
            case "genOneYear":
                try {
                    genOneYear(params, listener);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            default:
                logger.warn("Unsupported operation: " + operation);
        }
        return false;
    }
}
