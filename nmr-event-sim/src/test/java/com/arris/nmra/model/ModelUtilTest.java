package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by smishra on 11/28/16.
 */
public class ModelUtilTest {
    private static final Logger log = LoggerFactory.getLogger(ModelUtilTest.class);

    @Test
    public void givenModelAnnotatedClassTableNameShouldMatchTheValueAttrOfModel() {
        StatsData statsData = new StatsData();
        Assert.assertEquals("stats_raw", ModelUtil.getTableName(statsData));

        TierData tierData = new TierData();
        Assert.assertEquals("tiers_raw", ModelUtil.getTableName(tierData));

        TopologyData topologyData = new TopologyData();
        Assert.assertEquals("topology", ModelUtil.getTableName(topologyData));
    }

    @Test
    public void givenNonModelAnnotatedClassTableNameShouldMatchTheClassName() {
        ArrayList statsData = new ArrayList();
        Assert.assertEquals("arraylist", ModelUtil.getTableName(statsData));

        Object object = new Object();
        Assert.assertEquals("object", ModelUtil.getTableName(object));

        object = new TestClass();
        Assert.assertEquals("testclass", ModelUtil.getTableName(object));

        object = new InnerTestClass();
        Assert.assertEquals("modelutiltest$innertestclass", ModelUtil.getTableName(object));
    }

    @Test
    public void givenListEmptyObjectsWhenGetPSValuesCalledThenAnEmptyPSParamListShouldBeGenerated() {
        List<ModelUtil.ColumnData> columnDataList = ModelUtil.getColumnDataForInserts(null);
        Assert.assertTrue(columnDataList.isEmpty());
        columnDataList = ModelUtil.getColumnDataForInserts(new ArrayList<>());
        Assert.assertTrue(columnDataList.isEmpty());
    }

    @Test
    public void givenListOfModelObjectsWhenGetPSValuesCalledThenANonEmptyPSParamListShouldBeGenerated() {
        List<TopologyData> list = getTopologyData();

        List<ModelUtil.ColumnData> columnDataList = ModelUtil.getColumnDataForInserts(list);
        log.info(columnDataList.toString());

        columnDataList.forEach(p -> Assert.assertEquals(list.size(), p.values.size()));
    }

    @Test
    public void givenEmptyListOfModelObjectsWhenCreateStatementCalledThenAnEmptyPSValuesAndStatementShouldBeGenerated() {
        List<ModelUtil.ColumnData> columnDatas = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(null, columnDatas);
        Assert.assertTrue(columnDatas.isEmpty());
        Assert.assertTrue(stmt.isEmpty());

        stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(Collections.EMPTY_LIST, columnDatas);
        Assert.assertTrue(columnDatas.isEmpty());
        Assert.assertTrue(stmt.isEmpty());
    }

    @Test
    public void givenListOfModelObjectsWhenCreateStatementCalledThenNonAEmptyPSValuesAndStatementShouldBeGenerated() {
        List<TopologyData> list = getTopologyData();
        List<ModelUtil.ColumnData> columnDatas = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(list, columnDatas);
        Assert.assertTrue(columnDatas.size()>0);
        Assert.assertTrue(stmt.length()>0);

        //verify that there are values for each of the topology value read
        columnDatas.forEach(p -> Assert.assertEquals(list.size(), p.values.size()));

        //verify that number of ? in prepared statement matches number of column data
        int paramCount = StringUtils.countMatches(stmt, "?");
        Assert.assertEquals(columnDatas.size(), paramCount);
        log.info("preparedStatement:\n" + stmt);
        Assert.assertTrue(stmt.startsWith("insert into topology (") || stmt.startsWith("if not exists (select * from topology"));
    }

    @Test
    public void fieldsWithIgnoreAnnotationShouldBeIgnored() {
        TestClass tc = new TestClass("sanjeev", "mishra");
        Collection c = ModelUtil.getColumnDataForInserts(Collections.singletonList(tc));
        Assert.assertEquals(3, c.size());
        log.debug(c.toString());

        Collection<ModelUtil.ColumnData> result = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(Collections.singletonList(tc), result);
        Assert.assertFalse(StringUtils.isEmpty(stmt));
        Assert.assertEquals(3, result.size());
        log.info(stmt);
        Assert.assertTrue(stmt.indexOf("lname")>-1);
        Assert.assertTrue(stmt.indexOf("fname")>-1);
        Assert.assertTrue(stmt.indexOf("name")>-1);
        Assert.assertFalse(stmt.indexOf("fullname")>-1);
        ModelUtil.ColumnData name = result.stream().filter(p -> p.columnName.equals("name")).findFirst().get();

        Assert.assertEquals(tc.fullName, name.values.get(0));
    }

    @Test
    public void interchangingTheFieldAndColumnNamesShouldWork() {
        TestClass tc = new TestClass("sanjeev", "mishra");

        Collection<ModelUtil.ColumnData> result = new ArrayList<>();
        String stmt = ModelUtil.createPreparedStatementAndPopulateColumnDataForPS(Collections.singletonList(tc), result);
        Assert.assertFalse(StringUtils.isEmpty(stmt));
        Assert.assertEquals(3, result.size());
        log.info(stmt);
        Assert.assertTrue(stmt.indexOf("lname")>-1);
        Assert.assertTrue(stmt.indexOf("fname")>-1);
        Assert.assertTrue(stmt.indexOf("name")>-1);
        Assert.assertFalse(stmt.indexOf("fullname")>-1);
        ModelUtil.ColumnData name = result.stream().filter(p -> p.columnName.equals("name")).findFirst().get();

        Assert.assertEquals(tc.fullName, name.values.get(0));
    }

    @Test
    public void givenPreCheckAnnotationPreCheckPSShouldBeGenerated() {
        TestPreCheckClass t = new TestPreCheckClass("101", "101-22-2345", "sanjeev", "mishra");
        List<ModelUtil.ColumnData> psValues = ModelUtil.getPreCheckColumnData(Arrays.asList(t));
        log.info("precheck: " + psValues);
        Assert.assertEquals(2, psValues.size());
        Assert.assertTrue(psValues.stream().filter(p -> p.columnName.equals("id")).findFirst().isPresent());
        Assert.assertTrue(psValues.stream().filter(p -> p.columnName.equals("ssn")).findFirst().isPresent());
    }

    private List<StatsData> getStatsData() throws IOException {
        StatsData arr = new ObjectMapper().readValue(Util.streamFromResource("data/stats-single.json"), StatsData.class);
        return Arrays.asList(arr);
    }

    private List<TopologyData> getTopologyData() {
        List<TopologyData> list = new ArrayList<>();
        {
            TopologyData topologyData = new TopologyData();
            topologyData.setCmts("cmts-1");
            topologyData.setHub("hub-1");
            topologyData.setMarket("market-1");
            topologyData.setTs(System.currentTimeMillis());
            list.add(topologyData);
        }

        {
            TopologyData topologyData = new TopologyData();
            topologyData.setCmts("cmts-2");
            topologyData.setHub("hub-1");
            topologyData.setMarket("market-1");
            topologyData.setTs(System.currentTimeMillis());
            list.add(topologyData);
        }
        return list;
    }

    class InnerTestClass {
    }
}

class TestClass {
    @ModelAttr(value = "fname")
    public String firstName;
    @ModelAttr(value = "lname")
    public String lastName;
    @ModelAttr(ignore = true)
    public String name;
    @ModelAttr(value = "name")
    public String fullName;

    public TestClass(){}
    public TestClass(String fname, String lname) {
        this.firstName = fname;
        this.lastName = lname;
        this.name = lname + ", " + fname;
        this.fullName = fname + " " + lname;
    }
}


class TestPreCheckClass {
    @ModelAttr(preCheck = true)
    public String id;
    @ModelAttr(value = "ssn", preCheck = true)
    public String ssn;
    @ModelAttr(value = "fname")
    public String firstName;
    @ModelAttr(value = "lname")
    public String lastName;
    @ModelAttr(ignore = true)
    public String name;
    @ModelAttr(value = "name")
    public String fullName;

    public TestPreCheckClass(){}
    public TestPreCheckClass(String fname, String lname) {
        this.firstName = fname;
        this.lastName = lname;
        this.name = lname + ", " + fname;
        this.fullName = fname + " " + lname;
    }

    public TestPreCheckClass(String id, String ssn, String fname, String lname) {
        this(fname,lname);
        this.id =id;
        this.ssn = ssn;
    }
}

