package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by smishra on 11/10/16.
 */
public class TierDataTest {
    private static final Logger log = LoggerFactory.getLogger(TierDataTest.class);
    @Test
    public void givenJsonFileTierDataShouldBeRead() throws IOException {
        String fileName = "data/upstreamTiers.json";
        ObjectMapper mapper = new ObjectMapper();
        TierData[] array = mapper.readValue(Util.bytesFromResource(fileName), TierData[].class);
        Assert.assertNotNull(array);
        Assert.assertTrue(array.length>0);

        byte[] bytes = mapper.writeValueAsBytes(array);
        log.debug(new String(bytes));
        TierData[] converted = mapper.readValue(bytes, TierData[].class);
        Assert.assertEquals(array.length, converted.length);
        for (int i=0; i<converted.length;++i) {
            Assert.assertEquals(array[i].getCableMac(), converted[i].getCableMac());
            Assert.assertTrue(array[i].getTs()!=null && array[i].getTs()>0);
            Assert.assertEquals(array[i].getTs(), converted[i].getTs());
        }
    }
}
