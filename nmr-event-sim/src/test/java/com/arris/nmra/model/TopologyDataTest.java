package com.arris.nmra.model;

import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by smishra on 11/10/16.
 */
public class TopologyDataTest {
    private static final Logger log = LoggerFactory.getLogger(TopologyDataTest.class);
    @Test
    public void givenJsonFileTopologyDataShouldBeRead() throws IOException {
        String fileName = "data/topology.json";
        ObjectMapper mapper = new ObjectMapper();
        TopologyData[] array = mapper.readValue(Util.bytesFromResource(fileName), TopologyData[].class);
        Assert.assertNotNull(array);
        Assert.assertTrue(array.length>0);

        byte[] bytes = mapper.writeValueAsBytes(array);
        log.debug(new String(bytes));
        TopologyData[] converted = mapper.readValue(bytes, TopologyData[].class);
        Assert.assertEquals(array.length, converted.length);
        for (int i=0; i<converted.length;++i) {
            Assert.assertEquals(array[i].getHub(), converted[i].getHub());
            Assert.assertTrue(array[i].getTs()!=null && array[i].getTs()>0);
            Assert.assertEquals(array[i].getTs(), converted[i].getTs());
        }
    }
}
