package com.arris.nmra.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by smishra on 1/11/17.
 */
public class CMTSUtilizationTest {
    private static final Logger logger = LoggerFactory.getLogger(CMTSUtilizationTest.class);
    Random random = new Random();

    @Test
    public void shouldBeAbleToWriteToJsonAndReadFromJson() throws IOException {
        List<CMTSUtilization> coll = createUtilizations();

        ObjectMapper mapper = new ObjectMapper();
        byte[] bytes = mapper.writeValueAsBytes(coll);
        logger.debug(new String(bytes));
        CMTSUtilization[] arr = mapper.readValue(bytes, CMTSUtilization[].class);
        Assert.assertEquals(coll.size(), arr.length);
        for (int i=0; i<coll.size();++i) {
            Assert.assertEquals(coll.get(i).getMarket(), arr[i].getMarket());
            Assert.assertEquals(coll.get(i).getHub(), arr[i].getHub());
            Assert.assertEquals(coll.get(i).getCmts(), arr[i].getCmts());
            Assert.assertEquals(coll.get(i).getCmCount(), arr[i].getCmCount());
        }

        Collection<UtilizationNode> list = UtilizationNode.toUtilizationNodes(coll);
        byte[] cmtsUtilNodeBytes = mapper.writeValueAsBytes(list);
        logger.debug(new String(cmtsUtilNodeBytes));

        UtilizationNode[] readNodeArr = mapper.readValue(cmtsUtilNodeBytes, UtilizationNode[].class);
        Assert.assertEquals(list.size(), readNodeArr.length);
    }

    @Test
    public void givenCMTSUtilizationsSingleHubConversionToUtilizationNodesShouldSucceed() throws JsonProcessingException {
        List<CMTSUtilization> coll = new ArrayList<>();
        int min = 300;
        int count = 3;
        for (int i=min; i < min+count; i++) {
            coll.add(createUtilization(i, "New England", "Hub_MA"));
        }

        int expNodeCount = count + 1 + 1; //3 cmts + 1 hub + 1 market
        Collection<UtilizationNode> nodes = UtilizationNode.toUtilizationNodes(coll);

        Assert.assertEquals(expNodeCount, nodes.size());
        Assert.assertEquals(1,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).count());
        Assert.assertEquals(1,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.hub)).count());
        Assert.assertEquals(3,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.cmts)).count());

        int cmCount = coll.stream().mapToInt(cmts -> cmts.getCmCount()).sum();
        nodes.stream().forEach(n -> Assert.assertFalse(n.getPath().contains(n.getName())));
        Assert.assertEquals(cmCount, nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).findFirst().get().getCms());
        Assert.assertEquals(cmCount, nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.hub)).findFirst().get().getCms());

        int upstreamSGCount = coll.stream().mapToInt(cmts -> cmts.getUpstreamSGCount()).sum();
        Assert.assertEquals(upstreamSGCount, nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).findFirst().get().getUpstreamProjected().serviceGroups);
        Assert.assertEquals(upstreamSGCount, nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.hub)).findFirst().get().getUpstreamProjected().serviceGroups);

        ObjectMapper mapper = new ObjectMapper();
        byte[] bytes = mapper.writeValueAsBytes(nodes);
        logger.info(new String(bytes));
    }

    @Test
    public void givenCMTSUtilizationsMultiHubConversionToUtilizationNodesShouldSucceed() throws JsonProcessingException {
        List<CMTSUtilization> coll = new ArrayList<>();
        int min = 300;
        int count = 3;
        int i =min;
        for (i=i; i < min+count; i++) {
            coll.add(createUtilization(i, "New England", "Hub_MA"));
        }

        for (i=i; i < min+(2*count); i++) {
            coll.add(createUtilization(i, "New England", "Hub_DC"));
        }

        int expNodeCount = count*2 + 2 + 1; //3 cmts + 1 hub + 1 market
        Collection<UtilizationNode> nodes = UtilizationNode.toUtilizationNodes(coll);

        Assert.assertEquals(expNodeCount, nodes.size());

        Assert.assertEquals(1,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).count());
        Assert.assertEquals(2,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.hub)).count());
        Assert.assertEquals(6,nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.cmts)).count());

        nodes.stream().forEach(n -> Assert.assertFalse(n.getPath().contains(n.getName())));

        nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).
            forEach(n -> Assert.assertEquals("",n.getPath()));

        int cmCount = coll.stream().mapToInt(cmts -> cmts.getCmCount()).sum();
        Assert.assertEquals(cmCount, nodes.stream().filter(n -> n.getType().equals(UtilizationNode.NodeType.market)).findFirst().get().getCms());

        ObjectMapper mapper = new ObjectMapper();
        byte[] bytes = mapper.writeValueAsBytes(nodes);
        logger.info(new String(bytes));
    }


    private List<CMTSUtilization> createUtilizations() {
        List<CMTSUtilization> coll = new ArrayList<>();
        for (int i = 300; i < 304; i++) {
            coll.add(createUtilization(i, "New England", "Hub_MA"));
        }

        for (int i = 400; i < 403; i++) {
            coll.add(createUtilization(i, "New England", "Hub_NH"));
        }

        for (int i = 500; i < 507; i++) {
            coll.add(createUtilization(i, "Mid Atlantic", "Hub_DC"));
        }
        return coll;
    }

    private CMTSUtilization createUtilization(int topologyId, String market, String hub) {
        String cmts = "CMTS_" + topologyId;
        CMTSUtilization utilization = new CMTSUtilization(topologyId, market, hub, cmts);
        utilization.setCmCount(1000 + (int)((10000 - 1000) * random.nextDouble()));
        utilization.setDownstreamAvgUtil(10 + (90 - 10) * random.nextDouble());
        int downstreamSGCount = 10 + (int)((32 - 10) * random.nextDouble());
        utilization.setDownstreamSGCount(downstreamSGCount);
        int downstreamOvercapacityCount = (int)(downstreamSGCount * random.nextDouble());
        utilization.setDownstreamOverCapacityCount(downstreamOvercapacityCount);
        utilization.setDownstreamOverCapacityPercent(downstreamOvercapacityCount*100/downstreamSGCount);


        utilization.setUpstreamAvgUtil(10 + (90 - 10) * random.nextDouble());
        int upstreamSGCount = 10 + (int)((32 - 10) * random.nextDouble());
        utilization.setUpstreamSGCount(upstreamSGCount);
        int upstreamOvercapacityCount = (int)(upstreamSGCount * random.nextDouble());
        utilization.setUpstreamOverCapacityCount(upstreamOvercapacityCount);
        utilization.setUpstreamOverCapacityPercent(upstreamOvercapacityCount*100/upstreamSGCount);

        return utilization;
    }
}
