package com.arris.nmra.model;

import org.junit.Assert;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by smishra on 11/28/16.
 */
public class ModelAnnotationTest {
    @Test
    public void givenModelAnnotatedClassValueShouldBeFound() {
        StatsData statsData = new StatsData();
        Optional<Annotation> optional = Arrays.stream(statsData.getClass().getAnnotations()).filter(t -> t.annotationType().equals(Model.class)).findFirst();
        Assert.assertNotNull(optional.get());
        Assert.assertEquals("stats_raw", ((Model)optional.get()).value());

        TierData tierData = new TierData();
        optional = Arrays.stream(tierData.getClass().getAnnotations()).filter(t -> t.annotationType().equals(Model.class)).findFirst();
        Assert.assertNotNull(optional.get());
        Assert.assertEquals("tiers_raw", ((Model)optional.get()).value());

        TopologyData topologyData = new TopologyData();
        optional = Arrays.stream(topologyData.getClass().getAnnotations()).filter(t -> t.annotationType().equals(Model.class)).findFirst();
        Assert.assertNotNull(optional.get());
        Assert.assertEquals("topology", ((Model)optional.get()).value());
    }
}
