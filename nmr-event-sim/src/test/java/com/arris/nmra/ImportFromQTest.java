package com.arris.nmra;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest()
abstract public class ImportFromQTest {
    private static final Logger log = LoggerFactory.getLogger(ImportFromQTest.class);

    @Autowired
    private RabbitTemplate template;

    @Before
    public void setUp() {
    }

    @Test
    public void pass() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Message message = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String queue = "topology";
        if ((message = template.receive(queue)) != null) {
            try {
                baos.write(message.getBody());
                baos.write('\n');
            } catch (IOException e) {
                log.warn("Error writing to stream", e);
            }
        }
        FileOutputStream faos = new FileOutputStream(queue + "_" + System.currentTimeMillis() + ".json");
        faos.write(baos.toByteArray());
        faos.flush();
        faos.close();
    }
}
