package com.arris.nmra.config;

import com.arris.nmra.service.DBService;
import com.arris.nmra.service.MockDBService;
import com.arris.nmra.service.MockRabbitTemplate;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.config.CronTask;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by smishra on 12/22/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class DataPublisherTest {
    private static final Logger logger = LoggerFactory.getLogger(DataPublisherTest.class);

    private MockRabbitTemplate mockRabbitTemplate = new MockRabbitTemplate();
    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private DataPublisher dataPublisher;

    @Before
    public void setUp() {
        dataPublisher.setRabbitTemplate(mockRabbitTemplate);
    }

    @After
    public void tearDown() {
        dataPublisher.setRabbitTemplate(template);
    }

    @Test
    public void cronTasksShouldBeConfiguredProperly() throws InterruptedException {
        Map<String, Collection<CronTask>> cronTaskMap = dataPublisher.cronTaskMap;

        Assert.assertEquals(1, cronTaskMap.size());
        Collection<CronTask> cronTasks = cronTaskMap.values().iterator().next();
        Assert.assertEquals(1, cronTasks.size());

        cronTaskMap.entrySet().forEach(e -> logger.info(String.format("Key %s -> Expression %s", e.getKey(),
                e.getValue().stream().map(t -> t.getExpression()).collect(Collectors.joining(",")))));
        Assert.assertTrue(mockRabbitTemplate.getExchange().isEmpty());
        Assert.assertTrue(mockRabbitTemplate.getCalls().isEmpty());
        cronTasks.iterator().next().getRunnable().run();
        Assert.assertFalse(mockRabbitTemplate.getExchanges().isEmpty());
        Assert.assertFalse(mockRabbitTemplate.getCalls().isEmpty());
        Map<String, Integer> exchanges = mockRabbitTemplate.getExchanges();
        Assert.assertEquals(1, exchanges.size());
        Assert.assertTrue(exchanges.keySet().contains(exchange));
        Assert.assertEquals((Integer) 3, exchanges.get(exchange));
        Assert.assertEquals(3, mockRabbitTemplate.getCalls().size());
        Assert.assertTrue(mockRabbitTemplate.getCalls().containsKey("test.sa.topology"));
        Assert.assertTrue(mockRabbitTemplate.getCalls().containsKey("test.sa.upstream.stats"));
        Assert.assertTrue(mockRabbitTemplate.getCalls().containsKey("test.sa.downstream.stats"));
    }
}
