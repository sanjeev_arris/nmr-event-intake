package com.arris.nmra.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;

/**
 * Created by smishra on 12/22/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class ScheduledTaskTest {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTaskTest.class);

    @Autowired
    private PublicationConfig publicationConfig;

    @Test
    public void aggregationQueriesShouldBeDefined() {
        Assert.assertFalse(publicationConfig.getTasks().isEmpty());
        publicationConfig.getTasks().forEach(q -> Assert.assertFalse(q.getTasks().isEmpty()));
        publicationConfig.getTasks().stream().flatMap(q -> q.getTasks().stream()).forEach(q -> Assert.assertNotNull(q.getOperation()));
        publicationConfig.getTasks().stream().flatMap(q -> q.getTasks().stream()).forEach(q -> Assert.assertNotNull(q.getParams()));
        Assert.assertFalse(publicationConfig.getTasks().stream().flatMap(q -> q.getTasks().stream()).filter(t -> t.getParams().isEmpty()).findAny().isPresent());
        int queryCount = publicationConfig.getTasks().stream().flatMap(q -> q.getTasks().stream()).collect(Collectors.toList()).size();
        Assert.assertEquals(3, queryCount);
        logger.info(publicationConfig.getTasks().stream().flatMap(q -> q.getTasks().stream()).map(q -> q.getOperation()).collect(Collectors.toList()).toString());
    }
}
