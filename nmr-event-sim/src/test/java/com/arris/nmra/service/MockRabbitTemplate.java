package com.arris.nmra.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by smishra on 1/24/17.
 */
public class MockRabbitTemplate extends RabbitTemplate {
    Map<String, Integer> exchanges = new HashMap<>();
    Map<String, Collection<Object>> calls = new HashMap<>();

    @Override
    public void convertAndSend(String exchange, String routing, Object object) {
        exchanges.putIfAbsent(exchange,0);
        Integer i = exchanges.get(exchange);
        exchanges.put(exchange, ++i);
        calls.putIfAbsent(routing, new ArrayList<>());
        calls.get(routing).add(object);
    }

    public Map<String, Integer> getExchanges() {
        return exchanges;
    }

    public Map<String,Collection<Object>> getCalls() {
        return calls;
    }

    public void reset() {
        exchanges.clear();
        calls.clear();
    }
}
