package com.arris.nmra.service;

import com.arris.nmra.model.SgStatsData;
import com.arris.nmra.model.TopologyData;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.random.CorrelatedRandomVectorGenerator;
import org.apache.commons.math3.random.GaussianRandomGenerator;
import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.rng.UniformRandomProvider;
import org.apache.commons.rng.core.source64.RandomLongSource;
import org.apache.commons.rng.simple.RandomSource;
import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by smishra on 1/23/17.
 */
public class SimulatorTest {
    public static final Logger logger = LoggerFactory.getLogger(SimulatorTest.class);

    @Test
    public void genTopology() {
        Simulator simulator = new Simulator();
        Collection<TopologyData> topology = simulator.genTopology();
        Assert.assertFalse(topology.isEmpty());
    }

    @Test
    public void genSgStats() {
        Simulator simulator = new Simulator();
        Collection<SgStatsData> sgStats = simulator.genSgStats(true);
        Assert.assertFalse(sgStats.isEmpty());
        Assert.assertEquals(0, sgStats.stream().filter(s -> s.getDownstream() == 0).count());
        sgStats = simulator.genSgStats(false);
        Assert.assertFalse(sgStats.isEmpty());
        Assert.assertEquals(0, sgStats.stream().filter(s -> s.getDownstream() == 1).count());
    }

    @Test
    public void givenSupportedOperationsSimulatorShouldSimulate() {
        Simulator simulator = new Simulator();
        String[] support = simulator.getSupportedOperations();
        Assert.assertEquals(2, support.length);
        Assert.assertEquals(1, Arrays.stream(support).filter(n -> n.equals("genTopology")).count());
        Assert.assertEquals(1, Arrays.stream(support).filter(n -> n.equals("genSgStats")).count());
        {
            Collection<TopologyData> result = (Collection<TopologyData>) simulator.simulate("genTopology", new HashMap<>(), null);
            Assert.assertFalse(result.isEmpty());
            TopologyData first = result.iterator().next();
        }
        {
            Collection<SgStatsData> result = (Collection<SgStatsData>) simulator.simulate("genSgStats", new HashMap<>(), null);
            Assert.assertFalse(result.isEmpty());
            SgStatsData first = result.iterator().next();
        }
    }

    @Test
    public void givenSupportedOperationAndParameterSimulatorShouldSimulate() {
        Simulator simulator = new Simulator();
        Collection<SgStatsData> result = (Collection<SgStatsData>) simulator.simulate("genSgStats", Collections.singletonMap("downstream", "false"), null);
        Assert.assertFalse(result.isEmpty());
        SgStatsData first = result.iterator().next();
        Assert.assertFalse(result.stream().filter(s -> s.getDownstream() > 0).findAny().isPresent());
        result = (Collection<SgStatsData>) simulator.simulate("genSgStats", Collections.singletonMap("downstream", "true"), null);
        Assert.assertTrue(result.stream().filter(s -> s.getDownstream() > 0).findAny().isPresent());
    }

    @Test
    public void givenUnSupportedOperationsSimulatorShouldReturnFalse() {
        Simulator simulator = new Simulator();
        Object result = simulator.simulate("unsupported", new HashMap<>(), null);
        Assert.assertNotNull(result);
        Boolean bool = (Boolean) result;
        Assert.assertFalse(bool);
    }

    @Test
    public void normal() throws IOException {
        double mean = 40.25;
        double sd = 21.56168;
        double onePerc = 10207.75;
        NormalDistribution dist = new NormalDistribution(mean, sd);

        FileOutputStream faos = new FileOutputStream("random.out");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < 1000; ++i) {
            double d = dist.sample();
            baos.write((d + "," + d * onePerc + "\n").getBytes());
        }
        faos.write(baos.toByteArray());
        faos.close();
    }

    @Test
    public void multivariate() throws IOException {
        double[] mean = {7.8720, 81020.0};
        double[][] cov = {{27.72269, 1}, {1, 2936665372.0}};
        RealMatrix covariance = MatrixUtils.createRealMatrix(cov);

        long seed = 17399225432L;

        GaussianRandomGenerator rawGenerator = new GaussianRandomGenerator(new MersenneTwister(seed));

        CorrelatedRandomVectorGenerator generator =
                new CorrelatedRandomVectorGenerator(mean, covariance, 1.0e-12 * covariance.getNorm(), rawGenerator);

        FileOutputStream faos = new FileOutputStream("random.out");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for (int i = 0; i < 1000; ++i) {
            double[] randomVector = generator.nextVector();
            StringBuilder sb = new StringBuilder();
            for (double d : randomVector) {
                sb.append(d).append(",");
            }
            if (sb.length() > 0) sb.setLength(sb.length() - 1);
            sb.append("\n");
            baos.write(sb.toString().getBytes());
        }
        faos.write(baos.toByteArray());
        faos.close();
    }

    //@Test
    public void oneYearSGData() throws IOException {
        Simulator simulator = new Simulator();
        Map<String, String> params = new HashMap<>();
        params.put("downstream", "true");
        params.put("file", "true");
        simulator.genOneYear(params, (collection) -> {});
    }

    @Test
    public void nextY() {
        int x = 1;
        double m = .301;
        double c = 9.733031;
        Simulator simulator = new Simulator();

        StringBuilder sb = new StringBuilder();
        for (x =1; x<=365; ++x) {
            sb.append(String.format("%s,%s\n", x,simulator.nextY(x,m,c)));
        }

        logger.info(sb.toString());
    }
}
