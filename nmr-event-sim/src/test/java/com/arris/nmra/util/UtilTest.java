package com.arris.nmra.util;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.format.DateTimeParseException;

/**
 * Created by smishra on 11/10/16.
 */
public class UtilTest {
    private static final Logger log = LoggerFactory.getLogger(UtilTest.class);

    @Test
    public void toUTC() {
        Long now = System.currentTimeMillis();
        String utc = Util.toUTC(now);
        Assert.assertEquals(now, Util.toMillis(utc, null));
    }

    @Test
    public void toMillis() {
        String dt = "2016-11-07 00";
        Long millis = Util.toMillis(dt, null);
        String utc = Util.toUTC(millis);
        log.info(String.format("orig: %s res: %s", dt, utc));
        Assert.assertEquals(millis, Util.toMillis(utc, null));

        dt = "2016-11-07 12:04";
        millis = Util.toMillis(dt, null);
        utc = Util.toUTC(millis);
        log.info(String.format("orig: %s res: %s", dt, utc));
        Assert.assertEquals(millis, Util.toMillis(utc, null));

        dt = "2016-11-07 12:04:32";
        millis = Util.toMillis(dt, null);
        utc = Util.toUTC(millis);
        log.info(String.format("orig: %s res: %s", dt, utc));
        Assert.assertEquals(millis, Util.toMillis(utc, null));

        Long now = System.currentTimeMillis();
        utc = Util.toUTC(now);
        Assert.assertEquals(now, Util.toMillisFromUTC(utc));
    }

    @Test
    public void givenCorrectFormattedDatetoMillisFromUTCShouldPass() {
        Long now = System.currentTimeMillis();
        String utc = Util.toUTC(now);
        Long res = Util.toMillisFromUTC(utc);
        Assert.assertEquals(now, res);
    }

    @Test
    public void givenWrongFormattedDatetoMillisFromUTCShouldFail() {
        try {
            String dt = "2016-11-07 00";
            Util.toMillisFromUTC(dt);
            Assert.fail("Exception should be thrown");
        } catch (DateTimeParseException e) {
        }
    }

    @Test
    public void givenNullTSDefaultValueShouldBeReturned() {
        Long now = System.currentTimeMillis();
        Assert.assertEquals(now, Util.toMillis(null, now));
    }

    @Test
    public void givenClasspathResourceItShouldBeRead() {
        String resName = "data/upstreamTiers.json";
        byte[] bytes = Util.bytesFromResource("data/upstreamTiers.json");
        Assert.assertTrue(bytes.length>0);
    }

    @Test
    public void givenAbsoluteFileItShouldBeRead() {
        String resName = "data/upstreamTiers.json";
        byte[] bytes = Util.bytesFromResource("data/upstreamTiers.json");
        Assert.assertTrue(bytes.length>0);

        String path = this.getClass().getClassLoader().getResource(resName).getPath();
        log.info("res path: " + path);
        Assert.assertEquals(bytes.length, Util.bytesFromResource(path).length);
    }

}
