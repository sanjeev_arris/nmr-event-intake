package com.arris.nmra.controller;

import com.arris.nmra.model.UtilizationNode;
import com.arris.nmra.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by smishra on 1/13/17.
 */
public class ServiceResponseTest {
    private static final Logger logger = LoggerFactory.getLogger(ServiceResponseTest.class);
    @Test
    public void givenSummaryResponseShouldSucceed() throws IOException {
        UtilizationNode[] nodes = new ObjectMapper().
            readValue(Util.bytesFromResource("data/cmtsUtilNodes.json"), UtilizationNode[].class);
        Assert.assertTrue(nodes.length>0);
        ServiceResponse<UtilizationNode[]> response =
            new ServiceResponse<>(nodes, 0, nodes.length);
        logger.debug(new ObjectMapper().writeValueAsString(response));
    }
}
