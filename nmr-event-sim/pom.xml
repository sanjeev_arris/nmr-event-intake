<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.4.1.RELEASE</version>
	</parent>
	<groupId>com.arris.nmr</groupId>
	<artifactId>simulator</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>network-mgmt-reporting-and-analytics</name>
	<description>ARRIS Assurance Solutions: Network Management Reporting and Analytics</description>
	<organization>
	   <name>ARRIS Solutions</name>
	   <url>http://www.arris.com/</url>
	</organization>
    <properties>
        <build.rpm.disabled>true</build.rpm.disabled>
        <java.version>1.8</java.version>
        <install.basedir>/opt/arris/nmra</install.basedir>
		<postgresql-jdbcDriver-version>9.2-1002-jdbc4</postgresql-jdbcDriver-version>
     </properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-amqp</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.session</groupId>
			<artifactId>spring-session-data-redis</artifactId>
			<version>1.2.2.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-core</artifactId>
		</dependency>
		<dependency>
			<groupId>org.codehaus.janino</groupId>
			<artifactId>janino</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.tomcat</groupId>
			<artifactId>tomcat-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>io.reactivex</groupId>
			<artifactId>rxjava</artifactId>
			<version>1.2.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.0</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-rng-core</artifactId>
			<version>1.0</version>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-rng-jmh</artifactId>
			<version>1.0</version>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-rng-client-api</artifactId>
			<version>1.0</version>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-math3</artifactId>
			<version>3.6.1</version>
		</dependency>
		<dependency>
			<groupId>com.microsoft.sqlserver</groupId>
			<artifactId>sqljdbc</artifactId>
			<version>42</version>
		</dependency>
        <dependency>
			<groupId>org.dbunit</groupId>
			<artifactId>dbunit</artifactId>
			<version>2.5.3</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<!--<dependency>-->
			<!--<groupId>com.h2database</groupId>-->
			<!--<artifactId>h2</artifactId>-->
			<!--<scope>test</scope>-->
		<!--</dependency>-->
		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<version>${postgresql-jdbcDriver-version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-jvm</artifactId>
			<version>3.1.2</version>
		</dependency>
	</dependencies>

	<build>
		<defaultGoal>package</defaultGoal>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<executable>true</executable>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<configuration>
					<descriptors>
						<descriptor>src/assembly/assembly.xml</descriptor>
					</descriptors>
				</configuration>
				<executions>
					<execution>
						<id>test-env</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
	<reporting>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>findbugs-maven-plugin</artifactId>
				<version>3.0.4</version>
				<configuration>
					<!-- Don't trigger CC_CYCLOMATIC_COMPLEXITY when < 100 -->
					<jvmArgs>-Dfb-contrib.cc.limit=100</jvmArgs>
					<threshold>Normal</threshold>
					<findbugsXmlOutput>true</findbugsXmlOutput>
					<findbugsXmlWithMessages>true</findbugsXmlWithMessages>
					<xmlOutput>true</xmlOutput>
					<effort>Max</effort>
					<excludeFilterFile>src/main/build/findbugs-exclude.xml</excludeFilterFile>
					<plugins>
						<plugin>
							<groupId>jp.skypencil.findbugs.slf4j</groupId>
							<artifactId>bug-pattern</artifactId>
							<version>1.2.4</version>
						</plugin>
						<plugin>
							<groupId>com.mebigfatguy.fb-contrib</groupId>
							<artifactId>fb-contrib</artifactId>
							<version>6.8.1</version>
						</plugin>
					</plugins>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>jdepend-maven-plugin</artifactId>
				<version>2.0</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changelog-plugin</artifactId>
				<version>2.3</version>
				<configuration>
					<type>date</type>
					<dates>
						<date implementation="java.lang.String">2016-10-01</date>
					</dates>
					<dateFormat>yyyy-MM-dd</dateFormat>
					<issueLinkUrl>https://arrisworkassure.atlassian.net/browse/%ISSUE%</issueLinkUrl>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-changes-plugin</artifactId>
				<version>2.12.1</version>
				<configuration>
					<useJql>true</useJql>
					<maxEntries>500</maxEntries>
					<webUser>mavenuser</webUser>
					<webPassword>mavenuser</webPassword>
					<resolutionIds>Fixed,Closed</resolutionIds>
					<columnNames>Key,Summary,Assignee,Status,Fix Version</columnNames>
					<sortColumnNames>Key DESC</sortColumnNames>
				</configuration>
				<reportSets>
					<reportSet>
						<reports>
							<report>jira-report</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.17</version>
				<configuration>
					<configLocation>src/main/build/checkstyle.xml</configLocation>
					<suppressionsLocation>src/main/build/checkstyle-suppressions.xml</suppressionsLocation>
					<propertiesLocation>src/main/build/checkstyle_maven.properties</propertiesLocation>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.4</version>
				<configuration>
					<!-- Default configuration for all reports -->
				</configuration>
				<reportSets>
					<reportSet>
						<id>non-aggregate</id>
						<configuration>
							<!-- Specific configuration for the non aggregate report -->
							<quiet>true</quiet>
						</configuration>
						<reports>
							<report>javadoc</report>
						</reports>
					</reportSet>
					<reportSet>
						<id>aggregate</id>
						<configuration>
							<!-- Specific configuration for the aggregate report -->
							<quiet>true</quiet>
						</configuration>
						<inherited>false</inherited>
						<reports>
							<report>aggregate</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-project-info-reports-plugin</artifactId>

				<reportSets>
					<reportSet>
						<reports>
							<!-- Continuous Integration -->
							<report>cim</report>
							<report>dependencies</report>
							<report>dependency-convergence</report>
							<!-- Disable dependency-management which is taking too much time -->
							<!-- <report>dependency-management</report> -->
							<!-- Index Page -->
							<report>index</report>
							<!-- Issue Treacking -->
							<report>issue-tracking</report>
							<!-- Project License -->
							<report>license</report>
							<report>modules</report>
							<report>plugin-management</report>
							<report>plugins</report>
							<report>project-team</report>
							<!-- Project Repository -->
							<!-- Disable. site-deploy fails due to new modules added <report>scm</report> -->
							<!-- Project Summary -->
							<report>summary</report>
						</reports>
					</reportSet>
				</reportSets>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.6</version>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>2.19.1</version>
				<configuration>
					<executions>
						<execution>
							<phase>test</phase>
							<goals>
								<goal>report-only</goal>
							</goals>
						</execution>
					</executions>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>taglist-maven-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<tagListOptions>
						<tagClasses>
							<tagClass>
								<displayName>Todo Work</displayName>
								<tags>
									<tag>
										<matchString>TODO</matchString>
										<matchType>ignoreCase</matchType>
									</tag>
									<tag>
										<matchString>FIXME</matchString>
										<matchType>ignoreCase</matchType>
									</tag>
									<tag>
										<matchString>DEPRECATED</matchString>
										<matchType>ignoreCase</matchType>
									</tag>
								</tags>
							</tagClass>
						</tagClasses>
					</tagListOptions>
				</configuration>
			</plugin>
		</plugins>
	</reporting>

	<url>http://www.arris.com/products/service-assurance/</url>
</project>
