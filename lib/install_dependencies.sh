#!/usr/bin/env bash

mvn install:install-file -Dfile=sqljdbc42.jar -DgroupId=com.microsoft.sqlserver -DartifactId=sqljdbc -Dversion=42 -Dpackaging=jar
